@extends('layouts.app')
<style>
    h4 {
        margin-bottom: 1.5em !important;
    }

    h2 {
        margin-bottom: 2em !important;
        text-decoration: underline;
    }
</style>
@section('content')

    <h2>Prova finalizada!</h2>


    <form id="logout-form" action="{{ url('/logout') }}" method="POST">

        {{ csrf_field() }}
        <button class="btn btn-success">Sair</button>
    </form>

@endsection
<script>
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
</script>