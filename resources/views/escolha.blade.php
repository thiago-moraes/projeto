@extends('layouts.prova')

@section('content')


{{ @verificaSeCandidatoEstaNaProvaCorreta($questao->id_prova) }}

<div class="row">

    <div class="col-md-12">
        <h4>Questão - {{ $questao->sequencia }}</h4>
        <hr>
        <p>{{ $questao->descricao }}</p>
    </div>

    <div class="col-md-12">

        @if (session('mensagem'))
        <div class="alert alert-success">

            {{ session('mensagem') }}
        </div>
        @endif

        <form class="form-horizontal" action="/prova/grava/questao/escolha" id="form-questoes" method="post">
            {{ csrf_field() }}

            <input type="hidden" name="idProvaQuestao" value="{{ $questao->id_prova_questao }}">
            <input type="hidden" name="sequencia" value="{{ $questao->sequencia }}">

            <ul id="opcoes">
                @foreach (@retornaOpcoes($questao->id_questao, $questao->id_prova_questao) as $key => $value)
                <li class="row">
                    <label style="font-weight: normal; width: 100%">
                        <span><input type='radio' name="opcao" {{ @verificaSeSelecionado(1,$value->valor) }}></span>
                        {{ $value->descricao }}
                    </label>
                </li>
               @endforeach
            </ul>
        </form>
    </div>
</div>

<div class="row" id="botoes">

    <div class="col-md-offset-6 col-md-3">
        <form method="post" action="/finaliza">
            <input type="hidden" name="idProvaQuestao" value="{{ $questao->id_prova }}">
            <button class="btn btn-info btn-block">Finalizar prova</button>
        </form>
    </div>

    <div class="col-md-3">
        <button class="btn btn-success btn-block" name="confirmar-respostas" onclick="enviaRespostas()">Confirmar respostas</button>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">&nbsp;</div>

    <div class="col-md-1">
        @if(($questao->sequencia - 1) > 0)
        <a href="{{ $questao->sequencia - 1 }}" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span></a>
        @endif
    </div>

    <div class="col-md-10">
        <div class="btn-group">
            &nbsp;
        </div>
    </div>

    <div class="col-md-1 text-right">
        @if(($questao->sequencia + 1) < $totalQuestoes)
        <a href="{{ $questao->sequencia + 1 }}" class="btn btn-default"><span class="glyphicon glyphicon-chevron-right"></span></a>
        @endif
    </div>
</div>

<script>
    function enviaRespostas() {
        document.getElementById('form-questoes').submit();
    }
</script>
@endsection
