@extends('layouts.app')

@section('content')
{!! NoCaptcha::renderJs() !!}
            <div class="col-xs-12 col-md-12 col-sm-12"> &nbsp; </div>
            <div class="col-xs-12 col-md-12 col-sm-12"> &nbsp; </div>

            <div class="col-xs-6 col-xs-offset-3 col-sm-4 col-xs-offset-4 col-md-offset-4 col-md-4">
                <a href="/login"><img src="{{ asset('images/logo-sba.jpg') }}" alt="SBA 70 anos" class="img-responsive"></a>
            </div>
            <div class="col-xs-12 col-md-12 col-sm-12"> &nbsp; </div>
            <div class="col-xs-12 col-md-12 col-sm-12"> &nbsp; </div>

            <div class="col-xs-12 col-md-12 col-sm-12"> <h3 class="text-center">Esqueci minha senha.</h3> </div>

            <div class="col-xs-12 col-md-12 col-sm-12"> &nbsp; </div>
            <div class="col-xs-12 col-md-12 col-sm-12"> &nbsp; </div>
                    

                    <form method="post" action="/login/remember" class="form-horizontal">
                    {{ csrf_field() }}
                        <div class="col-md-6 col-md-offset-3">
                                <label class="control-label">CPF:</label>
                                <input type="text" class="form-control" name="cpf" id="cpf" value="">
                           <br>
                        </div>
                        
                        <div class="col-md-6 col-md-offset-3">
                            
                                    <button type="submit" class="btn btn-block btn-success">Enviar</button><br>
                                    {!! NoCaptcha::display() !!}    
                            <div class="input-group">
                                <span id="helpBlock" class="help-block"> <h3 class="text-danger">{!! session('message') !!} </h3></span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

    <script src="{{ asset("/js/jquery.min.js") }}"></script>
    <script src="{{ asset("/js/jquery.maskedinput.min.js") }}"></script>
    <script>
        jQuery(function($){
            $("#cpf").mask("999.999.999-99",{
                placeholder:"999.999.999-99"
            });
        });

    </script>

</div>
@endsection