<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <title>Provas SBA - Dashboard</title>
    <style>
        
        .table {
            
            width: 100%;
        }
        
        .table tr {
            line-height: 2em;
        }
        
        .table tr:nth-child(odd) {
            
            border: solid 1px #e6e6e6;
            background: #e6e6e6;
        }
        
        .topo table{
            width: 100%;
        }
        
        h2 {
            margin: 2em;
            text-align: center;
        }
        
        .divisor {
            width: 100%;
        }
    </style>
</head>
    <body>
        <div class="container">
            
            <div class="topo">
                <table>
                    <tr>
                        <td width="50%"><img src="{{ public_path('/images/logo-sba.jpg') }}" width="180"></td>
                        <td width="50%">
                           @include('layouts.dados-topo')
                        </td>
                    </tr>
                </table>
                
                <span class="divisor"> &nbsp;</span>
            </div>

            <h2>RELATÓRIO DE NOTAS</h2>
            
              @yield('content')
                
        </div>
    </body>
</html>
