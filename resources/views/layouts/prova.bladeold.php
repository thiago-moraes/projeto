<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ $matriz->descricao or '' }} - {{ $matriz->ano or '' }}</title>
        <!-- Styles -->
        <!-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/prova.css') }}">
        <!-- Scripts -->
        <script>
            window.Laravel = <?= json_encode(['csrfToken' => csrf_token(),]); ?>
        </script>

        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Cache-control" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                    <div class="container-fluid">

                        <div class="navbar-header" style="width: 100%">

                            <div class="col-xs-6 col-sm-4 col-md-4"><h4>{{ $matriz->descricao or '' }} - {{ $matriz->ano or '' }}</h4></div>
                            <div class="col-xs-6 col-sm-4 col-md-4"><h4>Tempo restante: <span id="tempo-de-prova"></span></h4></div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <!-- <div class="collapse navbar-collapse" id="app-navbar-collapse"> -->
                                    <!-- Right Side Of Navbar -->
                                    <ul class="nav navbar-nav navbar-right" style="margin-right:1em;">
                                        <!-- Authentication Links -->
                                        @guest()
                                        <li><a href="{{ url('/login') }}">Login</a></li>
                                        @else
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->nome }} <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                        Sair
                                                    </a>

                                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                        @endif
                                    </ul>
                                <!-- </div> -->
                            </div>
                            <!-- Collapsed Hamburger -->
                            <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                                <span class="sr-only">Toggle Navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button> -->
                            <!-- Branding Image -->
                        </div>
                    </div>
            </nav>

            <div class="container">

                <!-- <div class="col-md-1" style="overflow-y: scroll; height: 650px;">
                    <h5>Atalho</h5>
                    @for($x=1;$x<$totalQuestoes; $x++)
                    <a href="{{ $x }}" class="btn btn-default btn-block">{{ $x }}</a>
                    @endfor
                </div>
                -->
                <div class="col-md-12 conteudo">

                    @yield('content')

                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 total-respondidas">
                            Questões respondidas: <span class="total"><?= totalDeQuestoesRespondidas($questao->id_prova) ?></span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 total-nao-respondidas">
                            Questões não respondidas: <span class="total"><?= totalDeQuestoesNaoRespondidas($questao->id_prova) ?></span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 total-parcialmente-respondidas">
                            Questões parcialmente respondidas <span class="total"><?= totalDeQuestoesParcialmenteRespondidas($questao->id_prova) ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rodape">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    Seu IP: {{  \Request::ip() }}
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 direita">
                    <?php $dataServidor = new \DateTime() ?>
                    Hora do servidor: <?= $dataServidor->format("d/m/Y H:i:s") ?>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 direita">
                    © Sociedade Brasileira de Anestesiologia 2018.
                </div>
            </div>
            
        </div>
        <script>
            /**  
                noback v0.0.1 
                library for prevent backbutton 
                Author: Kiko Mesquita: http://twitter.com/kikomesquita 
                Based on stackoverflow 
                * Copyright (c) 2015 @ kikomesquita 
                */ 

                (function(window) { 
                'use strict'; 
                
                var noback = { 
                    
                    //globals 
                    version: '0.0.1', 
                    history_api : typeof history.pushState !== 'undefined', 
                    
                    init:function(){ 
                        window.location.hash = '#no-back'; 
                        noback.configure(); 
                    }, 
                    
                    hasChanged:function(){ 
                        if (window.location.hash == '#no-back' ){ 
                            window.location.hash = '#BLOQUEIO';
                            //mostra mensagem que não pode usar o btn volta do browser
                            if($( "#msgAviso" ).css('display') =='none'){
                                $( "#msgAviso" ).slideToggle("slow");
                            }
                        } 
                    }, 
                    
                    checkCompat: function(){ 
                        if(window.addEventListener) { 
                            window.addEventListener("hashchange", noback.hasChanged, false); 
                        }else if (window.attachEvent) { 
                            window.attachEvent("onhashchange", noback.hasChanged); 
                        }else{ 
                            window.onhashchange = noback.hasChanged; 
                        } 
                    }, 
                    
                    configure: function(){ 
                        if ( window.location.hash == '#no-back' ) { 
                            if ( this.history_api ){ 
                                history.pushState(null, '', '#BLOQUEIO'); 
                            }else{  
                                window.location.hash = '#BLOQUEIO';
                                //mostra mensagem que não pode usar o btn volta do browser
                                if($( "#msgAviso" ).css('display') =='none'){
                                    $( "#msgAviso" ).slideToggle("slow");
                                }
                            } 
                        } 
                        noback.checkCompat(); 
                        noback.hasChanged(); 
                    } 
                    
                    }; 
                    
                    // AMD support 
                    if (typeof define === 'function' && define.amd) { 
                        define( function() { return noback; } ); 
                    }  
                    // For CommonJS and CommonJS-like 
                    else if (typeof module === 'object' && module.exports) { 
                        module.exports = noback; 
                    }  
                    else { 
                        window.noback = noback; 
                    } 
                    noback.init();
                }(window)); 
        </script>
        <?php

        $data = new \DateTime();

        $extension = verificaTimeExtension(Auth::user()->id);
        if(!is_null($extension) && $extension != '' ):
            $fimProva = new \DateTime( $extension );
        else:
            $fimProva = new \DateTime( $matriz->dt_fim );
        endif;
        $diffHora = $fimProva->diff($data);
        if( $diffHora->invert == 0 ){
            $fimProva = new \DateTime();
            $diffHora = $fimProva->diff($data);
        }
        ?>

        <!-- Scripts -->
        <script src="/js/app.js"></script>
        <script src="/js/contador.js"></script>
        <script>

            var hora2 = <?= json_encode($diffHora->format("%h")) ?>;
            tempoDeProva(<?= json_encode($fimProva->format('Y,m,d,H,i,s')) ?>, <?= json_encode($data->format('Y,m,d,H,i,s')) ?>);

        </script>
    </body>
</html>
