@extends('layouts.app')

@section('content')

<div class="row">

    <br>
    @if(count($questoes) > 0)
        <div class="col-md-12 text-center">
            <h3>Lista de questões não respondidas</h3>
            <h4>Clique na questão em que deseja acessar.</h4>
            <br>
        </div>

        <div class="col-md-offset-2 col-md-8 text-center" style="font-size: 1.3em">
            @foreach($questoes as $questao)
                <a href="/prova/objetiva/{{ $idProva }}/{{ $questao->sequencia }}">{{ $questao->sequencia }} - {!!$questao->descricao !!}</a> <br><br>
            @endforeach
        </div>
    @endif

</div>

@endsection

