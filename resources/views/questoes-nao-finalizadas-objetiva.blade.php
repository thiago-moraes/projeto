@extends('layouts.app')

@section('content')

<div class="row">

    <br>

    <div class="col-md-12 text-center">
        @if(count($questoes))
        <h3>Existem questões não respondidas ou parcialmente respondidas em sua prova.</h3><br>
        @endif
    </div>
    <div class="col-md-offset-2 col-md-8 text-center" style="font-size: 1.3em">
        @foreach($questoes as $questao)
            <a href="/prova/objetiva/{{ $idProva }}/{{ $questao->sequencia }}">{{ $questao->sequencia }} - {!!$questao->descricao !!}</a> <br><br>
        @endforeach
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <h3 class="text-center">Deseja finalizar sua prova?</h3><br><br>
        <div class="row" id="botoes">
            <div class="col-md-offset-3 col-md-3">
                <form method="post" action="/prova/finaliza/confirma">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ $idProva }}" name="idProva">
                    <button class="btn btn-danger btn-block" type="submit">Sim</button>
                </form>
            </div>
            <div class="col-md-3">
                <a href="{!!  URL::previous() !!}" class="btn btn-success btn-block">Não</a>
            </div>
        </div>
    </div>
</div>

@endsection

