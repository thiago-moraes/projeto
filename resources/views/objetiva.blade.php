@extends('layouts.prova')

@section('content')

{{ @verificaSeCandidatoEstaNaProvaCorreta($questao->id_prova) }}

<div class="row">

    <div class="col-md-12">
        <h4>Questão - {{ $questao->sequencia }}</h4>
        <hr>
        <p class="text-justify"> {!! $questao->descricao !!} </p>
        <div class="text-right resposta"> Respostas</div>
    </div>

    <div class="col-md-12">

        @if (session('mensagem'))
        <div class="alert alert-success"> {{ session('mensagem') }} </div>
        @endif

        <form class="form-horizontal" action="/prova/grava/questao" id="form-questoes" method="POST">
            {{ csrf_field() }}

            <input type="hidden" name="idProvaQuestao" value="{{ $questao->id_prova_questao }}">
            <input type="hidden" name="idProva" value="{{ $questao->id_prova }}">
            <input type="hidden" name="sequencia" value="" id="sequencia">
            <input type="hidden" name="finalizar" value="0" id="finalizar">

            <ul id="opcoes">
                @foreach (@retornaOpcoes($questao->id_questao, $questao->id_prova_questao) as $key => $value)
                <li class="row">

                    <div class="col-xs-12 col-sm-9 col-md-10">
                        <label for="<?= $value->id_opcao ?>" style="font-weight: lighter;">
                            {{ @indiceAlfaNumerico($key) }}) {!! $value->descricao !!}
                        </label>
                    </div>

                    <div class=" col-xs-12 col-sm-3 col-md-2">
                        @if ($matriz->id_tipo_prova == 1)
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-10 text-right">
                                <input type="radio" name="opcoes" class="opcao" id="<?= $value->id_opcao ?>" value="<?= $value->id_opcao ?>" <?= verificaSelecionado(2, $value->valor, 'radio') ?>>
                            </div>

                        </div>
                        @else
                        <div class="col-xs-1 col-sm-2 col-md-2 indice">{{ @indiceAlfaNumerico($key) }})</div>

                        <div class="col-xs-12 col-sm-9 col-md-10">
                            <select name="opcoes[<?= $key ?>]" class="form-control opcao">
                                @foreach (['-', 'Falso', 'Verdadeiro'] as $id => $opcao)
                                <option value="<?= $id ?>" <?= verificaSelecionado($id, $value->valor) ?>><?= $opcao ?></option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                    </div>
                </li>
                @endforeach
            </ul>
        </form>
    </div>
</div>

<div class="row" id="botoes">

    <div class="col-md-12 col-xs-12 col-sm-12">&nbsp;</div>

    <div class="col-xs-12 col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4 pull-right">
        <div class="row">
            <div class="col-xs-5 col-sm-offset-2 col-sm-4 col-md-offset-3 col-md-4">
                @if(($questao->sequencia - 1) > 0)
                <a href="javascript:void(0);" data="{{ $questao->sequencia - 1}}" class="btn btn-primary btn-block" onclick="enviaResposta( this )"><span class="glyphicon glyphicon-chevron-left"></span></a>
                @endif
            </div>
            <div class="col-xs-offset-2 col-xs-5 col-sm-offset-1 col-sm-4 col-md-offset-1 col-md-4">
                @if(($questao->sequencia + 1) <= $totalQuestoes) <a href="javascript:void(0);" data="{{ $questao->sequencia + 1 }}" class="btn btn-success btn-block" onclick="enviaResposta( this )"><span class="glyphicon glyphicon-chevron-right"></span></a>
                    @endif
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-4 col-md-4 pull-left">
        <button class="btn btn-danger btn-block" type="button" onclick="finalizaProva()">Finalizar prova</button>
    </div>
</div>

<script>
    function enviaResposta(link) {

        document.getElementById('sequencia').value = link.getAttribute('data');
        document.getElementById("form-questoes").submit();
    }

    function finalizaProva() {

        document.getElementById('sequencia').value = 1;
        document.getElementById('finalizar').value = 1;
        document.getElementById("form-questoes").submit();

    }
</script>
@endsection