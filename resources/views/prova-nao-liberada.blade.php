@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            
            <h2>Prova não liberada</h2>
            <p>Por favor acesse somente em {{ $data->format( 'd/m/Y' ) }} a partir de {{ $data->format( "H:i" ) }}</p>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12"><a href="/logout" class="btn btn-danger">Sair</a></div>
    </div>
</div>
@endsection
