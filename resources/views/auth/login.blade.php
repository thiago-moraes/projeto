@extends('layouts.app')

@section('content')
<div class="container">

    <div class="panel" id="login">
        <div class="panel-body" >

            <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-offset-0 col-xs-12">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" id="form-login">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label for="email" class="control-label">CPF (999.999.999-99)</label>
                            <input id="cpf" type="text" class="form-control" name="cpf" placeholder="999.999.999-99" value="{{ old('cpf') }}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label for="password" class="control-label">Senha</label>
                            <input id="password" type="password" class="form-control" name="password" id="cpf" required>
                            <small class="pull-right"><a href="login/remember">Esqueci minha senha.</a></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4 col-col-sm-4 col-xs-offset-0 col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block">Logar</button>
                        </div>
                    </div>

                    @if (session('mensagem'))
                        <span class="help-block">
                            <strong class="text-danger">{{ session('mensagem') }}</strong>
                        </span>
                    @endif
                    
                </form>
            </div>
        </div>
    </div>
    
    <section id="footer">
        <div class="row">


            <div class="col-xs-6 col-sm-4 col-md-offset-3 col-md-2">
                <img src="{{ asset('images/logo-sba.jpg') }}" alt="SBA 70 anos" class="img-responsive">
                <h4 id="prova-logo">Prova <i style="color:red">online</i> SBA.</h4>
            </div>

            <div class="col-xs-6 col-sm-offset-4 col-sm-4 col-md-offset-2 col-md-2">
                <img src="{{ asset('images/logo-sba70anos.jpg') }}" alt="SBA 70 anos" class="img-responsive">
            </div>
            
            <h3 class="text-center" id="prova-so">Prova <i style="color:red">online</i> SBA.</h3>
        </div>
    </section>

    <script src="{{ asset("/js/jquery.min.js") }}"></script>
    <script src="{{ asset("/js/jquery.maskedinput.min.js") }}"></script>
    <script>
        jQuery(function($){
            $("#cpf").mask("999.999.999-99",{
                placeholder:"999.999.999-99"
            });
        });

    </script>

</div>
@endsection