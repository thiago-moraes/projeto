@extends('layouts.admin')

@section('content')
<div class="container">
    <br><br><br><br><br><br><br><br>
    <div class="panel">
        <div class="panel-body" >
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/login') }}" id="form-login">
                {{ csrf_field() }}

                <div class="form-group {{ $errors->has('usuario') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">Usuário</label>
                    <div class="col-md-6">
                        <input id="cpf" type="text" class="form-control" name="usuario" value="{{ old('usuario') }}" required autofocus>

                        @if ($errors->has('usuario'))
                            <span class="help-block">
                                <strong>{{ $errors->first('usuario') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Senha</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-6">
                         @if(session('mensagem'))
                            <div class="alert alert-danger">{{ session('mensagem') }}</div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">Logar</button>
                    </div>
                </div>
            </form>

            </div>
        </div>
    </div>
@endsection
