<!DOCTYPE html>
<html>
<head>
    <title>Relatório de respostas do candidato.</title>
</head>

<body>
<h2>Provas SBA</h2>
<br/>
<p>
    Prezado dr(a). {{ $candidato->nome }},<br> segue abaixo o link para download de sua prova respondida.<br>
    <a href="https://www.sbahq.org/resources/pdf/prova/{{ $matriz->ano }}/me/{{ $matriz->trim }}trim/{{ $token }}.pdf">Prova Respondida</a> , o gabarito será disponibilizado no site da SBA.
</p>
<p>Caso seu provedor/cliente de email bloqueie o link copie e cole esta url em seu navegador.<br>
    https://www.sbahq.org/resources/pdf/prova/{{ $matriz->ano }}/me/{{ $matriz->trim }}trim/{{ $token }}.pdf</p>

<p>Atenciosamente,<br><br>Comissão de Ensino e Treinamento<br>Sociedade Brasileira de Anestesiologia</p>

</body>

</html>
