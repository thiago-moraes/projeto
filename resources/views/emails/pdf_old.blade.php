<!DOCTYPE html>
<html>
<head>
    <title>Relatório de respostas do candidato.</title>
</head>

<body>
<h2>Concurso Título de Especialista em Anestesiologia</h2>
<br/>
<p>
    Prezado dr(a). {{ $candidato->nome }},<br> você foi candidato(a) no Concurso do Título de Especialista em Anestesiologia.<br>
    E de acordo com o edital, sua prova respondida pode ser obtida acessando o link <a href="<?= $_SERVER['HTTP_HOST'] ?>/pdf/prova?token={{ $token }}">Prova Respondida</a> e o gabarito em <a href="http://www.sbahq.org/resources/pdf/GABARITO_TEA2018.pdf">TEA 2018</a></p>.</p>
<p>Caso seu provedor/cliente de email bloqueie o link copie e cole esta url em seu navegador.<br>
    <?= $_SERVER['HTTP_HOST'] ?>/pdf/prova?token={{ $token }}</p>

<p>Atenciosamente,<br><br>Comissão de Ensino e Treinamento<br>Sociedade Brasileira de Anestesiologia</p>

</body>

</html>
