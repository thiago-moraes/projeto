<!DOCTYPE html>
<html>
<head>
    <title>Lembrete de senha</title>
</head>

<body>
<h2>Dados para acesso ao sistema de prova online SBA</h2>
<br/>
<p>Para acessar utilize os dados abaixo:</p>

<ul>
    <li>Nome: {{ $candidato->nome }}</li>
    <li>Usuário: Seu número de CPF</li>
    <li>Senha (Letras maiúsculas): {{ $candidato->pwd_plain_text }}</li>
    <li>Horário: {{ @formataData($candidato->matriz()->first()->dt_inicio)->format("d/m/Y - H:i") }}h </li>
    <li>Url: <a href="http://www.provasonlinesba.com.br/" target="blank">http://www.provasonlinesba.com.br/</a></li>
 </ul>

<p>Atenciosamente,<br><br>Comissão de Ensino e Treinamento<br>Sociedade Brasileira de Anestesiologia</p>

</body>

</html>
