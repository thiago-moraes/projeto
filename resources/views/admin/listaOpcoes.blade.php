@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <h2>Editar opção</h2>
            <hr>
        </div>
    </div>

    <div class="row">
         <p>{!! $questao->descricao !!}</p>
        <div class="col-md-12 col-sm-12 col-xs-12"><hr></div>
    @if(session('mensagem'))
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info"> {{ session('mensagem') }} </div>
    </div>
    @endif
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

            @if(count($dados) > 0)
                @foreach($dados as $key => $dado)
                    <tr>
                        <td width="1">{{ \App\Http\Controllers\OpcaoController::INDICE_ALFA[$key] }})</td>
                        <td colspan="3">
                            <form class="form-horizontal row" action="/admin/opcoes/{{ $dado->id_opcao }}" method="post">
                                {{  csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="col-md-9 col-sm-9 col-xs-12"><textarea name="descricao" id="" cols="30" rows="5" class="form-control">{{ $dado->descricao }} </textarea></div>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                <select name="valor" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                                    @foreach(\App\Http\Controllers\OpcaoController::RESPOSTA as $key => $item)
                                        <option value="{{ $key }}" {{ @verificaSelecionado($key, $dado->valor ?? '') }}> {{ $item }} </option>
                                    @endforeach
                                </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12">
                                    <button class="btn btn-success btn-block pull-right">Salvar</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="8">Nenhum registro encontrado</td></tr>
            @endif
            </tbody>
        <tfoot>
            <tr><td colspan="5" class="text-right"><a href="/admin/questoes" class="btn btn-info">Voltar</a> </td></tr>
        </tfoot>
        </table>
    </div>
    <script>
        jQuery(function($){
            $(".confirma").click(function(){
               return confirm("Deseja realmente " + $(this).attr("title"));
            });
        })
    </script>
</div>

@endsection
