@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <h2>Relatório de Provas online</h2>
        <hr>
    </div>
</div>
<div class="row">
    
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="btn-group">
            <a href="/admin/lista/provas" class="btn btn-default">Provas</a>
        </div>
    </div>


        <form method="get" action="" id="form-filtro" class="form-inline">

            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="form-group">
                    <label class="control-label">Ano ME</label><br>
                    <select name="me" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($me as $key => $item)
                            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['me'] ?? '') }}> {{ $item }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Tipo prova</label><br>
                    <select name="tipo" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($tipo as $key => $item)
                            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['tipo'] ?? '') }}> {{ $item }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">Status</label><br>
                    <select name="s" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($status as $key => $item)
                            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['s'] ?? '') }}> {{ $item }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Situação</label><br>
                    <select name="si" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($situacao as $key => $item)
                            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['si'] ?? '') }}> {{ $item }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Ano</label><br>
                    <select name="ano" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($anos as $item)
                            <option value="{{ $item->ano }}" {{ @verificaSelecionado($item->ano, $_GET['ano'] ?? date("Y")) }}> {{ $item->ano }} </option>
                        @endforeach
                    </select>
                </div>
               <!-- <div class="form-group">
                    <label class="control-label">Matriz</label><br>
                    <select name="ma" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($matrizes as $key => $item)
                            <option value="{{ $item['id_matriz'] }}" {{ @verificaSelecionado($item['id_matriz'], $_GET['ma'] ?? '') }}> {{ $item['descricao'] }} </option>
                        @endforeach
                    </select>
                </div> -->
                <div class="form-group">
                    <label class="control-label">Trimestre</label><br>
                    <select name="trim" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($trim as $key => $item)
                            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['trim'] ?? '') }}> {{ $item }}º Trimestre </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Cet</label><br>
                    <select name="cet" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($cets as $item)
                            <option value="{{ $item }}" {{ @verificaSelecionado($item, $_GET['cet'] ?? '') }}> {{ $item }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="input-group">
                    <label class="control-label">Candidato</label><br>
                    <input name="c" class="form-control" value="{{ request()->input('c') }}" placeholder="Nome do candidato">
                    <span class="input-group-btn"><button class="btn btn-success" style="margin-top: 1.6em;"><span class="glyphicon glyphicon-search"></span></button></span>
                </div>
            </div>
        </form>


    <div class="col-md-12 col-sm-12 col-xs-12">
        <hr>
    </div>
</div>
    @if(session('mensagem'))
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info"> {{ session('mensagem') }} </div>
    </div>
    @endif
    
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Candidato</th>
                    <th>Matriz</th>
                    <th>Situação</th>
                    <th>CET</th>
                    <th>Status Prova</th>
                    <th>Inicio</th>
                    <th>Fim</th>
                </tr>
            </thead>
            <tbody>
        @foreach($dados as $dado)
            <tr>
                <td>{{ $dado->nome }}</td>
                <td>{{ $dado->matriz()->descricao }}</td>
                <td>{{ isset($dado->sessao->user_id) ? 'Ativo' : 'Inativo' }}</td>
                <td>{{ $dado->cet == 0 ? '' : $dado->cet }}</td>
                <td>{{ $status[$dado->status] }}</td>
                <td>{{ $dado->created_at ? formataData($dado->created_at)->format('d/m/Y H:i:s') : '' }}</td>
                <td>{{ $dado->updated_at && $dado->status == 2 ? formataData($dado->updated_at)->format('d/m/Y H:i:s') : '' }}</td>
            </tr>
        @endforeach
            </tbody>
        <tfoot>
        <tr><th colspan="2">Total de registros encontrado: {{ $total }}</th><td colspan="15">{{ $link }}</td></tr>
        </tfoot>
        </table>
    </div>

</div>

@endsection
