@extends('layouts.relnotapdf')

@section('content')

    <table class="table table-striped" style="border-collapse: collapse;">
        <tr>
            <th>Nome</th>
            <th align="right">Nota</th>
            <th align="right">Aproveitamento</th>
        </tr>
        @foreach($dados as $dado)
        <tr>
            <td>{{ $dado->nome }}</td>
            <td align="right">{{ $dado->nota }}</td>
            <td align="right">{{ $dado->aproveitamento }}</td>
        </tr>
        @endforeach
        <tr><td colspan="2">Total de registros:</td><td align="right">{{ count( $dados ) }}</td></tr>
    </table>
@endsection
