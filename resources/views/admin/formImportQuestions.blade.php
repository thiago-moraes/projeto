@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <h2>Importador de questões</h2>
        <hr>
    </div>
    <div class="col-md-12">
        @if(session('mensagem'))
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-info"> {{ session('mensagem') }} </div>
        </div>
        @endif
        <form class="form-horizontal" method="post" action="" class="form-horizontal">
            <div class="form-group">
                <div class="col-4">
                    <label for="prova">Prova</label>
                    <select name="prova" id="prova" class="form-control">
                        <?php foreach($provas as $prova): ?>
                        <option value="<?= $prova->id ?>"><?= $prova->descricao ?></option>
                        <!-- <option value="me2trim">ME - 2º Trimestre</option>
                        <option value="me3trim">ME - 3º Trimestre</option>
                        <option value="me4trim">ME - 4º Trimestre</option>
                        <option value="tea">TEA</option>
                        <option value="tsa">TSA</option> -->
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-4">
                    <label for="ano">Ano</label>
                    <select name="ano" id="ano" class="form-control">
                        <?php for ($i = 2019; $i <= date("Y"); $i++) : ?>
                        <option value="<?= $i ?>" <?= verificaSelecionado($i, date('Y')) ?>><?= $i ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </form>
    </div>

</div>

@endsection