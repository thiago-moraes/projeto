@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <h2>Nova matriz</h2>
        <hr>
    </div>
    <div class="col-md-12">
        @if(session('mensagem'))
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-info"> {{ session('mensagem') }} </div>
        </div>
        @endif
        <form class="form-horizontal" method="{{ $method }}" action ="/admin/novo/matriz">
            @include('admin.conteudo-form-matriz')
        </form>
    </div>

</div>

@endsection
