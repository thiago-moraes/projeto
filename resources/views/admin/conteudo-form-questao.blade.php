<div class="form-group">
    {{ csrf_field() }}
    <input type="hidden" class="form-control" name="id" value="{{ $dados->id_questao }}">
    <div class="form-group">
        <div class="col-md-12">
            <label class="control-label">Questão</label>
            <textarea name="descricao" cols="30" rows="5" class="form-control">{!! old("descricao",$dados->descricao ?? '') !!}</textarea>
        </div>
    </div>

    <div class="form-group">

        <div class="col-md-1 {{ $errors->first('ano') ? 'has-error' : '' }}">
            <label class="control-label">Ano</label>
            <input type="text" name="ano" class="form-control" value="{{ old('ano', $dados->ano ?? date("Y")) }}" id="ano">
            <span id="helpBlock" class="help-block"> {{ $errors->first('ano') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('dificuldade') ? 'has-error' : '' }}">
            <label class="control-label">Dificuldade</label>
            <select name="dificuldade" class="form-control" id="dificuldade">
                @foreach(\App\Http\Controllers\QuestaoController::DIFICULDADE as $key => $value)
                <option value="{{ $key }}" {{ @verificaSelecionado( $key, old('dificuldade', $dados->dificuldade ?? '')) }}>{{ $value }}</option>
                @endforeach
            </select>
            <span id="helpBlock" class="help-block"> {{ $errors->first('dificuldade') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('id_tipo_prova') ? 'has-error' : '' }}">
            <label class="control-label">Tipo prova</label>
            <select name="id_tipo_prova" class="form-control" id="id_tipo_prova">
                @foreach(\App\Http\Controllers\MatrizController::TIPO_PROVA as $key => $value)
                    <option value="{{ $key }}" {{ @verificaSelecionado( $key, old('id_tipo_prova', $dados->id_tipo_prova ?? '')) }}>{{ $value }}</option>
                @endforeach
            </select>
            <span id="helpBlock" class="help-block"> {{ $errors->first('id_tipo_prova') }}</span>
        </div>

        <div class="col-md-6 {{ $errors->first('ponto') ? 'has-error' : '' }}">
            <label class="control-label">Ponto</label>
            <input type="text" name="ponto" class="form-control" value="{{ old('ponto', $dados->ponto, "") }}" id="ponto">
            <span id="helpBlock" class="help-block"> {{ $errors->first('ponto') }}</span>
        </div>

        <div class="col-md-1 {{ $errors->first('ind_me') ? 'has-error' : '' }}">
            <label class="control-label">Ano ME</label>
            <select name="ind_me" class="form-control" id="ind_me">
                @foreach(\App\Http\Controllers\MatrizController::IND_ME as $key => $value)
                    <option value="{{ $key }}" {{ @verificaSelecionado( $key, old('ind_me', $dados->ind_me ?? '')) }}>{{ $value }}</option>
                @endforeach
            </select>
            <span id="helpBlock" class="help-block"> {{ $errors->first('dificuldade') }}</span>
        </div>

    </div>

    <div class="form-group">

        <div class="col-md-12">
            <label class="control-label">Justificativa</label>
            <textarea name="justificativa" id="" cols="30" rows="7" class="form-control">{!! old("justificativa",$dados->justificativa ?? '') !!}</textarea>
        </div>


        <div class="col-md-12 {{ $errors->first('autor') ? 'has-error' : '' }}">
            <label class="control-label">Autor</label>
            <select name="autor" class="form-control" id="autor">
                <option value="">-</option>
                @foreach($autores as $value)
                    <option value="{{ $value->MATRICULA }}" {{ @verificaSelecionado( $value->MATRICULA, old('autor', $dados->autor ?? '')) }}>{{ $value->NOME }}</option>
                @endforeach
            </select>
            <span id="helpBlock" class="help-block"> {{ $errors->first('id_tipo_prova') }}</span>
        </div>

    </div>
</div>

<div class="row">
    <div class="form-group">
        <div class="col-md-offset-10 col-md-2">
            <button type="submit" class="btn btn-success btn-block">Salvar</button>
        </div>
    </div>
</div>