 <style>
        
        .table {
            
            width: 100%;
        }
        
        .table tr {
            line-height: 2em;
        }
        
        .table tr:nth-child(odd) {
            
            border: solid 1px #e6e6e6;
            background: #e6e6e6;
            line-height: 2em;
        }
        
        .topo table{
            width: 100%;
        }

        
        h2 {
            margin: 2em;
            text-align: center;
        }
        
        .divisor {
            width: 100%;
        }
        
        .rodape{
            position:fixed;
            bottom: -70px;
            width: 100%;
            height: 50px;
            text-align: center;
            font-size: .8em;
        }
    </style>
    <div class="topo">
        <table>
            <tr>
                <td style="width: 12em"><img src="{{ public_path("/images/logo-sba.jpg") }}" width="160"></td>
                <td>
                    <table>
                        <tr><td>Nome:</td><td colspan="2">{{ $candidato->nome }}</td></tr>
                        <tr><td style="width: 3em">Data:</td><td colspan="2"> {{ formataData($prova->created_at)->format("d/m/Y") }}</td></tr>
                    </table>
                </td>
            </tr>

        </table>
    </div>

    <div class="divisor">
        <h3 style="text-align: center">Relatório de respostas do candidato.</h3>
    </div>
    <table class="table" style="font-size: .8em">
        @foreach ($questoes as $key => $value)
        <tr><td style="margin: .5em; font-weight: bold;">{{ $key+1 }}) {!! $value->descricao !!}</td></tr>
        <tr>
            <td>
                @foreach(opcoesComResposta($value->id_prova_questao) as $i => $dado)
                
                @if ($dado->valor === 2)  *  @endif
                {!! $dado->descricao !!}<br>
                @endforeach
            </td>
        </tr>
       @endforeach
       <tr><td>Legenda</td></tr>
       <tr><td>Sua respostas está marcada com asterisco (*)</td></tr>
    </table>
    <div class="rodape">
        Documento gerado em <?= date("d/m/Y") ?> para <?= $candidato->nome ?>.
    </div>