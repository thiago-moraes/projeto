@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <h2>Relatório de Sessões abertas.</h2>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
        <form method="get" action="" id="form-filtro" class="form-inline">
                <div class="input-group">
                    <label class="control-label">Candidato</label><br>
                    <input name="c" class="form-control" value="{{ request()->input('c') }}" placeholder="Nome do candidato">
                    <span class="input-group-btn"><button class="btn btn-success" style="margin-top: 1.6em;"><span class="glyphicon glyphicon-search"></span></button></span>
                </div>
        </form>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6"><br>
        <form method="POST" action="sessoes/remove" class="form-inline">
            {{ method_field("delete") }}
            {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{Auth()->user()->id }}">
            <button class="btn btn-danger confirma" title="Remover todas as sessões"><i class="glyphicon glyphicon-remove"></i> Encerrar sessões</button>
        </form>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <hr>
    </div>
</div>
    @if(session('mensagem'))
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info"> {{ session('mensagem') }} </div>
    </div>
    @endif
    
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Candidato</th>
                    <th>IP</th>
                    <th>Browser</th>
                    <th>Status Prova</th>
                    <th>Inatividade</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
        @foreach($dados as $key => $dado)
            @if(Auth::user()->id != $dado->user_id)
            <tr>
                <td>{{ $dado->nome }}</td>
                <td>{{ $dado->ip_address }}</td>
                <td>{{ $dado->user_agent }}</td>
                <td>{{ (new \DateTime())->setTimestamp($dado->last_activity)->diff(now())->format("%H:%I:%S") }}</td>
                <td>
                    <form action="sessoes/{{ $dado->user_id }}" method="post">
                        @method("delete")
                        @csrf
                        <input type="hidden" name="user_id" value="{{ $dado->user_id }}">
                        <button class="btn btn-danger confirma" title="Excluir a sessão de {{ $dado->nome }}"><i class="glyphicon glyphicon-remove"></i></button>
                    </form>
                </td>
            </tr>
            @endif
        @endforeach
            </tbody>
        <tfoot>
        <tr><th colspan="2">Total de registros encontrado: {{ $total }}</th><td colspan="15">{{ $link }}</td></tr>
        </tfoot>
        </table>
    </div>
<script>
    jQuery(function($){
        $(".confirma").click(function(){
           return confirm("Deseja realmente " + $(this).attr("title"));
        });
    });
</script>

@endsection
