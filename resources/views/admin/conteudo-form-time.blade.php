<div class="form-group">

    {{ csrf_field() }}

    <input type="hidden" class="form-control" name="id_candidato" value="{{ $dados->getCandidato()->id }}">
    
    <div class="col-md-12">
        <label class="control-label">Candidato</label>
        <div class="form-control">{{ $dados->getCandidato()->nome }}</div>
    </div>
    <?php $dtFim = verificaTimeExtension($dados->getCandidato()->id); ?>
    @if($dtFim != '')
        <?php $dtFim = formataData( $dtFim )->format("d/m/Y H:i:s") ?>
    @else
        <?php $dtFim = formataData( $dados->getCandidato()->matriz()->first()->dt_fim )->format("d/m/Y H:i:s") ?>
    @endif

    <div class="col-md-12 {{ $errors->first('dt_fim') ? 'has-error' : '' }}">
        <label class="control-label">Fim da prova</label>
        <input type="text" name="dt_fim" class="form-control" value="{{ old('dt_fim', $dtFim) }}" id="dt-fim">
        <span id="helpBlock" class="help-block"> {{ $errors->first('dt_fim') }}</span>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-10 col-md-2">
        <button type="submit" class="btn btn-success btn-block">Salvar</button>
    </div>
</div>

<script src="{{ asset("/js/jquery.min.js") }}"></script>
<script src="{{ asset("/js/jquery.maskedinput.min.js") }}"></script>
<script>
    jQuery(function($){
        $("#dt-fim").mask("99/99/9999 99:99:99",{
            placeholder:"00/00/0000 00:00:00"
        });
    });

</script>

