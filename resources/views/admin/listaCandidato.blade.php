@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <h2>Relatório de Candidatos</h2>
        <hr>
    </div>
</div>

<form method="get" action="" id="form-filtro" class="form-inline text-right">
    <select name="tipo" onchange="document.getElementById('form-filtro').submit()" class="form-control">
        <option value="">-</option>
        @foreach($tipo as $key => $item)
            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['tipo'] ?? '') }}> {{ $item }} </option>
        @endforeach
    </select>
    
    <select name="ano" onchange="document.getElementById('form-filtro').submit()" class="form-control">
        <option value="">-</option>
        @foreach($anos as $item)
            <option value="{{ $item->ano }}" {{ @verificaSelecionado($item->ano, $_GET['ano'] ?? date("Y")) }}> {{ $item->ano }} </option>
        @endforeach
    </select>

    <select name="me" onchange="document.getElementById('form-filtro').submit()" class="form-control">
        <option value="">-</option>
        @foreach($me as $key => $item)
            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['me'] ?? '') }}> {{ $item }} </option>
        @endforeach
    </select>

    <select name="trim" onchange="document.getElementById('form-filtro').submit()" class="form-control">
        <option value="">-</option>
        @foreach($trim as $key => $item)
            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['trim'] ?? '') }}> {{ $item }}º Trimestre </option>
        @endforeach
    </select>

    <div class="input-group">
        <input name="c" class="form-control" value="{{ request()->input('c') }}">
        <span class="input-group-btn"><button class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button></span>
    </div>

</form>

<div class="row">

    @if(session('mensagem'))
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info"> {{ session('mensagem') }} </div>
    </div>
    @endif
    
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>Celular</th>
                    <th>Matriz</th>
                    <th>Ano ME</th>
                    <th>Trim</th>
                    <th colspan="4"></th>
                </tr>
            </thead>
            <tbody>
        @foreach($dados as $dado)
            <tr>
                <td>{{ $dado->getNome() }}</td>
                <td>{{ $dado->getCPF() }}</td>
                <td>{{ $dado->getCelular() }}</td>
                <td>{{ $dado->getDadosMatriz()->getDescricao() }}</td>
                <td>{{ $me[$dado->getDadosMatriz()->getIndME()] }}</td>
                <td>{{ $dado->getDadosMatriz()->getTrim() }}</td>
                <td>
                    <form action="/admin/delete/candidato" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $dado->getId() }}">
                        <div class="btn-group pull-right">
                            <a href='/admin/candidato/{{ $dado->getId() }}/edit' class="btn btn-primary" title="Editar cadastro"><span class="glyphicon glyphicon-edit"></span></a>
                            <a href="/admin/candidato/{{ $dado->getId() }}/mailprova" class="btn btn-warning" title="Enviar prova"><span class="glyphicon glyphicon-file"></span></a>
                            <a href="/admin/send/senha/{{ $dado->getId() }}" class="btn btn-info confirma" title="Senha por e-mail"><span class="glyphicon glyphicon-envelope"></span></a>
                            <a href="/admin/send/senha/sms/{{ $dado->getId() }}" class="btn btn-success confirma" title="Senha por SMS"><span class="glyphicon glyphicon-phone"></span></a>
                            <button class="btn btn-danger confirma" title="Excluir candidato"><span class="glyphicon glyphicon-trash"></span></button>
                        </div>
                        
                </form>
            </tr>
        @endforeach
            </tbody>
        <tfoot>
            <tr>
                <th colspan="1">Total de registros encontrados: {{ $total }}</th>
                <th colspan="6">{{ $links }}</th></tr>
        </tfoot>
        </table>
    </div>

</div>
<script>
    jQuery(function($){
        $(".confirma").click(function(){
           return confirm("Deseja realmente " + $(this).attr("title"));
        });
    })
</script>
@endsection