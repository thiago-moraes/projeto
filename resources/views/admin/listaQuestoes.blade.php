@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <h2>Relatório de Questões</h2>
            <hr>
        </div>
    </div>

    <div class="row">


        <div class="col-md-12 col-xs-12 col-sm-12">

            <form method="get" action="" id="form-filtro" class="form-inline">
                <div class="input-group">
                    <label class="control-label">Ano</label>
                    <select name="ano" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($anos as $ano)
                            <option value="{{ $ano }}" {{ @verificaSelecionado($ano, $_GET['ano'] ?? date("Y")) }}> {{ $ano }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group">
                    <label for="" class="control-label">Ano ME</label>
                    <select name="ind" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($periodo as $key => $value)
                            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['ind'] ?? '') }}> {{ $value }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group">
                    <label for="tipo" class="control-label">Tipo</label>
                    <select name="t" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                    <option value="">-</option>
                        @foreach($tipoProva as $key => $value)
                            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['t'] ?? '') }}> {{ $value }} </option>
                        @endforeach
                    </select>
                </div>
            </form>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12"><hr></div>
    @if(session('mensagem'))
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info"> {{ session('mensagem') }} </div>
    </div>
    @endif
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Questão</th>
                    <th>Tipo</th>
                    <th>Ano</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

            @if(count($dados) > 0)
                @foreach($dados as $dado)
                <tr>
                    <td>{!! $dado->descricao !!}</td>
                    <td>{{ \App\Http\Controllers\MatrizController::TIPO_PROVA[$dado->id_tipo_prova] }}</td>
                    <td>{{ $dado->ano }}</td>
                    <td style="width: 10em;">
                        <span class="btn-group pull-right">
                            <a href='/admin/questoes/{{ $dado->id_questao }}/edit' class="btn btn-warning" title="Editar prova"><span class="glyphicon glyphicon-edit"></span></a>
                            <a href="/admin/opcoes/{{ $dado->id_questao }}" class="btn btn-success" title="Mostrar opções"><span class="glyphicon glyphicon-list"></span></a>
                        </span>
                    </td>
                </tr>
                @endforeach
            @else
                <tr><td colspan="8">Nenhum registro encontrado</td></tr>
            @endif
            </tbody>
        <tfoot>
            <tr><td colspan="15">{{ $dados->links() }}</td></tr>
        </tfoot>
        </table>
    </div>
    <script>
        jQuery(function($){
            $(".confirma").click(function(){
               return confirm("Deseja realmente " + $(this).attr("title"));
            });
        })
    </script>
</div>

@endsection
