@extends('layouts.admin')

@section('content')

<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    <h2>Relatório de Matrizes</h2>
    <hr>
  </div>
</div>
@if(session('mensagem'))
<div class="alert alert-info"> {{ session('mensagem') }} </div>
@endif
<div class="row">

  <form method="get" action="/admin/lista/matriz" id="form-filtro">
    <div class="col-xs-12 col-md-3 col-sm-3">
      <label class="control-label">Tipo</label>
      <select name="tipo" class="form-control filtros">
       <option value="0"> - </option>
       @foreach($tipo as $t)
       <option value="{{ $t->id_tipo_prova }}" {{ verificaSelecionado(request()->input('tipo'), $t->id_tipo_prova) }}> {{ $t->descricao }}</option>
       @endforeach
     </select>
   </div>

   <div class="col-xs-12 col-md-3 col-sm-3">
     <label class="control-label">Status</label>
     <select name="status" class="form-control filtros">
       <option value="0"> - </option>
       @foreach($status->get() as $s)
       <option value="{{ $s->codigo }}" {{ verificaSelecionado(request()->input('status'), $s->codigo) }}> {{ $s->descricao }}</option>
       @endforeach
     </select>
   </div>

   <div class="col-xs-12 col-md-3 col-sm-3">
    <label class="control-label">Ano</label>
    <select name="ano" class="form-control filtros">
      <option value="0"> - </option>
      @foreach($ano as $a)
      <option value="{{ $a[0]->ano }}" {{ verificaSelecionado(request()->input('ano'), $a[0]->ano ) }}> {{ $a[0]->ano }}</option>
      @endforeach
    </select>
  </div>

  <div class="col-xs-12 col-md-3 col-sm-3">
   <label class="control-label">Descrição</label>
   <div class="input-group">
    <input name="descricao" class="form-control" value="{{ request()->input('descricao')}}">
    <span class="input-group-btn">
      <button class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
    </span>
  </div>
</div>
</form>

<div class="col-md-12">
  <table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>Descrição</th>
        <th>Tipo</th>
        <th>Status</th>
        <th>Ano</th>
        <th>Questão</th>
        <th>Trim</th>
        <th>Início</th>
        <th>Fim</th>
      </tr>
    </thead>
    <tbody>
      @foreach($dados as $dado)
      <tr>
        <td>{{ $dado->getIdMatriz() }}</td>
        <td>{{ $dado->getDescricao() }}</td>
        <td>{{ $dado->getTipoProva()->descricao }}</td>
        <td>{{ $dado->getStatus()->descricao }}</td>
        <td>{{ $dado->getAno() }}</td>
        <td>{{ $dado->getAnoQuestao() }}</td>
        <td>{{ $dado->getTrim() }}</td>
        <td>{{ $dado->getDtInicio()->format("d/m/Y H:i:s") }}</td>
        <td>{{ $dado->getDtFim()->format("d/m/Y H:i:s") }}</td>
        <td style="width: 22em;">
          <form action="/admin/delete/matriz" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="id_matriz" value="{{ $dado->getIdMatriz() }}">
            <div class="row">
              
            <div class="btn-group pull-right">
              <a href='/admin/edit/matriz/{{ $dado->getIdMatriz() }}' class="btn btn-primary" title="Editar matriz"><span class="glyphicon glyphicon-edit"></span></a>
              <a href='/admin/matriz/{{ $dado->getIdMatriz() }}/nota' target="_blank" class="btn btn-info" title="PDF Notas"><span class="glyphicon glyphicon-education"></span></a>
              <a href='/admin/candidato/email/provas/{{ $dado->getIdMatriz() }}' class="btn btn-success confirma" title="Enviar a prova para os candidatos"><span class="glyphicon glyphicon-file"></span></a>
              <a href='../util/gerarsenhas/{{ $dado->getIdMatriz() }}' class="btn btn-warning confirma" title="Gerar e encriptar senhas"><span class="glyphicon glyphicon-random"></span></a>
              <a href='../util/encriptarsenhas/{{ $dado->getIdMatriz() }}' class="btn btn-danger confirma" title="Encriptar as senhas"><span class="glyphicon glyphicon-lock"></span></a>
            </div>
          </div>
          <div class="row">
            
            <div class="btn-group pull-right">
              <a href='/admin/criaprovapormatriz/{{ $dado->getIdMatriz() }}' class="btn btn-primary confirma" title="Criar provas"><span class="glyphicon glyphicon-asterisk"></span></a>

              <a href='../util/enviarsenhas/{{ $dado->getIdMatriz() }}' class="btn btn-info confirma" title="Senhas por e-mail"><span class="glyphicon glyphicon-envelope"></span></a>
              <a href="../util/enviarsenhas/sms/{{ $dado->getIdMatriz() }}" class="btn btn-success confirma" title="Senhas por SMS"><span class="glyphicon glyphicon-phone"></span></a>
              <a href="/admin/provas/finaliza/{{ $dado->getIdMatriz() }}" class="btn btn-warning confirma" title="Finalizar provas"><span class="glyphicon glyphicon-remove"></span></a>
              <button class="btn btn-danger confirma" title="Excluir matriz"><span class="glyphicon glyphicon-trash"></span></button>
            </div>
          </div>
          <div class="row">
            
            <div class="btn-group pull-right">
              <a href='/admin/pdf/provas/{{ $dado->getIdMatriz() }}' class="btn btn-warning confirma" title="Gerar os PDF's da provas"><span class="glyphicon glyphicon-download-alt"></span></a>
            </div>
          </div>
          </form>

        </td>
      </tr>
      @endforeach
    </tbody>
    <tfoot>
      <tr><td colspan="15">{{ $links }}</td></tr>
    </tfoot>
  </table>
</div>

</div>


<script>
 $(document).ready(function(){

   $('.filtros').on('change',function(){
     $("#form-filtro").submit();
   });
   $(".confirma").click(function(){
    return confirm("Deseja realmente " + $(this).attr("title"));
  });
 });
</script>

@endsection
