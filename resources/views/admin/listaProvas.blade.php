@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <h2>Relatório de Provas</h2>
            <hr>
        </div>
    </div>

    <div class="row">

        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="btn-group">
                <a href="/admin/prova/online" class="btn btn-default">Online</a>
            </div>
        </div>


        <form  method="get" action="" id="form-filtro" class="form-inline">
            <div class="col-md-12 col-xs-12 col-sm-12">

                <div class="form-group">
                    <label class="control-label">Tipo prova</label><br>
                    <select name="tipo" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($tipo as $key => $item)
                            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['tipo'] ?? '') }}> {{ $item }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Ano</label><br>
                    <select name="ano" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($anos as $item)
                            <option value="{{ $item->ano }}" {{ @verificaSelecionado($item->ano, $_GET['ano'] ?? date("Y")) }}> {{ $item->ano }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Ano ME</label><br>
                    <select name="me" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($me as $key => $item)
                            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['me'] ?? '') }}> {{ $item }} </option>
                        @endforeach
                    </select>
                </div>

                
                <div class="form-group">
                    <label class="control-label">Trimestre</label><br>
                    <select name="trim" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($trim as $key => $item)
                            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['trim'] ?? '') }}> {{ $item }}º Trimestre </option>
                        @endforeach
                    </select>
                </div>
                
                <div class="form-group">
                    <label class="control-label">Cet</label><br>
                    <select name="cet" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($cets as $item)
                            <option value="{{ $item }}" {{ @verificaSelecionado($item, $_GET['cet'] ?? '') }}> {{ $item }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Status</label><br>
                    <select name="s" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        @foreach($status as $key => $item)
                            <option value="{{ $key }}" {{ @verificaSelecionado($key, $_GET['s'] ?? '') }}> {{ $item }} </option>
                        @endforeach
                    </select>
                </div>

                <div class="input-group">
                    <label class="control-label">Candidato</label><br>
                    <input name="c" class="form-control" value="{{ request()->input('c') }}" placeholder="Nome do candidato">
                    <span class="input-group-btn"><button class="btn btn-success" style="margin-top: 1.6em;"><span class="glyphicon glyphicon-search"></span></button></span>
                </div>
            </div>
        </form>

        <div class="col-md-12 col-sm-12 col-xs-12"><hr></div>
    @if(session('mensagem'))
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info"> {{ session('mensagem') }} </div>
    </div>
    @endif
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Candidato</th>
                    <th>Matriz</th>
                    <th>Status</th>
                    <th>Ano</th>
                    <th>Ano ME</th>
                    <th>CET</th>
                    <th>Trim</th>
                    <th>Tipo</th>
                    <th>Início</th>
                    <th>Fim</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

            @if(count($dados) > 0)
                @foreach($dados as $dado)

                    <tr>
                        <td>{{ $dado->getCandidato()->nome }}</td>
                        <td>{{ $dado->getMatriz()->descricao }}</td>
                        <td>{{ $status[$dado->getStatus()] }}</td>
                        <td>{{ $dado->getMatriz()->ano }}</td>
                        <td>{{ $me[$dado->getMatriz()->ind_me] }}</td>
                        <td>{{ $dado->getCandidato()->cet }}</td>
                        <td>{{ $dado->getMatriz()->trim }}</td>
                        <td>{{ $tipo[$dado->getMatriz()->id_tipo_prova] }}</td>
                        <td>{{ $dado->getInicio()->format('d/m/Y H:i:s') }} </td>
                        <td> {{ $dado->getStatus() == 2 ? $dado->getFim()->format('d/m/Y H:i:s') : '' }}</td>
                        <td>
                            <form action="/admin/delete/prova" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="id_prova" value="{{ $dado->getIdProva() }}">
                                <div  class="btn-group pull-right">
                                    <a href='/admin/edit/prova/{{ $dado->getIdProva() }}' class="btn btn-primary" title="Editar prova"><span class="glyphicon glyphicon-edit"></span></a>
                                    <a href="/admin/add/time/{{ $dado->getCandidato()->id }}" class="btn btn-success" title="Adicionar tempo a prova"><span class="glyphicon glyphicon-time"></span></a>
                                    <a href="/admin/cancela/prova/{{ $dado->getIdProva() }}" class="btn btn-warning confirma" title="Finalizar a prova"><span class="glyphicon glyphicon-remove-circle"></span></a>
                                    <button class="btn btn-danger confirma" title="Excluir a prova "><span class="glyphicon glyphicon-trash"></span></button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr><td colspan="8">Nenhum registro encontrado</td></tr>
            @endif
            </tbody>
        <tfoot>
        <tr><th colspan="2">Total de registros encontrado: {{ $total }}</th><td colspan="15">{{ $link }}</td></tr>
        </tfoot>
        </table>
    </div>
    <script>
        jQuery(function($){
            $(".confirma").click(function(){
               return confirm("Deseja realmente " + $(this).attr("title"));
            });
        })
    </script>
</div>

@endsection
