<div class="form-group">

    {{ csrf_field() }}
    
    <input type="hidden" class="form-control" name="id_prova" value="{{ $dados->getIdProva() }}">
    
    <div class="col-md-12">
        <label class="control-label">Candidato</label>
        <div class="form-control">{{ $dados->getCandidato()->nome }}</div>
    </div>
    
    <div class="col-md-12 {{ $errors->first('id_matriz') ? 'has-error' : '' }}">
        <label class="control-label">Matriz</label>
        <select name="id_matriz" class="form-control">
            <option value=""> - </option>
            @foreach($matrizes as $matriz)
            <option value="{{ $matriz->id_matriz }}" {{ verificaSelecionado($matriz->id_matriz, old('id_matriz', $dados->getIdMatriz())) }} >{{ $matriz->descricao }}</option>
            @endforeach
        </select>
        <span id="helpBlock" class="help-block"> {{ $errors->first('id_matriz') }}</span>
    </div>
    
    <div class="col-md-12 {{ $errors->first('status') ? 'has-error' : '' }}">
        <label class="control-label">Status</label>
        
        <select name="status" class="form-control">
            <option value=""> - </option>
            @foreach($codigos as $codigo)
            <option value="{{ $codigo->codigo }}" {{ verificaSelecionado($codigo->codigo, old('status', $dados->getStatus())) }} >{{ $codigo->descricao }}</option>
            @endforeach
        </select>
        <span id="helpBlock" class="help-block"> {{ $errors->first('status') }}</span>
    </div>
</div>
    

<div class="form-group">
    <div class="col-md-offset-10 col-md-2">
        <button type="submit" class="btn btn-success btn-block">Salvar</button>
    </div>
</div>