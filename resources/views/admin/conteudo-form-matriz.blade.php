


    {{ csrf_field() }}
    <input type="hidden" name="id_matriz" value="{{ $dados->getIdMatriz() }}">
    
    <div class="form-group">
        <div class="col-md-12 {{ $errors->first('descricao') ? 'has-error' : '' }}">
            <label class="control-label">Descrição</label>
            <input class="form-control" name="descricao" value="{{ old('descricao', $dados->getDescricao()) }}" placeholder="Descrição da prova">
            <span id="helpBlock" class="help-block"> {{ $errors->first('descricao') }}</span>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-md-6 {{ $errors->first('dt_inicio') ? 'has-error' : '' }}">
            <label class="control-label">Início</label>
            <input class="form-control" name="dt_inicio" value="{{ old('dt_inicio', $dados->getDtInicio()->format('d/m/Y H:i:s')) }}" placeholder="dd/mm/yyyy hh:mm:ss">
            <span id="helpBlock" class="help-block"> {{ $errors->first('dt_inicio') }}</span>
        </div>

        <div class="col-md-6 {{ $errors->first('dt_fim') ? 'has-error' : '' }}">
            <label class="control-label">Fim</label>
            <input class="form-control" name="dt_fim" value="{{ old('dt_fim', $dados->getDtFim()->format('d/m/Y H:i:s')) }}" placeholder="dd/mm/yyyy hh:mm:ss">
            <span id="helpBlock" class="help-block"> {{ $errors->first('dt_fim') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('ano') ? 'has-error' : '' }}">
            <label class="control-label">Ano Matriz</label>
            <input class="form-control" name="ano" value="{{ old('ano', $dados->getAno()) }}" placeholder="yyyy">
            <span id="helpBlock" class="help-block"> {{ $errors->first('ano') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('ano_questao') ? 'has-error' : '' }}">
            <label class="control-label">Ano Questão</label>
            <input class="form-control" name="ano_questao" value="{{ old('ano_questao', $dados->getAnoQuestao()) }}" placeholder="yyyy">
            <span id="helpBlock" class="help-block"> {{ $errors->first('ano_questao') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('ponto_ini') ? 'has-error' : '' }}">
            <label class="control-label">Ponto Início</label>
            <input class="form-control" name="ponto_ini" value="{{ old('ponto_ini', $dados->getPontoIni()) }}" placeholder="Ponto inicial">
            <span id="helpBlock" class="help-block"> {{ $errors->first('ponto_ini') }}</span>
        </div>

        <div class="col-md-2  {{ $errors->first('ponto_fim') ? 'has-error' : '' }}">
            <label class="control-label">Ponto Fim</label>
            <input class="form-control" name="ponto_fim" value="{{ old('ponto_fim',  $dados->getPontoFim()) }}" placeholder="Ponto Final">
            <span id="helpBlock" class="help-block"> {{ $errors->first('ponto_fim') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('trim') ? 'has-error' : '' }}">
            <label class="control-label">Trimestre</label>
            <input class="form-control" name="trim" value="{{ old('tim', $dados->getTrim()) }}" placeholder="Trimestre">
            <span id="helpBlock" class="help-block"> {{ $errors->first('trim') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('ind_me') ? 'has-error' : '' }}">
            <label class="control-label">Ano/ME</label>
            <input class="form-control" name="ind_me" value="{{ old('ind_me', $dados->getIndME()) }}" placeholder="Ano ME">
            <span id="helpBlock" class="help-block"> {{ $errors->first('ind_me') }}</span>
        </div>
    </div>
    
    <div class="form-group">
        
        <div class="col-md-2 {{ $errors->first('qtd_facil') ? 'has-error' : '' }}">
            <label class="control-label">Total Fácil</label>
            <input class="form-control" name="qtd_facil" value="{{ old('qtd_facil', $dados->getQtdFacil()) }}" placeholder="Total de questões fáceis">
            <span id="helpBlock" class="help-block"> {{ $errors->first('qtd_facil') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('qtd_medio') ? 'has-error' : '' }}">
            <label class="control-label">Total Médio</label>
            <input class="form-control" name="qtd_medio" value="{{ old('qtd_medio', $dados->getQtdMedio()) }}" placeholder="Total de questões médias">
            <span id="helpBlock" class="help-block"> {{ $errors->first('qtd_medio') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('qtd_dificil') ? 'has-error' : '' }}">
            <label class="control-label">Total Difícil</label>
            <input class="form-control" name="qtd_dificil" value="{{ old('qtd_dificil', $dados->getQtdDificil()) }}" placeholder="Total de questões dificeis">
            <span id="helpBlock" class="help-block"> {{ $errors->first('qtd_dificil') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('qtd_questoes') ? 'has-error' : '' }}">
            <label class="control-label">Total Questões</label>
            <input class="form-control" name="qtd_questoes" value="{{ old('qtd_questoes', $dados->getQtdQuestoes()) }}" placeholder="Total de questões da prova">
            <span id="helpBlock" class="help-block"> {{ $errors->first('qtd_questoes') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('id_tipo_prova') ? 'has-error' : '' }}">
            <label class="control-label">Tipo de Prova</label>
            <select class="form-control" name="id_tipo_prova">
                <option value="">-</option>
                @foreach($tipos as $tipo)
                <option value="{{ $tipo->id_tipo_prova }}" {{ verificaSelecionado($tipo->id_tipo_prova, old('id_tipo_prova', $dados->id_tipo_prova)) }}>{{ $tipo->descricao }}</option>
                @endforeach
            </select>
            <span id="helpBlock" class="help-block"> {{ $errors->first('id_tipo_prova') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('status') ? 'has-error' : '' }}">
            <label class="control-label">Status</label>
            <select class="form-control" name="status">
                <option value="">-</option>
                @foreach($status as $value)
                <option value="{{ $value->codigo }}" {{ verificaSelecionado($value->codigo, old('status', $dados->getStatus())) }}>{{ $value->descricao }}</option>
                @endforeach
            </select>
            <span id="helpBlock" class="help-block"> {{ $errors->first('status') }}</span>
        </div>

        <div class="col-md-2 {{ $errors->first('senha') ? 'has-error' : '' }}">
            <label class="control-label">Senha</label>
            <input type="password" name="password" class="form-control">
            <span id="helpBlock" class="help-block"> {{ $errors->first('status') }}</span>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-md-offset-10 col-md-2">
            <button type="submit" class="btn btn-success btn-block">Salvar</button>
        </div>
    </div>