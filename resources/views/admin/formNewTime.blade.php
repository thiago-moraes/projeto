@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <h2>Adicionar tempo a prova</h2>
        <hr>
    </div>
    <div class="col-md-12">
        @if(session('mensagem'))
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-info"> {{ session('mensagem') }} </div>
        </div>
        @endif
        <form class="form-horizontal" method="post" action ="/admin/add/time">
           @include('admin.conteudo-form-time')
        </form>
    </div>

</div>

@endsection
