@extends('layouts.admin')

@section('content')

<div class="row">
    <h2>Editar questão</h2>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <hr>
    </div>
    <div class="col-md-12">
        @if(session('mensagem'))
            <div class="alert alert-info"> {{ session('mensagem') }} </div>
        @endif
        <form class="form-horizontal" method="post" action ="/admin/questoes/{{ $dados->id_questao }}">
            {{ @method_field('PUT') }}
           @include('admin.conteudo-form-questao')
        </form>
    </div>

</div>

<script src="{{ asset("/js/jquery.min.js") }}"></script>
<script src="{{ asset("/js/jquery.maskedinput.min.js") }}"></script>
<script>
    jQuery(function($){
        $("#dt-fim").mask("99/99/9999 99:99:99",{
            placeholder:"00/00/0000 00:00:00"
        });
    });
</script>
@endsection

