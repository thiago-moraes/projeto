<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Provas SBA</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style>
            ul li {
                padding: .8em 0px;
                font-size: 1.1em;
                font-weight: normal;
            }
            ul {
                list-style: none;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height"><br><br>
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-4 col-md-4"><img src="{{ asset('images/logo-sba.jpg') }}" alt="SBA 70 anos" width="250"></div>
                </div><br>
                <div class="col-md-8 col-md-offset-2">
                    <h3 class="text-center">Prova <i>online</i> SBA.</h3>
                    <div class="links">
                        <ul>
                            <li>Para acesso a prova utilize seu cpf para usuário e senha</li>
                            <li>Ao fazer login, o sistema criará sua prova. <i style='color:red; font-weight: bold;'>Por favor aguarde</i>.</li>
                            <li>Durante o tempo de prova você poderá navegar pelas questões respondendo ou não as opções</li>
                            <li>O sistema permite questões parcialmente repondidas, ou seja, as opções podem ficar em branco.</li>
                            <li>Ao avançar ou retroceder na prova as respostas serão confirmadas.</li>
                            <li>Ao terminar de responder as questões clique no botão <button class="btn btn-danger">Finalizar Prova.</button> Antes de finalizar, revise sua prova.</li>
                            <li>Ao finalizar a prova o sistema solicitará confirmação.</li>
                            <li>Finalizando a prova o sistema irá calcular sua nota e aproveitamento.</li>
                            <li><br><div class="col-md-4 col-md-offset-4"><a href="{{ url('/login') }}" class="btn btn-info btn-block">Login</a></div></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
