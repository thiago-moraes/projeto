<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use \App\TWW\TWWLibrary;
use \App\Channels\SMSChannel;

class ReminderSenhaSMS extends Notification implements ShouldQueue
{
    use Queueable;

    protected $candidatos;
    const ACESSO = ["numusu"=> "sba", "senha"=> "panda2004", "url"=> "https://webservices2.twwwireless.com.br/reluzcap/", "port"=>"443", "SOAPAction"=> "https://www.twwwireless.com.br/reluzcap/wsreluzcap/"];
    protected $message;
    public $tries = 3;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct( $message )
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via()
    {
        return [SMSChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toSMS($notifiable)
    {
        return (string) (new TWWLibrary(self::ACESSO))
            ->enviaSMS($notifiable->celular, $this->message, 0);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
