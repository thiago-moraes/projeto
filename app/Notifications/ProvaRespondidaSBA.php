<?php

namespace App\Notifications;

use App\Mail\PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProvaRespondidaSBA extends Notification implements ShouldQueue
{
    use Queueable;
    public $token;
    public $matriz;
    public $tries = 3;
    /*
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct( $token, $matriz )
    {
       $this->token = $token;
       $this->matriz = $matriz;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new PDF($notifiable, $this->token, $this->matriz))->subject("Prova respondida SBA.")->to($notifiable->email, $notifiable->nome);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
