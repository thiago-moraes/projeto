<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notas extends Model
{

	protected $table = 'notas';
	protected $primaryKey = 'id_nota';
	protected $fillable = [
            'id_nota','id_prova', 'id_matriz', 'id_candidato', 'nota', 'aproveitamento'
	];
        public $timestamps = false;
}
