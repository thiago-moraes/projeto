<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opcao extends Model
{
    protected $table = 'opcao';
    protected $primaryKey = 'id_opcao';
    protected $fillable = [
        'id_questao',
        'descricao',
        'valor',
    ];
    public $timestamps;
}
