<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next, $guard = 'admin')
    {
        
        if(!Auth::guard($guard)->check() && !$request->is('/admin/login')){
            return redirect('/admin/login');            
        }

        return $next($request);
        
    }
}
