<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MatrizRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'ano' => 'required|integer|min:4',
            'ind_me' => 'required|integer',
            'trim' => 'required|integer',
            'qtd_questoes' => 'required|integer',
            'ponto_ini' => 'required|integer',
            'ponto_fim' => 'required|integer',
            'ano_questao' => 'required|integer',
            'qtd_facil' => 'required|integer',
            'qtd_medio' => 'required|integer',
            'qtd_dificil' => 'required|integer',
            'id_tipo_prova' => 'required|integer',
            'dt_inicio' => 'required|dateFormat:d/m/Y H:i:s',
            'dt_fim' => 'required|dateFormat:d/m/Y H:i:s',
            'descricao' => 'required|string|max:100',
            'status' => 'required'
        ];
    }
    
    public function messages(){
        
        return [
            'required' => 'O campo :attribute é obrigatório, por favor verifique.',
            'descricao.max' => 'O campo descrição comporta no máximo 100 carateres, por favor verifique.',
            'integer'  => 'Tipo de dado incorreto o campo :attribute aceita somente números inteiros.',
            'datetime' => 'Tipo de dado incorreto o campo :attribute aceita somente data no formato dd/mm/yyyy hh:mm:ss.'
        ];
    }
}
