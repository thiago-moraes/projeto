<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CandidatoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'cpf' => 'required',
            'email' => 'required',
            'celular' => 'required',
            'id_matriz' => 'required'
        ];
        
        if( request()->is("admin/novo/candidato") )
            $rules['password'] = 'required';
        
        return $rules;
    }
    
    public function messages(){
        
        return [
            'required' => 'O campo :attribute é obrigatório, por favor verifique'
        ];
    }
}