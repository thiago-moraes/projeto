<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'descricao' => 'required',
            'ano' => 'required|numeric',
            'dificuldade' => 'required|numeric',
            'tipo_prova' => 'required|numeric',
            'ponto' => 'required|numeric',
            'ind_me' => 'required|numeric',
            'justificativa' => 'required',
            'autor' => 'required|numeric',
        ];
    }

    public function messages()
    {
        
        return [
            'required' => 'O campo :attribute é obrigatório, por favor verifique'
        ];
    }
}
