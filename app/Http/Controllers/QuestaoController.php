<?php

namespace App\Http\Controllers;

use App\Matriz;
use App\MatrizQuestao;
use App\Opcao;
use App\Questao;
use Illuminate\Http\Request;
use App\Http\Requests\QuestaoRequest;
use Illuminate\Support\Facades\DB;

class QuestaoController extends Controller
{

    const DIFICULDADE = ['-','Fácil', 'Médio', 'Difícil'];

    public function buscaQuestoesPorNivelRandom(\App\Matriz $matriz ){
        
        return DB::table('questao')
                ->where('dificuldade', '=',  1)
                ->where('id_tipo_prova', '=',  $matriz->id_tipo_prova)
                ->where('ano', $matriz->ano_questao)
                ->whereBetween('ponto', [$matriz->ponto_ini, $matriz->ponto_fim])
                ->get();
    }
    
    
    public function dadosQuestao($idQuestao){

        return  Questao::find($idQuestao);
    }


    public function index(Request $request)
    {

        $questoes = Questao::when($request->input("ano"), function( $query ){
            $query->where("ano", request()->input("ano"));
        })->when($request->input("d"), function( $query ){
            $query->where("dificuldade", request()->input('d'));
        })->when($request->input("ind"), function( $query ){
            $query->where("ind_me", request()->input('ind'));
        })->when($request->input("autor"), function( $query ){
            $query->where("autor", request()->input('autor'));
        })->when($request->input("t"), function( $query ){
            $query->where("id_tipo_prova", request()->input('t'));
        })->orderBy("ind_me","ponto")->paginate(10)->appends([
            'c' => request()->input('c') ?? '',
            'd' => request()->input('d') ?? '',
            'ind' => request()->input('ind') ?? '',
            't' => request()->input('t') ?? '',
            'ano' => request()->input('ano') ?? '',
            'c' => request()->input('c') ?? '',
        ]);

            return view("admin.listaQuestoes", [
                'dados' => $questoes,
                'anos' => array_keys(Questao::all()->groupBy('ano')->toArray()),
                'autores' => (object) array_unique(array_column($questoes->toArray()['data'], "autor")),
                'periodo' => MatrizController::IND_ME,
                'tipoProva' => MatrizController::TIPO_PROVA,
            ]);
    }

    public function edit( $questao )
    {
        
        $dados = Questao::where("id_questao", $questao)->first();
        $autores = MatrizQuestaoController::dadosMatrizQuestao();
        
        return view("admin.formQuestao", [
            'dados' => $dados,
            'autores' => $autores->distinct("autores")->get(),
        ]);
    }

    public function update(Questao $questao, QuestaoRequest $request)
    {
        

        $questao->ano = trim($request->input("ano"));
        $questao->dificuldade = trim($request->input("dificuldade"));
        $questao->ponto = trim($request->input("ponto"));
        $questao->ind_me = trim($request->input("ind_me"));
        $questao->descricao = trim($request->input("descricao"));

        $questao->save();
        
        //return redirect()->back()->with("mensagem", "Questão atualizada com sucesso!");

    }

    public function create()
    {
        $autores = MatrizQuestaoController::dadosMatrizQuestao();
        $dados = ['id_questao' => '', 'ano' => '', 'dificuldade' => '', 'ponto' => '', 'ind_me' => '', 'descricao' => '', 'justificativa' => '', 'referencia' => '', 'autor' => '', 'id_tipo_prova' => ''];
        return view("admin.formNovaQuestao", [
            'dados' => (object) $dados,
            'autores' => $autores->distinct("autores")->get(),
        ]);
    }

    public function store( QuestaoRequest $request )
    {
        Questao::create($request->all());
        return redirect("")->with("mensagem", "Questão adicionada com sucesso!");
    }
}