<?php

namespace App\Http\Controllers;

use App\Notas;
use Illuminate\Http\Request;
use App\Http\Requests\CandidatoRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Matriz;
use App\Candidato;
use App\Prova;
use App\TipoProva;
use Illuminate\Notifications\Notifiable as Notifiable;

class CandidatoController extends Controller
{
    use Notifiable;

    private $prova;
    private $candidato;
    private $id;
    private $nome;
    private $email;
    private $id_pessoa;
    private $id_matriz;
    private $cpf;
    private $celular;
    private $pwd_plain_text;
    private $cet;
    private $dadosMatriz;
    private $token;
    
    public function logout(){

        Auth::logout();
        Session::flush();

        return redirect('/login');
    }

    public function getProva()
    {
        return $this->prova;
    }

    public function getId(){
        return $this->id;
    }
    
    private function setId( $id ){
        $this->id = $id;
    }
    
    public function getIdPessoa(){
        return $this->id_pessoa;
    }
    
    private function setIdPessoa( $idPessoa ){
        $this->id_pessoa = $idPessoa;
    }
    
    public function getCET(){
        return $this->cet;
    }
    
    public function setCET( $cet ){
        $this->cet = $cet;
    }
        
    public function getNome(){
        return $this->nome;
    }
    
    public function setNome( $nome ){
        $this->nome = $nome;
    }
    
    public function getIdMatriz(){
        return $this->id_matriz;
    }
    
    private function setIdMatriz( $idMatriz ){
        $this->id_matriz = $idMatriz;
    }
    
    public function getCPF(){
        return preg_replace('/(\d{3})(\d{3})(\d{3})(\d{2})/', '$1.$2.$3-$4', $this->cpf);
    }
    
    public function getCPFBanco(){
        return preg_replace('/(\d{3})(\d{3})(\d{3})(\d{2})/', '$1.$2.$3-$4', $this->cpf);
    }
    
    private function setCPF( $cpf ){
        $this->cpf = $cpf;
    }
    
    public function getCelular(){
        return $this->celular;
    }
    
    public function getEmail(){
        return $this->email;
    }
    
    public function setEmail( $email ){
        $this->email = $email;
    }
    
    public function setCelular( $celular ){
        $this->celular = $celular;
    }
    
    public function setMatriz() {
       $this->candidato->matriz = Matriz::find($this->getCandidato()->id_matriz);
    }

    public function setCandidato( $idCandidato ){
        $this->candidato = Candidato::find( $idCandidato );
    }

    public function getCandidato(){
        return $this->candidato;
    }

    public function setNota(){
        $this->candidato->nota = \App\Notas::where('id_matriz', $this->getCandidato()->id_matriz)
                ->where('id_candidato', $this->getCandidato()->id);
    }

    public function getNota(){
        return $this->nota;
    }
    
    private function setPwdPlainTex( $plainText ){
        $this->pwd_plain_text = $plainText;
    }
    
    private function getPwdPlainText()
    {
        return $this->pwd_plain_text;
    }
    
    private function setDadosMatriz( $dados )
    {
        $this->dadosMatriz = $dados;
    }
    
    public function getDadosMatriz()
    {
        return $this->dadosMatriz;
    }
    
    public function formRemember()
    {
        return view("formRemember");
    }

    public function remember(Request $request)
    {
        
        $captcha = new \Anhskohbo\NoCaptcha\NoCaptcha(
            '6LeFFH4UAAAAAOh1aR3TJgY7yewohlOGIA4kIDNv',
            '6LeFFH4UAAAAAPrw-mwB-iqlkM0a2fayf1CePsHJ'
        );

        if($captcha->verifyResponse($request->input('g-recaptcha-response')) ){

            $candidato = Candidato::where("cpf", $request->input("cpf"));
            if( $candidato->count() > 0 ){

                $candidato = $candidato->first();
                $utilController = new UtilController();
                $utilController->reminderSenhaSMS( $candidato, "Lembrete de senha prova on-line SBA: {$candidato->pwd_plain_text}" );

                $codificado = substr($candidato->celular,0,4)."*****".substr($candidato->celular, 9,5);

                return redirect()->back()->with("message", "Senha enviada para: {$codificado}");
            }

            return redirect()->back()->with("message","Usuário não encontrado");
        }else{
            return redirect()->back()->with("message", "Captcha não encontrado! Por favor verifique o campo foi marcado corretamente.");
        }
    }

    public function candidatosPorMatriz( $idMatriz )
    {

        return Candidato::where('id_matriz', $idMatriz);
    }
   	
    public function edit(\App\Candidato $candidato )
    {
        
        $this->setDados( $candidato );
        $matrizes = Matriz::where('status', 1)->get();
        
        return view('admin.formCandidato', ['dados' => $this, 'matrizes' => $matrizes , 'tipo' => 'cadastro']);
    }

    public function formAddExtension($idCandidato)
    {

        $this->setCandidato($idCandidato);
        $this->getCandidato();
        $this->setMatriz();

        return view("admin.formNewTime",['dados' => $this]);
    }

    public function AddExtension(Request $request)
    {

        $data = \DateTime::createFromFormat("d/m/Y H:i:s", $request->input("dt_fim"));
        $dados = [
            'dt_fim' => $data->format('Y-m-d H:i:s'),
            'candidato' => $request->input("id_candidato")
        ];

        \App\TimeExtension::create($dados);

        $this->setCandidato($request->input("id_candidato" ));
        $this->getCandidato();
        $this->setMatriz();

        return redirect()->back()
            ->with(['mensagem' => "Alteração de horário confirmada"]);
    }

    public function create()
    {
        
        
        $matrizes = Matriz::where('status', 1)->get();
        
        return view('admin.formNovoCandidato', ['dados' => $this, 'matrizes' => $matrizes , 'tipo' => 'cadastro']);
    }

    public function reminderSenha( Candidato $candidato )
    {
        
        $utilController = new UtilController();
        $utilController->reminderSenha( $candidato );
        
        return redirect('/admin/candidato')->with('mensagem', "Senha enviada com sucesso!");
    }

    public function reminderSenhaSMS( Candidato $candidato )
    {

        $utilController = new UtilController();
        $utilController->reminderSenhaSMS( $candidato, "Lembrete de senha prova on-line SBA: {$candidato->pwd_plain_text}" );
        
        return redirect('/admin/candidato')->with('mensagem', "Senha enviada com sucesso!");
    }
    
    public function relProvaMail( $candidato )
    {

        
        $candidato = Candidato::find($candidato);
        $prova = new ProvaController();
        $dadosProva = $prova->buscaProvaPorIdCandidato($candidato->id, $candidato->id_matriz)->first();

        if( is_null($dadosProva->token) ):
            $dadosProva->token = md5( $candidato->id );
            $dadosProva->save();
        endif;

        $util = new UtilController();
        $util->sendProvaCandidato($candidato, $dadosProva->token, \App\Matriz::find($candidato->id_matriz));

        return redirect('/admin/candidato')->with('mensagem', "Prova enviada com sucesso!");
    }
    
    public function relNotas(Request $request)
    {
        
        $token = $request->input("token");
        if($token != ''){
            
            $prova = Prova::where("token", $token)->first();
            if( $prova->count() > 0 ){
                $questoes = \App\ProvaQuestao::join("questao", "questao.id_questao", "=","prova_questao.id_questao")
                        ->where("prova_questao.id_prova", $prova->id_prova);

                $dados = $questoes->get();
                Candidato::where('id', $prova->id_candidato)->first();
                 return \PDF::loadView("admin.nota-candidatopdf", [
                     "questoes" => $dados,
                     "dados" => TipoProva::where("id_tipo_prova", $dados[0]->id_tipo_prova)->get(),
                     "prova" => $prova,
                     "candidato" => Candidato::where('id', $prova->id_candidato)->first(),
                     "nota" => Notas::where('id_candidato', $prova->id_candidato)->where("id_matriz", $prova->id_matriz)->first(),
                     ])->stream();
            }
        
        }
    }

	
    public function delete( Request $request )
    {
        
        $mensagem = 'Candidato não encontrado!';
        $idCandidato = $request->input( 'id' );
        $candidato = Candidato::find( $idCandidato );
        
        if( $candidato ){
            
            $mensagem = 'Candidato removido com sucesso!';
            $prova = \App\Prova::where('id_candidato', $idCandidato)->first();

            if($prova->count() > 0 ){
                $provaQuestao = \App\ProvaQuestao::where('id_prova', $prova->id_prova)->first();
                if( $provaQuestao->count() > 0 ){
                    
                    $provaQuestao->delete();
                }
                
                $prova->delete();
            }
        
            $candidato->delete();
        
        }
        
        return redirect('/admin/lista/candidatos')->with('mensagem', $mensagem);
    }
    
    public function store( CandidatoRequest $request )
    {
        
        $this->setDados( (object) $request->all() );
        $this->setPwdPlainTex($request->input('password'));

        $candidato = Candidato::create($request->all());
        $candidato->password = bcrypt( $this->getPwdPlainText() );
        $candidato->pwd_plain_text = $this->getPwdPlainText();
        $candidato->save();

        $prova = new ProvaController();
        $provaObj = $prova->criaProva($candidato->id, $candidato->id_matriz);

        $matriz = Matriz::where("id_matriz", $candidato->id_matriz)->first();

        $provaQuestao = new ProvaQuestaoController();
        $provaQuestao->criaQuestoesProvaAdm($matriz, $provaObj->id_prova);

        return redirect('/admin/candidato')->with('mensagem', 'Candidato adicionado com sucesso!');
    
    }
    
    public function update( CandidatoRequest $request )
    {
  
        $this->setDados( (object) $request->all() );

        $candidato = Candidato::find( $request->input('id') );
        $candidato->id_matriz = $this->getIdMatriz();
        $candidato->id_pessoa = $this->getIdPessoa();
        $candidato->nome = $this->getNome();
        $candidato->email = $this->getEmail();
        $candidato->cpf = $this->getCPFBanco();
        $candidato->celular = $this->getCelular();
        $candidato->cet = $this->getCET();

        if( $request->input('password') ){

            $this->setPwdPlainTex( $request->input('password') );

            $candidato->password = bcrypt( $this->getPwdPlainText() );
            $candidato->pwd_plain_text = $this->getPwdPlainText();

        }

        $candidato->save();
        
        return redirect('/admin/candidato')->with('mensagem', 'Candidato alterado com sucesso!');
    }
    
    public function index(Request $request)
    {
        
        $nome = $request->input('c') ?? '';
        
        $dados = Candidato::join("matriz", "matriz.id_matriz", "=","candidatos.id_matriz")
            ->where('candidatos.nome','like',"%{$nome}%")
            ->when($request->input("ano"), function( $query ){
                $query->whereYear("candidatos.updated_at", request()->input("ano"));
            })->when($request->input("me"), function( $query ){
                $query->where("matriz.ind_me", request()->input("me"));
            })->when($request->input("tipo"), function( $query ){
                $query->where("matriz.id_tipo_prova", request()->input("tipo"));
            })->when($request->input("matriz"), function( $query ){
                $query->where("candidatos.id_matriz", request()->input("matriz"));
            })->when($request->input("cet"), function( $query ){
                $query->where("candidatos.cet", request()->input("cet"));
            })->when($request->input("trim"), function( $query ){
                $query->where("matriz.trim", request()->input("trim"));
            })->select("candidatos.*","matriz.*");

            $dados->orderBy('candidatos.nome','ASC');
            $total = $dados->count();
            $links = $dados->paginate(10)
            ->appends([
                'c' => $request->input("c"),
                'ano' => $request->input("ano"),
                'trim' => $request->input("trim"),
                'me' => $request->input("me"),
                'tipo' => $request->input("tipo"),
                'matriz' => $request->input("matriz"),
                'cet' => $request->input("cet"),
            ])->links();

        foreach($dados->get() as $dado):
            $candidatoController = new CandidatoController();
            $candidatoController->setDados($dado);
            $matriz = new MatrizController();
            $matriz->setDados($dado);
            $candidatoController->setDadosMatriz( $matriz );
            $lista[] = $candidatoController;
        endforeach;
        
        return view('admin.listaCandidato', [
            'dados' => $lista ?? [],
            'anos' => $this->anosCandidato(),
            'me' => MatrizController::IND_ME,
            'tipo' => MatrizController::TIPO_PROVA,
            'trim' => MatrizController::TRIMESTRE,
            'total' => $total,
            'links' => $links
        ]);
    }

    public function anosCandidato()
    {
        $anos = DB::select("select YEAR(updated_at) as ano from candidatos group by YEAR(updated_at)");
        return $anos ?? [];
    }

    public function setDados( $dados ){
        
        $this->setId( $dados->id ?? 0 );
        $this->setIdMatriz( $dados->id_matriz );
        $this->setIdPessoa( (int) $dados->id_pessoa );
        $this->setCET( (int) $dados->cet );
        $this->setNome( $dados->nome );
        $this->setEmail( $dados->email );
        $this->setCPF( $dados->cpf );
        $this->setCelular( $dados->celular );
        
    }
}
