<?php

namespace App\Http\Controllers;

use Illuminate\Notifications\Notification;
use App\Http\Controllers\CandidatoController;
use App\Notifications\ProvaRespondidaSBA;
use App\Notifications\ReminderSenhaNotification;
use App\Notifications\ReminderSenhaSMS;
use App\Candidato;
use App\Jobs\ImportaQuestoesSGQ;
use App\Matriz;
use App\ProvaSGQ;
use Illuminate\Http\Request;

class UtilController extends Controller
{
    public function encrypt($idMatriz)
    {

        $candidatos = Candidato::where('id_matriz', $idMatriz)->get();
        dispatch(new \App\Jobs\EncriptaSenhasCandidato($candidatos));

        return redirect()->back()->with('mensagem', "As senhas estão sendo encriptadas.");
    }


    public function gerarSenha($idMatriz)
    {

        $candidatos = Candidato::where('id_matriz', $idMatriz)->get();
        dispatch(new \App\Jobs\GeraSenhasCandidato($candidatos));

        return redirect()->back()->with('mensagem', 'As senhas estão sendo geradas!');
    }


    public function encryptAdmin()
    {

        $admins = \App\Admin::all();
        foreach ($admins as $admin) {
            $senha = $admin->password;
            $admin->password = bcrypt($senha);
            $admin->save();
        }
    }

    public function enviaProva($idMatriz)
    {
        $candidatos = Candidato::join("prova", function ($join) {
            $join->on("prova.id_candidato", "=", "candidatos.id")
                ->on("prova.id_matriz", "=", "candidatos.id_matriz");
        })->join("notas", "notas.id_prova", "prova.id_prova")->where("notas.id_matriz", $idMatriz)->get();
        $count = 1;

        $matriz = Matriz::find($idMatriz);

        foreach ($candidatos as $candidato) :
            $prova = \App\Prova::find($candidato->id_prova);

            $email = $candidato->email;

            if ($prova->token == '') :
                $prova->token = md5($candidato->id_candidato);
                $prova->save();
            endif;

            $this->sendProvaCandidato($candidato, $prova->token, $matriz);

        endforeach;

        return redirect()->back()->with('mensagem', "Os emails com as provas estão sendo enviados.");
    }

    public function sendProvaCandidato(Candidato $candidato, $token, $matriz)
    {
        $this->sendNotification($candidato, new ProvaRespondidaSBA($token, $matriz));
    }

    public function envia_senha($idMatriz)
    {

        $candidatoController = new CandidatoController();
        $candidatos = $candidatoController->candidatosPorMatriz($idMatriz);

        foreach ($candidatos->get() as $candidato) :

            $candidatoController->setCandidato($candidato->id);
            $candidatoController->setMatriz();
            $this->sendNotification($candidato, new ReminderSenhaNotification());

        endforeach;

        return redirect()->back()->with('mensagem', "Senhas enviadas para a fila de processos!");
    }

    public function enviaSenhaSMS($idMatriz)
    {

        $candidatoController = new CandidatoController();
        $candidatos = $candidatoController->candidatosPorMatriz($idMatriz)->get();
        $matriz = $candidatos[0]->matriz()->first();

        $data = formataData($matriz->dt_inicio)->format("d/m/Y");
        $hora = formataData($matriz->dt_inicio)->format("H:i");

        foreach ($candidatos as $candidato) :
            $nome = explode(" ", $candidato->nome);
            $mensagem = "Dr(a). {$nome[0]}, senha Prova Trimestral {$candidato->pwd_plain_text}, inicio {$data} {$hora} HORARIO de BRASILIA-http://www.provasonlinesba.com.br/";

            if (!empty($candidato->celular))
                $this->sendNotification($candidato, new ReminderSenhaSMS($mensagem));

        endforeach;

        return redirect()->back()->with('mensagem', "Senhas enviadas para a fila de processos!");
    }

    public function formImportQuestions()
    {
        return view('admin.formImportQuestions', ['provas' => ProvaSGQ::all()]);
    }

    public function importQuestions(Request $request)
    {
        $ano = $request->input("ano");
        $prova = $request->input("prova");

        $importador = new ImportaQuestoesSGQ($prova, $ano);
        $importador->handle();
        
        return redirect()->back()->with('mensagem', "Questões importadas com sucesso!");
    }

    public function reminderSenha(Candidato $candidato)
    {
        return $this->sendNotification($candidato, new ReminderSenhaNotification());
    }

    public function reminderSenhaSMS(Candidato $candidato, $message)
    {
        return $this->sendNotification($candidato, new ReminderSenhaSMS($message));
    }

    private function sendNotification(Candidato $candidato, Notification $notification)
    {
        \Notification::send($candidato, $notification);
    }
}
