<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public  function username()
    {
        return 'cpf';
    }
    

    public function login(Request $request)
    {
        $candidato = new \App\Candidato;
        $dadosCandidato = $candidato->where("cpf", $request->input("cpf"))->orderBy('id', 'desc')->first();
        
        if( !is_null($dadosCandidato) ){
               
            $dadosMatriz = $dadosCandidato->matriz()->first();
            if( is_object($dadosMatriz) ){

                if( Hash::check(strtoupper($request->input('password')), $dadosCandidato->password) 
                    || $this->verificaSenhaMatriz($dadosMatriz->senha_matriz, $request->input("password"))
                ){
                    
                    $this->guard()->login($dadosCandidato);
                    return redirect()->intended($this->redirectTo());
                }
            }
        }

        return redirect()->back()->with(['mensagem' => 'Dados não encontrados. Por favor verifique e tente novamente!']);
    }

    protected function redirectTo()
    {

        $this->redirectTo = '/prova/create/';
        return $this->redirectTo;
    }

    protected function verificaSenhaMatriz( $senhaMatriz, $senha )
    {
        if( Hash::check($senha, $senhaMatriz) ){
            return true;
        }

        return false;
    }

    protected function dadosMatriz($idMatriz){

        $matriz = Matriz::find( $idMatriz );
        if( !is_null($matriz) ){
            return $matriz;
        }

        return false;
    }
}
