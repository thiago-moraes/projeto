<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;


class AdminLoginController extends Controller
{
    protected $guard = 'admin';
    protected $redirectTo = '/admin/login';
    
    use AuthenticatesUsers;
    use Notifiable;
    
    public  function username()
    {
        return 'usuario';
    }

    public function showLoginForm()
    {
        return view('auth.admin.login');
    }

    public function showRegistrationForm()
    {
        return view('auth.admin.register');
    }
    
    public function authenticate(Request $request){
        
        $credentials = [
            'usuario' => $request->input('usuario'),
            'password' => $request->input('password'),
        ];
        
        if( Auth::guard('admin')->attempt($credentials) ){
        
            
          return redirect()->intended('/admin/dashboard');  
        }
        
        return redirect('/admin/login')->with(['mensagem' => 'Usuário e/ou senha não encontrado, por favor verifique e tente novamente!']);
    }

}