<?php

namespace App\Http\Controllers;

use App\Http\Controllers\OpcaoProvaQuestaoController;
use App\Http\Controllers\ProvaQuestaoController;
use App\Prova;
use App\Notas;
use App\Matriz;

use Illuminate\Support\Facades\Auth;

class NotaController extends Controller
{

    private $resumo;
    private $matriz;
    private $prova;

    public function gravaNota($idProva)
    {

        $dadosNota = $this->dadosNota($idProva);

        $this->prova = Prova::find($idProva);
        $this->matriz = Matriz::find($this->prova->id_matriz);

        $nota['id_candidato'] = $this->prova->id_candidato;
        $nota['id_matriz'] = $this->prova->id_matriz;
        $nota['nota'] = $this->nota();
        $nota['aproveitamento'] = $this->aproveitamento();
        $nota['id_prova'] = $idProva;

        if ($dadosNota->count() == 0) {
            $dadosNota = Notas::create($nota)->where("id_prova", $idProva)->where("id_candidato", Auth::user()->id);
        } else {

            $dadosNota->first();
            $dadosNota->update($nota);
        }

        return $dadosNota->first();
    }

    private function nota()
    {
        $opcaoProvaQuestaoController = new OpcaoProvaQuestaoController();
        $this->resumo = (object) $opcaoProvaQuestaoController->resumo($this->prova->id_prova);

        if ($this->matriz->id_tipo_prova == 2) {
            
            $nota = number_format($this->resumo->valorPorOpcao * count($this->resumo->opcoesCorretas), 2);
        } else {

            $provaQuestao = new ProvaQuestaoController();
            $questoes = $provaQuestao->questoesPorIdProva($this->prova->id_prova);
            $valorPorQuestao = 10 / count($questoes);

            $nota = number_format($valorPorQuestao * count($this->resumo->opcoesCorretas), 2);
        }

        return $nota;
    }

    private function aproveitamento()
    {
        return number_format((count($this->resumo->opcoesCorretas) / count($this->resumo->opcoes)) * 100, 2);
    }

    public function dadosNota($idProva)
    {

        return Notas::where('id_prova', '=', $idProva);
    }
}
