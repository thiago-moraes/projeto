<?php

namespace App\Http\Controllers;

use App\MatrizQuestao;
use Illuminate\Http\Request;

class MatrizQuestaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MatrizQuestao  $matrizQuestao
     * @return \Illuminate\Http\Response
     */
    public function show(MatrizQuestao $matrizQuestao)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MatrizQuestao  $matrizQuestao
     * @return \Illuminate\Http\Response
     */
    public function edit(MatrizQuestao $matrizQuestao)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MatrizQuestao  $matrizQuestao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MatrizQuestao $matrizQuestao)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MatrizQuestao  $matrizQuestao
     * @return \Illuminate\Http\Response
     */
    public function destroy(MatrizQuestao $matrizQuestao)
    {
        //
    }

    public static function dadosMatrizQuestao()
    {
        return MatrizQuestao::join("sbahq.SECRET2", "SECRET2.MATRICULA", "=", "MATRIZ.autor")
            ->join("sbahq.PESSOA", "PESSOA.ID_PESSOA", "=", "SECRET2.ID_PESSOA")
            ->select("SECRET2.MATRICULA", "NOME")
            ->where("MATRIZ.ano", date("Y"))->select("SECRET2.MATRICULA", "NOME")
            ->orderBy("PESSOA.NOME");
    }
}
