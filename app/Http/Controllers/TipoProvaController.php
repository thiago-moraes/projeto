<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoProva;

class TipoProvaController extends Controller
{


    public function tipoProva($tipoProva){

        return TipoProva::find($tipoProva);
    }
}
