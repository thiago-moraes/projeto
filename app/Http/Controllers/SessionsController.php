<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $dados = \App\Candidato::join("sessions", "candidatos.id", "sessions.user_id")->paginate(10);
        return view("admin.listaSessoes", [
            'dados' => $dados,
            'total' => $dados->total(),
            'link' => $dados->links(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request )
    {
        
        $session = \App\Sessions::where('user_id', $request->input('user_id'))->first();
        $session->delete();
        
        return redirect()->back()->with('mensagem', 'Sessão removida com sucesso!');
    }

    public function destroyAll(Request $request)
    {

        $session = \App\Sessions::where('user_id',"!=" , $request->input('user_id'))->first();
        $session->delete();
        
        return redirect()->back()->with('mensagem', 'Sessão removida com sucesso!');
    }
}
