<?php

namespace App\Http\Controllers\Classes;

class MatrizClass {
    
    
    private $matriz;
    private $indME;
    private $ano;
    private $trim;
    private $qtdQuestoes;
    private $pontoIni;
    private $pontoFim;
    private $anoQuestao;
    private $qtdFacil;
    private $qtdMedio;
    private $qtdDificil;
    private $tipoProva;
    private $dtInicio;
    private $dtFim;
    private $status;
    private $descricao;


    private function setIdMatriz($idMatriz){
        $this->matriz = $idMatriz;
    }

    public function getIdMatriz(){
        return $this->matriz;
    }

    public function setIndME($indME){
        $this->indME = $indME;
    }

    public function getIndME(){
        return $this->indME;
    }

    public function setAno($ano){
        $this->ano = $ano;
    }

    public function getAno(){
        return $this->ano;
    }

    public function setTrim($trimestre){
        $this->trim = $trimestre;
    }

    public function getTrim(){
        return $this->trim;
    }

    public function setQtdQuestoes($qtd){
        $this->qtdQuestoes = $qtd;
    }

    public function getQtdQuestoes(){
        return $this->qtdQuestoes;
    }

    public function setQtdFacil($qtd){
        $this->qtdFacil = $qtd;
    }

    public function getQtdFacil(){
        return $this->qtdFacil;
    }

    public function setQtdMedio($qtd){
        $this->qtdMedio = $qtd;
    }

    public function getQtdMedio(){
        return $this->qtdMedio;
    }

    public function setQtdDificil($qtd){
        $this->qtdDificil = $qtd;
    }

    public function getQtdDificil(){
        return $this->qtdDificil;
    }

    public function setTipoProva( $tipo ){
        $this->tipoProva = $tipo;
    }

    public function getTipoProva(){
        return $this->tipoProva;
    }

    public function setDtInicio( \DateTime $inicio ){
        $this->dtInicio = $inicio;
    }

    public function getDtInicio(){
        return $this->dtInicio;
    }

    public function setDtFim( \DateTime $fim ){
        $this->dtFim = $fim;
    }

    public function getDtFim(){
        return $this->dtFim;
    }

    public function setStatus( $status ){
        $this->status = $status;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function setPontoIni( $inicio ){
        $this->pontoIni = $inicio;
    }

    public function getPontoIni(){
        return $this->pontoIni;
    }

    public function setPontoFim( $fim ){
        $this->pontoFim = $fim;
    }

    public function getPontoFim(){
        return $this->pontoFim;
    }

    public function setAnoQuestao( $ano ){
        $this->anoQuestao = $ano;
    }

    public function getAnoQuestao(){
        return $this->anoQuestao;
    }
    
    public function setDados($dados){

        $this->setIndME($dados->ind_me ?? '');
        $this->setAno($dados->ano ?? '');
        $this->setTrim($dados->trim ?? '');
        $this->setQtdQuestoes($dados->qtd_questoes ?? '');
        $this->setPontoIni($dados->ponto_ini ?? '');
        $this->setPontoFim($dados->ponto_fim ?? '');
        $this->setAnoQuestao($dados->ano_questao ?? '');
        $this->setQtdFacil($dados->qtd_facil ?? '');
        $this->setQtdMedio($dados->qtd_medio ?? '');
        $this->setQtdDificil($dados->qtd_dificil ?? '');
        $this->setTipoProva($dados->id_tipoProva ?? '');
        $this->setDtInicio(new \DateTime($dados->dt_inicio ?? ''));
        $this->setDtFim(new \DateTime($dados->dt_fim ?? ''));
        $this->setStatus($dados->status ?? '');
        $this->setDescricao($dados->descricao ?? '');
        $this->setIdMatriz($dados->id_matriz ?? 0);
    }
}