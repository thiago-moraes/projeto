<?php

namespace App\Http\Controllers\Classes;

use App\Http\Controllers\OpcaoProvaQuestaoController;

class TEA
{
    function gravaRespostas($idProvaQuestao, $opcaoRespondida = 0)
    {

        $opcaoProvaQuestao = new OpcaoProvaQuestaoController();
        $opcoes = $opcaoProvaQuestao->opcoesPorQuestao($idProvaQuestao)->get();
        foreach ($opcoes as $opcao) {
            if ($opcaoRespondida > 0) {
                $resposta = $opcao->id_opcao == $opcaoRespondida ? 2 : 0;
                $opcao->valor =  $resposta;
                $opcao->save();
                $emBranco[] = false;
            } else {
                $emBranco[] = true;
            }
        }

        return $emBranco;
    }
}
