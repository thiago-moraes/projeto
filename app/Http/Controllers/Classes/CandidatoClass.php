<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Classes;

/**
 * Description of newPHPClass
 *
 * @author Thiago Carvalho
 */
class CandidatoClass {
    
    private $prova;
    private $candidato;
    private $id;
    private $nome;
    private $email;
    private $id_pessoa;
    private $id_matriz;
    private $cpf;
    private $celular;
    private $pwd_plain_text;
    private $cet;
    
    public function logout(){

        Auth::logout();
        Session::flush();

        return redirect('/login');
    }
    
    public function getId(){
        return $this->id;
    }
    
    private function setId( $id ){
        $this->id = $id;
    }
    
    public function getIdPessoa(){
        return $this->id_pessoa;
    }
    
    private function setIdPessoa( $idPessoa ){
        $this->id_pessoa = $idPessoa;
    }
    
    public function getCET(){
        return $this->cet;
    }
    
    public function setCET( $cet ){
        $this->cet = $cet;
    }
    
    
    public function getNome(){
        return $this->nome;
    }
    
    public function setNome( $nome ){
        $this->nome = $nome;
    }
    
    public function getIdMatriz(){
        return $this->id_matriz;
    }
    
    private function setIdMatriz( $idMatriz ){
        $this->id_matriz = $idMatriz;
    }
    
    public function getCPF(){
        return $this->cpf;
    }
    
    private function setCPF( $cpf ){
        $this->cpf = $cpf;
    }
    
    public function getCelular(){
        return $this->celular;
    }
    
    public function getEmail(){
        return $this->email;
    }
    
    public function setEmail( $email ){
        $this->email = $email;
    }
    
    public function setCelular( $celular ){
        $this->celular = $celular;
    }
    
    public function setMatriz() {
       $this->candidato->matriz = Matriz::find($this->getCandidato()->id_matriz);
    }

    public function setCandidato( $idCandidato ){
        $this->candidato = Candidato::find( $idCandidato );
    }

    public function getCandidato(){
        return $this->candidato;
    }

    public function setNota(Nota $nota){
        $this->candidato->nota = \App\Nota::where('id_matriz', $this->getCandidato()->id_matriz)
                ->where('id_candidato', $this->getCandidato()->id);
    }

    public function getNota(){
        return $this->nota;
    }
    
    private function setPwdPlainTex( $plainText ){
        $this->pwd_plain_text = $plainText;
    }
    
    private function getPwdPlainText(){
        return $this->pwd_plain_text;
    }
    
    public function setDados( $dados ){
        
        $this->setId( $dados->id ?? 0 );
        $this->setIdMatriz( $dados->id_matriz );
        $this->setIdPessoa( $dados->id_pessoa );
        $this->setNome( $dados->nome );
        $this->setEmail( $dados->email );
        $this->setCPF( $dados->cpf );
        $this->setCelular( $dados->celular );
        
    }
}
