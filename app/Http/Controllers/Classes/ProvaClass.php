<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Classes;

use App\Http\Controllers\Controller;

/**
 * Description of newPHPClass
 *
 * @author Thiago Carvalho
 */
class ProvaClass extends Controller {
    
    private $candidato;
    private $matriz;
    private $idProva;
    private $idCandidato;
    private $idMatriz;
    private $status;
    private $inicio;
    private $fim;
    
    public function setCandidato($candidato){
        $this->candidato = $candidato;
    }

    public function getCandidato(){
        return $this->candidato;
    }

    public function setMatriz($matriz){
        $this->matriz = $matriz;

    }
    public function getMatriz(){
        return $this->matriz;

    }
    
    public function setIdProva($id){
        $this->idProva = $id;
    }
    
    public function getIdProva(){
        return $this->idProva;
    }
    
    public function setIdMatriz( $idMatriz ){
        $this->idMatriz = $idMatriz;
    }
    
    public function getIdMatriz(){
        return $this->idMatriz;
    }
    
    public function setIdCandidato( $idCandidato ){
        return $this->idCandidato = $idCandidato;
    }
    
    public function getIdCandidato(){
        return $this->idCandidato;
    }
    
    public function setStatus( $status ){
        $this->status = $status;
    }
    
    public function getStatus(){
        return $this->status;
    }
    
    public function getInicio(){
        return $this->inicio;
    }
    
    public function setInicio( $inicio ){
 
        $this->inicio = $inicio;
    }
    
    public function getFim(){
        return $this->inicio;
    }
    
    public function setFim( $fim ){

        $this->fim = $fim;
    }
    
    protected function setDados( $prova ){
        
        $this->setIdProva($prova->id_prova);
        $this->setIdCandidato($prova->id_candidato);
        $this->setIdMatriz($prova->id_matriz);
        $this->setStatus($prova->dadosCodigos);
        $this->setInicio($prova->created_at);
        $this->setFim($prova->updated_at);
        $this->setCandidato($prova->dadosCandidato);
    }
}
