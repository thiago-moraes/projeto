<?php

namespace App\Http\Controllers;

use App\Questao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Opcao;

class OpcaoController extends Controller
{
    const INDICE_ALFA = ["A", "B", "C", "D", "E",];
    const RESPOSTA = [
        0 => '-',
        1 => 'Falso',
        2 => 'Verdadeiro',
        3 => 'Anulada',
    ];

    public function retornaOpcoesPorQuestao($idQuestao)
    {

        return Opcao::where('id_questao', $idQuestao)->get();
    }

    public function buscaOpcoes($idProvaQuestao)
    {
        return DB::select(DB::raw("SELECT * FROM `prova_questao` INNER JOIN opcao_prova_questao on prova_questao.id_prova_questao = opcao_prova_questao.id_prova_questao where prova_questao.id_prova_questao = {$idProvaQuestao}"));

    }

    public function show($idQuestao)
    {

        $opcoes = $this->retornaOpcoesPorQuestao($idQuestao);
        $questao = Questao::find($opcoes[0]->id_questao);

        return view("admin.listaOpcoes", ["dados" => $opcoes, 'questao' => $questao]);
    }

    public function update($idOpcao, Request $request)
    {
        $opcao = Opcao::find($idOpcao);
        $opcao->update($request->all());

        return redirect()->back()->with(['mensagem' => "Dados atualizados com sucesso"]);
    }

}
