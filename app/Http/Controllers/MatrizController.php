<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Matriz;
use App\Http\Requests\MatrizRequest;
use Hash;
use App\Jobs\GeraPDFProvasPorMatriz;
use App\Jobs\GeraProvasPorMatriz;

class MatrizController extends Controller
{

    private $matriz;
    private $indME;
    private $idTipoProva;
    private $ano;
    private $trim;
    private $qtdQuestoes;
    private $pontoIni;
    private $pontoFim;
    private $anoQuestao;
    private $qtdFacil;
    private $qtdMedio;
    private $qtdDificil;
    private $tipoProva;
    private $dtInicio;
    private $dtFim;
    private $status;
    private $descricao;

    const TRIMESTRE = [1 => 1, 2 => 2, 3 => 3, 4 => 4];
    const IND_ME = [1 => 'ME1', 2 => 'ME2', 3 => 'ME3'];
    const TIPO_PROVA = [1 => 'TEA', 2 => 'ME - Trimestral'];

    private function setIdMatriz($idMatriz)
    {
        $this->matriz = $idMatriz;
    }

    public function getIdMatriz()
    {
        return $this->matriz;
    }

    private function setIdTipoProva($idTipoProva)
    {
        $this->id_tipo_prova = $idTipoProva;
    }

    public function getIdTipoProva()
    {
        return $this->id_tipo_prova;
    }

    public function setIndME($indME)
    {
        $this->indME = $indME;
    }

    public function getIndME()
    {
        return $this->indME;
    }

    public function setAno($ano)
    {
        $this->ano = $ano;
    }

    public function getAno()
    {
        return $this->ano;
    }

    public function setTrim($trimestre)
    {
        $this->trim = $trimestre;
    }

    public function getTrim()
    {
        return $this->trim;
    }

    public function setQtdQuestoes($qtd)
    {
        $this->qtdQuestoes = $qtd;
    }

    public function getQtdQuestoes()
    {
        return $this->qtdQuestoes;
    }

    public function setQtdFacil($qtd)
    {
        $this->qtdFacil = $qtd;
    }

    public function getQtdFacil()
    {
        return $this->qtdFacil;
    }

    public function setQtdMedio($qtd)
    {
        $this->qtdMedio = $qtd;
    }

    public function getQtdMedio()
    {
        return $this->qtdMedio;
    }

    public function setQtdDificil($qtd)
    {
        $this->qtdDificil = $qtd;
    }

    public function getQtdDificil()
    {
        return $this->qtdDificil;
    }

    public function setTipoProva($tipo)
    {
        $this->tipoProva = $tipo;
    }

    public function getTipoProva()
    {
        return $this->tipoProva;
    }

    public function setDtInicio($inicio)
    {

        $data = new \DateTime();

        $this->dtInicio = $data;
        if (strpos($inicio, '/'))
            $this->dtInicio = $data->createFromFormat('d/m/Y H:i:s', $inicio);

        if (strpos($inicio, '-'))
            $this->dtInicio = $data->createFromFormat('Y-m-d H:i:s', $inicio);
    }

    public function getDtInicio()
    {

        return $this->dtInicio;
    }

    public function setDtFim($fim)
    {

        $data = new \DateTime();
        $this->dtFim = $data;
        if (strpos($fim, '/'))
            $this->dtFim = $data->createFromFormat('d/m/Y H:i:s', $fim);

        if (strpos($fim, '-'))
            $this->dtFim = $data->createFromFormat('Y-m-d H:i:s', $fim);
    }

    public function getDtFim()
    {
        return $this->dtFim;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setPontoIni($inicio)
    {
        $this->pontoIni = $inicio;
    }

    public function getPontoIni()
    {
        return $this->pontoIni;
    }

    public function setPontoFim($fim)
    {
        $this->pontoFim = $fim;
    }

    public function getPontoFim()
    {
        return $this->pontoFim;
    }

    public function setAnoQuestao($ano)
    {
        $this->anoQuestao = $ano;
    }

    public function getAnoQuestao()
    {
        return $this->anoQuestao;
    }

    public function getSenha()
    {
        return $this->anoQuestao;
    }

    public function retornaDadosMatriz($idMatriz)
    {

        $dados = Matriz::find($idMatriz);
        $this->setDados($dados);

        return $dados;
    }

    public function addMatriz(MatrizRequest $request)
    {

        $this->setDados((object) $request->all());

        $matriz = new Matriz();
        $matriz->ano = $this->getAno();
        $matriz->ind_me = $this->getIndME();
        $matriz->trim = $this->getTrim();
        $matriz->qtd_questoes = $this->getQtdQuestoes();
        $matriz->ponto_ini = $this->getPontoIni();
        $matriz->ponto_fim = $this->getPontoFim();
        $matriz->ano_questao = $this->getAnoQuestao();
        $matriz->qtd_facil = $this->getQtdFacil();
        $matriz->qtd_medio = $this->getQtdMedio();
        $matriz->qtd_dificil = $this->getQtdDificil();
        $matriz->id_tipo_prova = $this->getIdTipoProva();
        $matriz->dt_inicio = $this->getDtInicio()->format('Y-m-d H:i:s');
        $matriz->dt_fim = $this->getDtFim()->format('Y-m-d H:i:s');
        $matriz->status = $this->getStatus();
        $matriz->descricao = $this->getDescricao();
        $matriz->senha_matriz = Hash::make($request->input("password"));

        $matriz->save();

        return redirect('/admin/lista/matriz');
    }

    public function updateMatriz(Request $request)
    {

        $this->setDados((object) $request->all());

        $matriz = Matriz::find($this->getIdMatriz());

        $matriz->ano = $this->getAno();
        $matriz->ind_me = $this->getIndME();
        $matriz->trim = $this->getTrim();
        $matriz->qtd_questoes = $this->getQtdQuestoes();
        $matriz->ponto_ini = $this->getPontoIni();
        $matriz->ponto_fim = $this->getPontoFim();
        $matriz->ano_questao = $this->getAnoQuestao();
        $matriz->qtd_facil = $this->getQtdFacil();
        $matriz->qtd_medio = $this->getQtdMedio();
        $matriz->qtd_dificil = $this->getQtdDificil();
        $matriz->id_tipo_prova = $this->getIdTipoProva();
        $matriz->dt_inicio = $this->getDtInicio()->format('Y-m-d H:i:s');
        $matriz->dt_fim = $this->getDtFim()->format('Y-m-d H:i:s');
        $matriz->status = $this->getStatus();
        $matriz->descricao = $this->getDescricao();
        $matriz->senha_matriz = Hash::make($request->input("password"));

        $matriz->save();

        return redirect('/admin/lista/matriz')->with('mensagem', 'Matriz atualizada com sucesso!');
    }

    public function delete(Request $request)
    {

        $idMatriz = $request->input('id_matriz');
        $mensagem = 'Matriz não encontrada, por favor verifique e tente novamente!';
        if ($idMatriz) {

            $matriz = Matriz::find($idMatriz);
            $matriz->status = 2;
            $matriz->save();

            $mensagem = 'Matriz removida com sucesso';
        }

        return redirect('/admin/lista/matriz')->with('mensagem', $mensagem);
    }

    public function formMatriz($idMatriz)
    {

        $mensagem = 'Matriz não encontrada, por favor verifique e tente novamente';
        if ($idMatriz) {

            $this->retornaDadosMatriz($idMatriz);
            $tipos = \App\TipoProva::all();
            $status = \App\Codigos::where('grupo', 2)->get();

            $mensagem = '';
        }

        return view('admin.formMatriz', ['dados' => $this, 'tipos' => $tipos, 'status' => $status, 'method' => 'post'])->with('mensagem', $mensagem);
    }

    public function formNovaMatriz()
    {

        $tipoProva = \App\TipoProva::all();
        $this->setDados('');
        $status = \App\Codigos::where('grupo', 2)->get();
        return view('admin.formNovaMatriz', ['dados' => $this, 'tipos' => $tipoProva, 'status' => $status, 'method' => 'post']);
    }

    public function lista(Request $request)
    {

        $matriz = Matriz::where('descricao', 'like', "%" . $request->input('descricao') . "%")
            ->when($request->input('ano'), function ($query) {
                $query->where('ano', request()->input('ano'));
            })
            ->when($request->input('tipo'), function ($query) {
                $query->where('id_tipo_prova', request()->input('tipo'));
            })
            ->when($request->input('status'), function ($query) {
                $query->where('status', request()->input('status'));
            })->orderBy('id_matriz', 'DESC');

        $links = $matriz->paginate(10)->links();

        if ($matriz->count() > 0) {

            foreach ($matriz->get() as $values) :

                $m = new MatrizController();
                $m->setDados($values);
                $m->setStatus($values->dadosStatus()[0]);
                $m->setTipoProva($values->tipoProva()[0]);
                $dados[] = $m;
            endforeach;
        } else {

            $dados = [];
        }

        return view('admin.listaMatriz', [
            'dados' => $dados,
            'links' => $links,
            'status' => \App\Codigos::where('grupo', 2),
            'tipo' => \App\TipoProva::all(),
            'ano' => \App\Matriz::all()->groupBy('ano')
        ])
            ->withInput($request);
    }


    public function relNotas($idMatriz)
    {

        $dados = \DB::select("select * from matriz m inner join candidatos c on m.id_matriz = c.id_matriz left join prova p on c.id = p.id_candidato and p.id_matriz = m.id_matriz left join notas n on p.id_prova = n.id_prova and c.id = n.id_candidato where m.id_matriz = {$idMatriz} order by nome");
        if (count($dados)) :
            return \PDF::loadView('admin.notaspdf', ['dados' => $dados])
                ->stream();
        endif;
        return '<h1>Nehuma nota encontrada para esta matriz!</h1>';
    }

    public function relMailNotas($idMatriz)
    {
        dispatch(new \App\Jobs\GeraPDFProvasPorMatriz($idMatriz));
        return redirect()->back()->with('mensagem', 'Os PDFs estão sendo gerados!');
    }

    public function setDados($dados)
    {

        $this->setIndME($dados->ind_me ?? '');
        $this->setAno($dados->ano ?? '');
        $this->setTrim($dados->trim ?? '');
        $this->setQtdQuestoes($dados->qtd_questoes ?? '');
        $this->setPontoIni($dados->ponto_ini ?? '');
        $this->setPontoFim($dados->ponto_fim ?? '');
        $this->setAnoQuestao($dados->ano_questao ?? '');
        $this->setQtdFacil($dados->qtd_facil ?? '');
        $this->setQtdMedio($dados->qtd_medio ?? '');
        $this->setQtdDificil($dados->qtd_dificil ?? '');
        $this->setDtInicio($dados->dt_inicio ?? '');
        $this->setDtFim($dados->dt_fim ?? '');
        $this->setIdTipoProva($dados->id_tipo_prova ?? 0);
        $this->setStatus($dados->status ?? '');
        $this->setDescricao($dados->descricao ?? '');
        $this->setIdMatriz($dados->id_matriz ?? 0);
    }
}
