<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    
    public function index(){

        return view('admin.dashboard');
    }

    public function formCandidato(){

        return view('admin.formCandidato');
    }

    public function gravaCandidato(Request $request){


    }


    public function liberaProva(Request $request){


    }
}
