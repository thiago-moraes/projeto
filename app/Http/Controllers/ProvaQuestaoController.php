<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Classes\TEA;
use Illuminate\Http\Request;

use App\ProvaQuestao;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MatrizController;
use App\Http\Controllers\QuestaoController;
use App\Http\Controllers\TipoProvaController;
use Illuminate\Support\Facades\DB;
use \App\Http\Requests\GravaQuestaoRequest;
use App\User;

class ProvaQuestaoController extends Controller
{

    public function criaQuestoesProva($idProva)
    {

        $matriz = new MatrizController();
        $dadosMatriz = $matriz->retornaDadosMatriz(Auth::user()->id_matriz);

        $this->criaQuestoesProvaAdm($dadosMatriz, $idProva);

        $tipoProva = new TipoProvaController();
        $dadosTipoProva = $tipoProva->tipoProva($dadosMatriz->id_tipo_prova);

        return redirect("/prova/{$dadosTipoProva->slug}/{$idProva}");
    }

    public function criaQuestoesProvaAdm($matriz, $idProva)
    {

        $dados = [];
        $questoesProva = $this->questoesPorIdProva($idProva);
        if ($questoesProva->count() < $matriz->qtd_questoes) {

            $questoesController = new QuestaoController();
            if ($matriz->id_tipo_prova > 1) {
                //Monta as questões das provas != do TEA - Randomicas
                $questoes = array_merge(
                    $questoesController->buscaQuestoesPorNivelRandom($matriz)->random($matriz->qtd_facil)->shuffle()->toArray()
                );
            } else {
                //Monta as questões da prova do TEA
                $questoes = array_merge(
                    $questoesController->buscaQuestoesPorNivelRandom($matriz)->take($matriz->qtd_facil)->toArray()
                );
            }

            $questoes = $this->gravaQuestoesNaProva($questoes, $idProva);

            foreach ($questoes as $value) :
                $dados[] = ['id_questao' => $value->id_questao, 'id_prova_questao' => $value->id_prova_questao];
            endforeach;

            $OpcaoProvaQuestaoController = new OpcaoProvaQuestaoController();
            $OpcaoProvaQuestaoController->gravaOpcoesProva($dados, $matriz);
        }
    }


    private function gravaQuestoesNaProva($questoes, $idProva)
    {

        foreach ($questoes as $key => $questao) :
            $dados[] = ['id_questao' => $questao->id_questao, 'id_prova' => $idProva, 'status' => 1, 'sequencia' => str_pad(($key + 1), 2, "0", STR_PAD_LEFT)];
        endforeach;

        DB::table('prova_questao')->insert($dados);
        return $this->questoesPorIdProva($idProva);
    }


    public function questoesPorIdProva($idProva)
    {

        $dadosProva = ProvaQuestao::where('id_prova', $idProva)->orderBy('sequencia')->get();
        return $dadosProva;
    }

    public function questoesNaoRespondidasPorIdProva($idProva)
    {

        $dadosProva = ProvaQuestao::where('id_prova', $idProva)
            ->where('status', '<', 3)
            ->orderBy('sequencia', 'ASC');

        return $dadosProva;
    }

    public function questoesRespondidasPorIdProva($idProva)
    {

        $dadosProva = ProvaQuestao::where('id_prova', $idProva)
            ->where('status', 3)
            ->orderBy('sequencia');
        return $dadosProva;
    }

    public function dadosProvaPorIdProvaQuestao($idProvaQuestao)
    {

        $questao = DB::select(DB::raw("SELECT candidatos.id_matriz, prova.id_prova, prova_questao.id_prova_questao, prova_questao.id_questao, descricao, prova_questao.status, prova_questao.sequencia FROM candidatos "
            . "inner join prova on candidatos.id = prova.id_candidato and candidatos.id_matriz = prova.id_matriz "
            . "inner join prova_questao on prova.id_prova = prova_questao.id_prova and prova.status <= 1 "
            . "INNER JOIN questao ON prova_questao.id_questao = questao.id_questao where prova_questao.id_prova_questao = {$idProvaQuestao} order by prova_questao.sequencia"));


        return $this->dadosProvaQuestao((object) $questao[0]);
    }

    public function questaoPorProvaSequencia($idProva, $sequencia)
    {

        $questao = DB::select(DB::raw("SELECT candidatos.id_matriz, prova.id_prova, prova_questao.id_prova_questao, prova_questao.id_questao, descricao, prova_questao.status, prova_questao.sequencia FROM candidatos "
            . "inner join prova on candidatos.id = prova.id_candidato and candidatos.id_matriz = prova.id_matriz "
            . "inner join prova_questao on prova.id_prova = prova_questao.id_prova and prova.status <= 1 "
            . "INNER JOIN questao ON prova_questao.id_questao = questao.id_questao where prova_questao.id_prova = {$idProva} and prova_questao.sequencia = {$sequencia} order by prova_questao.sequencia"));

        if (count($questao) > 0)
            return $this->dadosProvaQuestao((object) $questao[0]);

        return $this->dadosProvaQuestao((object) $questao);
    }

    public function gravaQuestao(Request $request)
    { //Grava as respostas do candidato

        if (!Auth::check()) {
            return redirect('/prova/create');
        }

        $idProvaQuestao = $request->input('idProvaQuestao');
        $idProva = $request->input('idProva');
        $inputOpcoes = $request->input('opcoes');
        $sequencia = $request->input('sequencia');

        $matriz = \App\Matriz::find(Auth::user()->id_matriz);
        if ($matriz->id_tipo_prova > 1) {
            $opcaoProvaQuestao = new OpcaoProvaQuestaoController();
        } else {
            $opcaoProvaQuestao = new TEA;
        }
        $emBranco = $opcaoProvaQuestao->gravaRespostas($idProvaQuestao, $inputOpcoes);

        $emBranco = array_unique($emBranco);

        if (count($emBranco) == 1 && $emBranco[0] == false) //Respondida
            $this->alteraStatusProvaQuestao($idProvaQuestao, 3);

        if (count($emBranco) == 1 && $emBranco[0] == true) //Não repondida
            $this->alteraStatusProvaQuestao($idProvaQuestao, 1);

        if (count($emBranco) > 1) //Parcialmente respondida
            $this->alteraStatusProvaQuestao($idProvaQuestao, 2);

        if ($request->finalizar > 0) {
            if ($request->finalizar > 1) {
                return redirect()->action("ProvaController@confirmaFinalizacaoDaProva", ['idProva' => $request->input("idProva")]);
            }
            return redirect()->action("ProvaController@finalizaProva", ['idProva' => $request->input("idProva")]);
        }

        return redirect()->action('ProvaController@mostraQuestao', [
            'tipo' => 'objetiva', 'idProva' => $idProva, 'sequencia' => $sequencia
        ]);
    }


    public function totalQuestoesPorStatus($idProva, $status)
    {

        return ProvaQuestao::where('id_prova', $idProva)
            ->where('status', $status);
    }

    private function alteraStatusProvaQuestao($idProvaQuestao, $status)
    {

        $provaQuestao = ProvaQuestao::find($idProvaQuestao);
        $provaQuestao->status = $status;
        $provaQuestao->save();
    }

    private function dadosProvaQuestao($dados)
    {

        return [
            'id_prova_questao' => $dados->id_prova_questao ?? '',
            'id_questao' => $dados->id_questao ?? '',
            'status' => $dados->status ?? '',
            'id_prova' => $dados->id_prova ?? '',
            'descricao' => $dados->descricao ?? '',
            'matriz' => $dados->id_matriz ?? '',
            'sequencia' => $dados->sequencia ?? '',
        ];
    }
}
