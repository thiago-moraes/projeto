<?php

namespace App\Http\Controllers;

use App\Sessions;
use App\Candidato;
use Illuminate\Http\Request;

class ProvaOnlineController extends Controller {

    const STATUS = [1 => 'Inativo', 2 => 'Ativo'];

   public function index(Request $request){

       $nome = $request->input("c") ?? "";

        $candidatos = Candidato::leftJoin("sessions", "candidatos.id", "=", "sessions.user_id")
            ->join("matriz", "matriz.id_matriz", "=", "candidatos.id_matriz")
            ->join("prova", function($join){
                $join->on("prova.id_matriz", "=", "candidatos.id_matriz")
                ->on('prova.id_candidato', '=', 'candidatos.id');
            })
            ->where("candidatos.nome","like","%{$nome}%")
            ->when(request()->input("ano"), function( $query ){
                $query->whereYear("candidatos.updated_at", request()->input("ano"));
            })->when($request->input("me"), function( $query ){
                $query->where("matriz.ind_me", request()->input("me"));
            })->when($request->input("tipo"), function( $query ){
                $query->where("matriz.id_tipo_prova", request()->input("tipo"));
            })->when($request->input("matriz"), function( $query ){
                $query->where("candidatos.id_matriz", request()->input("matriz"));
            })->when($request->input("cet"), function( $query ){
                $query->where("candidatos.cet", request()->input("cet"));
            })->when($request->input("trim"), function( $query ){
                $query->where("matriz.trim", request()->input("trim"));
            })->when($request->input("s"), function( $query ){
                $query->where("prova.status", request()->input("s"));
            })->when($request->input("si"), function( $query ){

                if(request()->input('si') == 1):
                    $query->where("sessions.user_id", NULL);
                else:
                    $query->where("sessions.user_id", "!=", NULL);
                endif;

            })->select("matriz.*","sessions.*", "candidatos.*", 'prova.status')
            ->orderBy("nome")
            ->paginate(10)
            ->appends([ 'c' => $request->input("c"),
                'ano' => $request->input("ano"),
                'trim' => $request->input("trim"),
                'me' => $request->input("me"),
                'tipo' => $request->input("tipo"),
                'ma' => $request->input("ma"),
                's' => $request->input("s"),
                'si' => $request->input("si"),
                'cet' => $request->input("cet"),]);

        $candidatosController = new CandidatoController();
        $cets = array_keys(Candidato::all(['cet'])->groupBy('cet')->toArray());
        sort($cets);

        return view('admin.listaOnline', [
           'dados' => $candidatos,
           'anos' => $candidatosController->anosCandidato(),
           'total' => $candidatos->total(),
           'link' => $candidatos->links(),
            'matrizes' => array_collapse(\App\Matriz::all(['id_matriz','descricao'])->groupBy('id_matriz')->toArray()),
            'me' => MatrizController::IND_ME,
            'tipo' => MatrizController::TIPO_PROVA,
            'trim' => MatrizController::TRIMESTRE,
            'situacao' => ProvaOnlineController::STATUS,
            'status' => ProvaController::STATUS,
            'cets' => $cets,
        ]);


   }

}