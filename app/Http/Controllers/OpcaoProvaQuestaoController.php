<?php

namespace App\Http\Controllers;

use App\OpcaoProvaQuestao;
use App\Http\Controllers\OpcaoController;
use Illuminate\Support\Facades\DB;

class OpcaoProvaQuestaoController extends Controller
{

    private function buscaOpcoesRandom($idQuestao, $idTipoProva){

        $opcaoObj = new OpcaoController();
        if( $idTipoProva > 1 ){
            
            return $opcaoObj->retornaOpcoesPorQuestao($idQuestao)->shuffle();
        } else {
            
            return $opcaoObj->retornaOpcoesPorQuestao($idQuestao);
        }
    } 

    
    public function gravaOpcaoProva($idProvaQuestao, $idQuestao){

        $opcoesRandom = $this->buscaOpcoesRandom($idQuestao, 2);

        foreach($opcoesRandom as $opcao):
            OpcaoProvaQuestao::create([
                'id_prova_questao' => $idProvaQuestao,
                'id_opcao' => $opcao->id_opcao,
                'valor' => 0
            ]);
        endforeach;
    }
    
    
    public function gravaOpcoesProva( $dados, $matriz ){
        
        foreach($dados as $value):
            
            $opcoesRandom = $this->buscaOpcoesRandom( $value['id_questao'], $matriz->id_tipo_prova );
            foreach($opcoesRandom as $opcao):
                $dadosOpcao[] = ['id_prova_questao' => $value['id_prova_questao'], 'id_opcao' => $opcao->id_opcao, 'valor' => 0];
            endforeach;
        endforeach;
        DB::table('opcao_prova_questao')->insert($dadosOpcao);
    }
    
    public function opcoesCorretasPorIdProva($idProva){
        return DB::select(DB::raw("select * from prova_questao inner join opcao_prova_questao on prova_questao.id_prova_questao = opcao_prova_questao.id_prova_questao inner join opcao on opcao_prova_questao.id_opcao = opcao.id_opcao and (opcao_prova_questao.valor = opcao.valor or opcao.valor = 3) where prova_questao.id_prova = {$idProva}"));
    }

    public function opcoesPorIdProva($idProva){
        return DB::select(DB::raw("select * from prova_questao inner join opcao_prova_questao on prova_questao.id_prova_questao = opcao_prova_questao.id_prova_questao inner join opcao on opcao_prova_questao.id_opcao = opcao.id_opcao where prova_questao.id_prova = {$idProva}"));
    }

    public function resumo( $idProva ){
        
        $opcoes = $this->opcoesPorIdProva( $idProva );
        $opcoesCorretas = $this->opcoesCorretasPorIdProva( $idProva );
        $valorPorOpcao =10/count($opcoes);

        return [
            'opcoes' => $opcoes,
            'opcoesCorretas' => $opcoesCorretas,
            'valorPorOpcao' => $valorPorOpcao,
        ];
    }

    public function opcoesCompletasPorQuestao($idProvaQuestao){
        return DB::select(DB::raw("SELECT prova_questao.id_prova_questao, opcao_prova_questao.id_opcao, opcao.descricao, opcao_prova_questao.valor FROM `prova_questao` inner join opcao_prova_questao on opcao_prova_questao.id_prova_questao = prova_questao.id_prova_questao inner join opcao on opcao_prova_questao.id_opcao = opcao.id_opcao and prova_questao.id_questao = opcao.id_questao where prova_questao.id_prova_questao =  {$idProvaQuestao}"));
    }

    public function opcoesPorQuestao($idProvaQuestao){
        return OpcaoProvaQuestao::where('id_prova_questao', $idProvaQuestao);
    }

    public function retornaOpcaoEmBranco($valor){

        if($valor == 0)
            return true;

        return false;
    }

    public function gravaRespostas($idProvaQuestao, array $inputOpcoes){
        
        $opcoes = $this->opcoesPorQuestao($idProvaQuestao);
        foreach($opcoes->get() as $key => $opcao):

            $emBranco[] = $this->retornaOpcaoEmBranco($inputOpcoes[$key]);
            $opcao->valor = $inputOpcoes[$key];
            $opcao->save();
        endforeach;

        return $emBranco;
    }
}
