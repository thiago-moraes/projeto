<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeExtension extends Model
{
    protected $table = 'time_extension';
    public $timestamps = false;
    protected $fillable = [
        'candidato', 'dt_fim'
    ];
    protected $primaryKey = 'id';
}
