<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatrizSGQ extends Model
{
    protected $table = 'MATRIZ';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'topico','autor', 'ano', 'id_questao', 'nivel', 'anome', 'ordem', 'prova'];
    protected $connection = 'mysql_sbahq';

    public function questoes(){
        return $this->hasOne(QuestoesSGQ::class, 'id', 'idquestao');
    }
}