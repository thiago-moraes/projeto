<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prova extends Model
{
    protected $table = 'prova';
    protected $primaryKey = 'id_prova';
    protected $fillable = ['id_candidato', 'id_matriz', 'status', ];
    
    
    public function dadosCandidato(){
        return $this->belongsTo('App\Candidato','id_candidato','id');
    }
    
    public function dadosCodigos(){
        return $this->hasOne("App\Codigos", 'codigo', 'status')->where('grupo',1);
    }
    
    public function matriz(){
        return $this->hasOne('App\Matriz','id_matriz','id_matriz')->first();
    }
}