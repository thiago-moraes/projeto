<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Http\Controllers\ProvaQuestaoController;
use App\Http\Controllers\CandidatoController;
use Illuminate\Support\Facades\DB;
use App\Matriz;
use App\Prova;

class GeraProvasPorMatriz implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $candidatos;
    protected $idMatriz;
    protected $provaQuestao;
    protected $candidatoController;

    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($idMatriz)
    {
        $this->idMatriz = $idMatriz;
        $this->provaQuestao = new provaQuestaoController();
        $this->candidatoController = new CandidatoController();
    }

    protected function candidatos()
    {
        
        $candidatos = $this->candidatoController->candidatosPorMatriz( $this->idMatriz );
        $this->candidatos = $candidatos->get();
    }

    protected function criaQuestoes( $provas, $dadosMatriz )
    {

        foreach($provas as $prova):
            $this->provaQuestao->criaQuestoesProvaAdm( $dadosMatriz, $prova->id_prova);
        endforeach;
    }

    protected function dadosMatriz()
    {
        return Matriz::find($this->idMatriz);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->candidatos();
        

        foreach($this->candidatos as $candidato):
            $dados[] = ['id_candidato' => $candidato->id, 'id_matriz' => $candidato->id_matriz, 'status' => 0, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")];
        endforeach;

        DB::table('prova')->insert($dados);
        $this->criaQuestoes( Prova::where('id_matriz', $this->idMatriz)->get(), $this->dadosMatriz() );

       return true;
    }
}
