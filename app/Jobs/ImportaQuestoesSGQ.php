<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\ProvaSGQ;
use App\Questao;
use Illuminate\Support\Facades\DB;

class ImportaQuestoesSGQ implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $prova;
    protected $ano;
    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($prova, $ano)
    {
        $this->prova = $prova;
        $this->ano = $ano;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prova = ProvaSGQ::where('id', $this->prova)->first();
        $itensMatriz = $prova->matriz($this->ano)->get();
        
        foreach ($itensMatriz as $item) {

            $quest = $item->questoes()->first();

            preg_match('/\d+\.?\d{0,}/', $item->topico, $topico);

            $questao = DB::table('questao')->insert([
                'ano' => $this->ano,
                'dificuldade' => 1,
                'ponto' => $topico[0] ?? 0,
                'ind_me' => $item->anome,
                'descricao' => $this->strip($quest->cabecalho),
                'justificativa' => '',
                'referencia' => '',
                'autor' => $item->autor,
                'view' => 0,
                'id_tipo_prova' => $this->prova
            ]);

            $questao = DB::table('questao')->orderBy('id_questao', 'desc')->first();

            DB::table('opcao')->insert([
                ['id_questao' => $questao->id_questao, 'descricao' => $quest->opcaoa, 'valor' => ($quest->val_opcaoa) + 1],
                ['id_questao' => $questao->id_questao, 'descricao' => $quest->opcaob, 'valor' => ($quest->val_opcaob) + 1],
                ['id_questao' => $questao->id_questao, 'descricao' => $quest->opcaoc, 'valor' => ($quest->val_opcaoc) + 1],
                ['id_questao' => $questao->id_questao, 'descricao' => $quest->opcaod, 'valor' => ($quest->val_opcaod) + 1],
            ]);

            if (!is_null($item->val_opcaoe)&& $item->val_opcaoe !== '') {
                DB::table('opcao')->insert(['id_questao' => $questao->id_questao, 'descricao' => $quest->opcaoe, 'valor' => ($quest->val_opcaoe) + 1]);
            }
        }

        return true;
    }
    
    private function strip($str) 
    {
        return strip_tags($str, '<sup><sub><i><b><strong>');
    }
}
