<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class EncriptaSenhasCandidato implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $candidato;
    public $tries = 3;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($candidato)
    {
        $this->candidato = $candidato;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach($this->candidato as $candidato):
            $candidato->password = bcrypt($candidato->pwd_plain_text);
            $candidato->save();
        endforeach;
        
        return $this->candidato;
    }
}
