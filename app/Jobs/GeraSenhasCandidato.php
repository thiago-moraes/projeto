<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GeraSenhasCandidato implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $candidato;
    public $tries = 3;
    const SENHA = '23456789ABCDEFGHJKMNPQRSTUVWXYZ';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($candidato)
    {
        $this->candidato = $candidato;
    }


    protected function shuffleSenha()
    {
        return substr(str_shuffle(self::SENHA), 0, 8);
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach($this->candidato as $candidato):
            $candidato->pwd_plain_text = $this->shuffleSenha();
            $candidato->password = bcrypt($candidato->pwd_plain_text);
            $candidato->save();
        endforeach;

        return $this->candidato;
    }
}
