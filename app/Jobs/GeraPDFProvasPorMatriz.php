<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Http\Controllers\CandidatoController;
use App\Matriz;
use App\Prova;
use App\TipoProva;
use App\Notas;

class GeraPDFProvasPorMatriz implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $candidatos;
    protected $matriz;
    protected $provaQuestao;
    protected $candidatoController;

    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($idMatriz)
    {
        $this->matriz = Matriz::find($idMatriz);
        $this->candidatoController = new CandidatoController();
    }

    protected function candidatos()
    {

        $candidatos = $this->candidatoController->candidatosPorMatriz($this->matriz->id_matriz);
        $this->candidatos = $candidatos->get();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $this->candidatos();
  
        foreach ($this->candidatos as $candidato) :

            $prova = new Prova;
            $provaDados = $prova->where("id_candidato", $candidato->id)->where('id_matriz', $candidato->id_matriz)->where('status', 2)->first();
            
            if (isset($provaDados->responsible)) {
                
                if (is_null($provaDados->token) || $provaDados->token == '') {
                    $provaDados->token = MD5($candidato->id);
                    $provaDados->save();
                }

                $questoes = \App\ProvaQuestao::join("questao", "questao.id_questao", "=", "prova_questao.id_questao")
                    ->where("prova_questao.id_prova", $provaDados->id_prova);
                
                $tipoProva = TipoProva::where("id_tipo_prova", $this->matriz->id_tipo_prova)->get();

                \PDF::loadView($this->retornaView($this->matriz->id_tipo_prova), [
                    "questoes" => $questoes->get(),
                    "dados" => $tipoProva,
                    "prova" => $provaDados,
                    "candidato" => $candidato,
                    "nota" => Notas::where('id_candidato', $provaDados->id_candidato)->where("id_matriz", $provaDados->id_matriz)->first() ?? 0,
                ])->save(public_path() . "/prova/{$provaDados->token}.pdf")->stream();
             }
        endforeach;

        return true;
    }
    
    private function retornaView($tipoProva)
    {
        $view = "admin.nota-candidatopdf";
        if ($tipoProva === 1) {
            $view = "admin.nota-tea-candidatopdf";
        }

        return $view;
    }
}
