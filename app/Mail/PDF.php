<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class PDF extends Mailable
{

    public $candidato;
    public $token;
    public $matriz;
    const EMAIL_TEMPLATE = [
        1 => 'emails.pdf-tea',
        2 => 'emails.pdf',
    ];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    //public function __construct($candidato, $token)
    public function __construct( $candidato, $token, $matriz )
    {
        $this->token = $token;
        $this->candidato = $candidato;
        $this->matriz = $matriz;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       return $this->view(self::EMAIL_TEMPLATE[$this->matriz->id_tipo_prova]);
    }
}
