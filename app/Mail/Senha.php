<?php

namespace App\Mail;

use App\Candidato;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Senha extends Mailable
{

    public $candidato;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Candidato $candidato)
    {
        $this->candidato = $candidato;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       return $this->view('emails.senha');
    }
}
