<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Candidato;



class PDF extends Mailable
{
    use Queueable, SerializesModels;
    
    public $candidato;
    public $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($candidato, $token)
    {   
        $this->candidato = $candidato;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       return $this->view('emails.pdf-me1');
    }
}
