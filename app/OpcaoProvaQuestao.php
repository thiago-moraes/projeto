<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpcaoProvaQuestao extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_opcao','id_prova_questao', 'valor'
    ];
    protected $table = 'opcao_prova_questao';
    public  $timestamps = false;
}
