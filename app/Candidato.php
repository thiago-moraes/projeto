<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable as Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Candidato extends Authenticatable
{
    use Notifiable;

    protected $table = 'candidatos';
    protected $primaryKey = 'id';
    protected $fillable = ['email','cet', 'cpf', 'password', 'nome', 'id_matriz', 'id_pessoa', 'celular',];
    protected $hidden = ['password', 'pwd_plain_text', 'remember_token'];


    public function sessao(){
        return $this->belongsTo('\App\Sessions', 'user_id', 'id');
    }
    
    public function prova($idCandidato){
        
        return $this->belongsTo('App\Prova', 'id_matriz', 'id_matriz')->where('id_candidato', $idCandidato)->first();
    }
    
    public function matriz(){
        return $this->hasOne(\App\Matriz::class, 'id_matriz', 'id_matriz');
    }

}
