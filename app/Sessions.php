<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sessions extends Model
{
    public $timestamps = false;
    
    public function candidato(){
        return $this->hasOne('App\Candidato', 'id', 'user_id');
    }
    
}
