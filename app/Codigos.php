<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Codigos extends Model
{
    protected $fillable = ['codigo', 'grupo', 'descricao'];
    public $timestamps = false;
    protected $table = 'codigos';
    protected $primaryKey = 'id';
    
}
