<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questao extends Model
{
    protected $table = 'questao';
    protected $primaryKey = 'id_questao';
    protected $fillable = [
        'ano',
        'dificuldade',
        'ponto',
        'ind_me',
        'descricao',
        'justificativa',
        'referencias',
        'autor',
        'status',
        'view',
        'id_tipo_prova'
    ];
    public $timestamps =  false;
}
