<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProva extends Model
{
    protected $table = 'tipo_prova';
    public $timestamps = false;
    protected $fillable = [
        'id_tipo_prova', 'descricao', 'slug'
    ];
    protected $primaryKey = 'id_tipo_prova';
}
