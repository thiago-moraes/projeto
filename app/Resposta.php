<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resposta extends Model
{
    protected $table = 'resposta';
    protected $primaryKey = 'id_resposta';
    protected $fillable = [
        'id_candidato',
        'id_prova',
        'id_questao',
        'id_opcao',
        'id_resposta',
        'log',
    ];
}
