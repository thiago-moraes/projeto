<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MatrizSGQ;

class ProvaSGQ extends Model
{
    protected $table = 'PROVAS';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'descricao'];
    protected $connection = 'mysql_sbahq';

    public function matriz($ano){
        return $this->belongsTo(MatrizSGQ::class, 'id', 'prova')->where('ano', $ano);
    }
}