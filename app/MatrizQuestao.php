<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatrizQuestao extends Model
{
    protected $primaryKey = 'id';
    public $table = 'MATRIZ';
    protected $connection = 'mysql_sba';
}
