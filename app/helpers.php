<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use App\Http\Controllers\OpcaoProvaQuestaoController;
use App\Http\Controllers\ProvaController;
use App\Http\Controllers\ProvaQuestaoController;
use App\Prova;

function retornaOpcoes($idQuestao, $idProvaQuestao){

    $optObj = new OpcaoProvaQuestaoController();

    $opcoesNaProva = $optObj->opcoesCompletasPorQuestao($idProvaQuestao);

    if(count($opcoesNaProva) == 0){

       $optObj->gravaOpcaoProva($idProvaQuestao, $idQuestao);
       $opcoesNaProva = $optObj->opcoesCompletasPorQuestao($idProvaQuestao);
    }

    return $opcoesNaProva;
}

function opcoesComResposta( $idProvaQuestao ){
    
    $optObj = new OpcaoProvaQuestaoController();
    $opcoesNaProva = $optObj->opcoesPorQuestao($idProvaQuestao);
    $opcoes = $opcoesNaProva->join("opcao", "opcao.id_opcao","=", "opcao_prova_questao.id_opcao")
            ->select("opcao_prova_questao.*", "opcao.valor as resposta", "opcao.descricao");
    
    return $opcoes->get();
}

function verificaSeCandidatoEstaNaProvaCorreta( $idProva ){

    $prova = Prova::find( $idProva );
    if( $prova->status == 2){
        return redirect()->action("ProvaController@finalizaProva", ['idProva' => $idProva ]);
    }

    $idMatriz = Auth::user()->id_matriz;
    $idCandidato = Auth::user()->id;
    $provaController = new ProvaController();
    $dados = $provaController->buscaProvaPorIdCandidato($idCandidato, $idMatriz);

    if( $dados->id_prova != $idProva ){

        print '<h4>Erro, ao confirmar sua prova. Por favor clique <a href="/prova/">aqui</a> para voltar a sua prova.</h4>';
        die();
    }
}

function verificaSelecionado($item, $selecionado, $tipo = "select"){
    $slug = 'selected';
    if ($tipo !== "select") {
        $slug = 'checked';
    }

    if((string) $item == (string) $selecionado)
        return $slug;

    return '';
}

function totalDeQuestoesNaoRespondidas($idProva){

    $provaQuestaoController = new ProvaQuestaoController();
    return $provaQuestaoController->totalQuestoesPorStatus($idProva, 1)->get()->count();
}

function totalDeQuestoesParcialmenteRespondidas($idProva){

    $provaQuestaoController = new ProvaQuestaoController();
    return $provaQuestaoController->totalQuestoesPorStatus($idProva, 2)->get()->count();
}

function totalDeQuestoesRespondidas($idProva){

    $provaQuestaoController = new ProvaQuestaoController();
    return $provaQuestaoController->totalQuestoesPorStatus($idProva, 3)->get()->count();
}

function indiceAlfaNumerico($indice) {
    $arr = ['A','B','C','D','E'];

    return $arr[$indice];
}

function formataData($data){
    
    if( $data ){
        $dataFormatada = new \DateTime($data, new DateTimeZone( "America/Sao_Paulo"));
        return $dataFormatada;
    }
    
    return '';
}

function retornaVerdadeiroFalso( $i ){
    $arr = ['B', 'F', "V"];
    return $arr[$i];
}

function formataNumero( $numero ){
    
    if( $numero )
        return number_format ($numero, '2',',','.');
    
    
    return number_format (0, '2',',','.');
}

function verificaTimeExtension( $id ){

    $dt_fim = \App\TimeExtension::where("candidato", $id)->orderBy("id", "DESC")->first();
    if(!is_null($dt_fim)){
        return $dt_fim->dt_fim;
    }

    return NULL;
}

function dataHoraProva(){

    $dataAtual = new \DateTime();
    $matrizes = \App\Matriz::where('dt_inicio', ">=", $dataAtual->format("Y-m-d H:i:s"))->orderBy("dt_inicio")->first();
    if(!is_null($matrizes)){
        $diferenca = $dataAtual->diff((new DateTime($matrizes->dt_inicio)));
        if( $diferenca->format("%H") < 1 && $diferenca->format("%i") < 21){
            return $matrizes->dt_inicio;
        }
        
    }

    return false;
}