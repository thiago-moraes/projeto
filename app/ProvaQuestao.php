<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvaQuestao extends Model
{
    protected $table = 'prova_questao';
    protected $primaryKey = 'id_prova_questao';
    protected $fillable = [
        'id_questao',
        'id_prova',
        'status',
        'sequencia',
    ];
    public $timestamps = false;
    
    public function dadosQuestao(){
        return $this->hasOne("App\Questao", "id_questao", "id_questao");
    }
}