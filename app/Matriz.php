<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matriz extends Model
{
    protected $table = 'matriz';
    protected $primaryKey = 'id_matriz';
    protected $fillable = ['ano', 'ind_me', 'trim', 'qtd_questoes', 'ponto_ini', 'ponto_fim', 'ano_questao', 'qtd_facil', 'qtd_medio', 'qtd_dificil', 'id_tipo_prova', 'status','dt_inicio', 'dt_fim', 'senha' ];
    public $timestamps = false;


    public function tipoProva(){
        return $this->hasOne('App\TipoProva', 'id_tipo_prova', 'id_tipo_prova')->get();
    }
    
    public function dadosStatus(){
        return $this->hasOne('App\Codigos', 'codigo', 'status')->where('grupo',2)->get();
    }
    
}