<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestoesSGQ extends Model
{
    protected $table = 'QUESTAO';
    protected $primaryKey = 'id';
    // protected $fillable = ['id', 'topico','autor', 'ano', 'id_questao', 'nivel', 'anome', 'ordem', 'prova'];
    protected $connection = 'mysql_sbahq';

}