function tempoDeProva(fimProva, inicioProva) {
    var url = window.location.href;

    posicao = url.lastIndexOf('/');
    pagina = url.substring(posicao, url.leng);

    var finalizar = document.getElementById('finalizar');
    
    if( fimProva == inicioProva){
        document.getElementById('tempo-de-prova').innerHTML = formataCronometro(0, 0, 0);
        if( pagina != "/" && pagina != "/avaliacao"){
            finalizar.value = 2;
            return;
        }
        return;
    }

    var stringFim = fimProva.split(',');
    var stringIni = inicioProva.split(',');

    var fimProva = new Date( stringFim[0],(stringFim[1]-1),stringFim[2],stringFim[3],stringFim[4],stringFim[5] );
    var data = new Date( stringIni[0],stringIni[1],stringIni[2],stringIni[3],stringIni[4],stringIni[5] );

    data.setTime(data.getTime() + 1000);

    var segundo = (59 + (fimProva.getSeconds() - data.getSeconds())) % 60;
    var minuto = (59 + (fimProva.getMinutes() - data.getMinutes())) % 60;
    var hora = parseInt((24 + (fimProva.getHours() - data.getHours())) % 24);

    if (minuto == 59 && segundo == 59)
        hora2 -= 1;

    if( document.getElementById('tempo-de-prova') != null){
        document.getElementById('tempo-de-prova').innerHTML = formataCronometro(hora2, minuto, segundo);
    }

    if (hora <= 0 && minuto <= 0) {
        if (segundo <= 0) {
            if( pagina != "/" && pagina != "/avaliacao"){
                finalizar.value = 2;
		        return;
            }
        }
    }

    setTimeout(function () {

        tempoDeProva(formataData(fimProva), formataData(data));
    }, 1000);
}

function formataCronometro(hora, minuto, segundo) {

    if (hora <= 9)
        hora = "0" + hora;
    if (minuto <= 9)
        minuto = "0" + minuto;
    if (segundo <= 9)
        segundo = "0" + segundo;

    return hora + ':' + minuto + ':' + segundo;
}

function formataData( data ){
    return data.getFullYear() + ',' + data.getMonth() + ',' + data.getDay() + ',' + data.getHours() + ',' + data.getMinutes() + ',' + data.getSeconds();
}
