<?php

namespace App\Http\Controllers;

use App\Prova;
use App\Http\Controllers\OpcaoProvaQuestaoController;
use App\Notas;
use Illuminate\Support\Facades\Auth;

class NotaController extends Controller
{

    public function gravaNota($idProva){

        $dadosNota = $this->dadosNota( $idProva );

        $prova = Prova::find($idProva);
        $opcaoProvaQuestaoController = new OpcaoProvaQuestaoController();
        $resumo = (object) $opcaoProvaQuestaoController->resumo( $idProva );
        
        $nota['id_candidato'] = $prova->id_candidato;
        $nota['id_matriz'] = $prova->id_matriz;
        $nota['nota'] = number_format($resumo->valorPorOpcao * count( $resumo->opcoesCorretas ), 2);
        $nota['aproveitamento'] = number_format((count( $resumo->opcoesCorretas ) / count( $resumo->opcoes ))*100,2);
        $nota['id_prova'] = $idProva;

        if($dadosNota->count() == 0){
            $dadosNota = Notas::create($nota)->where("id_prova", $idProva)->where("id_candidato", Auth::user()->id);
        }else{
            
            $dadosNota->first();
            $dadosNota->update($nota);
        }

        return $dadosNota->first();
    }

    public function dadosNota( $idProva ){

        return Notas::where('id_prova', '=', $idProva);
    }

}
