<?php $__env->startSection('content'); ?>

<div class="row">

    <br>
    <?php if(count($questoes) > 0): ?>
        <div class="col-md-12 text-center">
            <h3>Lista de questões não respondidas</h3>
            <h4>Clique na questão em que deseja acessar.</h4>
            <br>
        </div>

        <div class="col-md-offset-2 col-md-8 text-center" style="font-size: 1.3em">
            <?php $__currentLoopData = $questoes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $questao): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <a href="/prova/objetiva/<?php echo e($idProva); ?>/<?php echo e($questao->sequencia); ?>"><?php echo e($questao->sequencia); ?> - <?php echo $questao->descricao; ?></a> <br><br>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    <?php endif; ?>

</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>