<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <h2>Relatório de Candidatos</h2>
        <hr>
    </div>
</div>

<form method="get" action="" id="form-filtro" class="form-inline text-right">
    <select name="tipo" onchange="document.getElementById('form-filtro').submit()" class="form-control">
        <option value="">-</option>
        <?php $__currentLoopData = $tipo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($key); ?>" <?php echo e(@verificaSelecionado($key, $_GET['tipo'] ?? '')); ?>> <?php echo e($item); ?> </option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
    
    <select name="ano" onchange="document.getElementById('form-filtro').submit()" class="form-control">
        <option value="">-</option>
        <?php $__currentLoopData = $anos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($item->ano); ?>" <?php echo e(@verificaSelecionado($item->ano, $_GET['ano'] ?? date("Y"))); ?>> <?php echo e($item->ano); ?> </option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>

    <select name="me" onchange="document.getElementById('form-filtro').submit()" class="form-control">
        <option value="">-</option>
        <?php $__currentLoopData = $me; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($key); ?>" <?php echo e(@verificaSelecionado($key, $_GET['me'] ?? '')); ?>> <?php echo e($item); ?> </option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>

    <select name="trim" onchange="document.getElementById('form-filtro').submit()" class="form-control">
        <option value="">-</option>
        <?php $__currentLoopData = $trim; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($key); ?>" <?php echo e(@verificaSelecionado($key, $_GET['trim'] ?? '')); ?>> <?php echo e($item); ?>º Trimestre </option>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>

    <div class="input-group">
        <input name="c" class="form-control" value="<?php echo e(request()->input('c')); ?>">
        <span class="input-group-btn"><button class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button></span>
    </div>

</form>

<div class="row">

    <?php if(session('mensagem')): ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info"> <?php echo e(session('mensagem')); ?> </div>
    </div>
    <?php endif; ?>
    
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>CPF</th>
                    <th>Celular</th>
                    <th>Matriz</th>
                    <th>Ano ME</th>
                    <th>Trim</th>
                    <th colspan="4"></th>
                </tr>
            </thead>
            <tbody>
        <?php $__currentLoopData = $dados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td><?php echo e($dado->getNome()); ?></td>
                <td><?php echo e($dado->getCPF()); ?></td>
                <td><?php echo e($dado->getCelular()); ?></td>
                <td><?php echo e($dado->getDadosMatriz()->getDescricao()); ?></td>
                <td><?php echo e($me[$dado->getDadosMatriz()->getIndME()]); ?></td>
                <td><?php echo e($dado->getDadosMatriz()->getTrim()); ?></td>
                <td>
                    <form action="/admin/delete/candidato" method="post">
                        <?php echo e(csrf_field()); ?>

                        <input type="hidden" name="id" value="<?php echo e($dado->getId()); ?>">
                        <div class="btn-group pull-right">
                            <a href='/admin/candidato/<?php echo e($dado->getId()); ?>/edit' class="btn btn-primary" title="Editar cadastro"><span class="glyphicon glyphicon-edit"></span></a>
                            <a href="/admin/candidato/<?php echo e($dado->getId()); ?>/mailprova" class="btn btn-warning" title="Enviar prova"><span class="glyphicon glyphicon-file"></span></a>
                            <a href="/admin/send/senha/<?php echo e($dado->getId()); ?>" class="btn btn-info confirma" title="Senha por e-mail"><span class="glyphicon glyphicon-envelope"></span></a>
                            <a href="/admin/send/senha/sms/<?php echo e($dado->getId()); ?>" class="btn btn-success confirma" title="Senha por SMS"><span class="glyphicon glyphicon-phone"></span></a>
                            <button class="btn btn-danger confirma" title="Excluir candidato"><span class="glyphicon glyphicon-trash"></span></button>
                        </div>
                        
                </form>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        <tfoot>
            <tr>
                <th colspan="1">Total de registros encontrados: <?php echo e($total); ?></th>
                <th colspan="6"><?php echo e($links); ?></th></tr>
        </tfoot>
        </table>
    </div>

</div>
<script>
    jQuery(function($){
        $(".confirma").click(function(){
           return confirm("Deseja realmente " + $(this).attr("title"));
        });
    })
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>