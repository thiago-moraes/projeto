<style>
    h4 {
        margin-bottom: 1.5em !important;
    }

    h2 {
        margin-bottom: 2em !important;
        text-decoration: underline;
    }
</style>
<?php $__env->startSection('content'); ?>

    <h2>Prova finalizada!</h2>


    <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST">

        <?php echo e(csrf_field()); ?>

        <button class="btn btn-success">Sair</button>
    </form>

<?php $__env->stopSection(); ?>
<script>
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
</script>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>