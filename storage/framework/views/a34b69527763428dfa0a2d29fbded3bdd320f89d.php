<div class="form-group">

    <?php echo e(csrf_field()); ?>

    
    <input type="hidden" class="form-control" name="id" value="<?php echo e($dados->getId()); ?>">
    <div class="col-md-12 <?php echo e($errors->first('id_pessoa') ? 'has-error' : ''); ?>">
        <label class="control-label">ID Pessoa</label>
        <input type="text" class="form-control" name="id_pessoa" value="<?php echo e(old('id_pessoa', $dados->getIdPessoa())); ?>">
        <span id="helpBlock" class="help-block"> <?php echo e($errors->first('id_pessoa')); ?></span>
    </div>
    <div class="col-md-12 <?php echo e($errors->first('nome') ? 'has-error' : ''); ?>">
        <label class="control-label">Nome</label>
        <input class="form-control" name="nome" value="<?php echo e(old('nome', $dados->getNome())); ?>" placeholder="Nome">
        <span id="helpBlock" class="help-block"> <?php echo e($errors->first('nome')); ?></span>
    </div>
    
    <div class="col-md-12 <?php echo e($errors->first('cpf') ? 'has-error' : ''); ?>">
        <label class="control-label">CPF</label>
        <input class="form-control" name="cpf" value="<?php echo e(old('cpf', $dados->getCPF())); ?>" placeholder="CPF (Somente números)">
        <span id="helpBlock" class="help-block"> <?php echo e($errors->first('cpf')); ?></span>
    </div>
    
    <div class="col-md-12 <?php echo e($errors->first('cet') ? 'has-error' : ''); ?>">
        <label class="control-label">CET</label>
        <input class="form-control" name="cet" value="<?php echo e(old('cet', $dados->getCET())); ?>" placeholder="CET (Somente números)">
        <span id="helpBlock" class="help-block"> <?php echo e($errors->first('cet')); ?></span>
    </div>
    
    <div class="col-md-12 <?php echo e($errors->first('email') ? 'has-error' : ''); ?>">
        <label class="control-label">Email</label>
        <input class="form-control" name="email" value="<?php echo e(old('email', $dados->getEmail())); ?>" placeholder="Email">
        <span id="helpBlock" class="help-block"> <?php echo e($errors->first('email')); ?></span>
    </div>
    
    <div class="col-md-12 <?php echo e($errors->first('celular') ? 'has-error' : ''); ?>">
        <label class="control-label">Celular</label>
        <input class="form-control" name="celular" value="<?php echo e(old('celular', $dados->getCelular())); ?>" placeholder="Celular (99)9999-9999">
        <span id="helpBlock" class="help-block"> <?php echo e($errors->first('celular')); ?></span>
    </div>
    
    
    <div class="col-md-12 <?php echo e($errors->first('senha') ? 'has-error' : ''); ?>">
        <label class="control-label">Senha</label>
        <input class="form-control" name="password" value="" placeholder="Senha">
        <span id="helpBlock" class="help-block"> <?php echo e($errors->first('senha')); ?></span>
    </div>
    
    <div class="col-md-12 <?php echo e($errors->first('id_matriz') ? 'has-error' : ''); ?>">
        <label class="control-label">Matriz</label>
        <select class="form-control" name="id_matriz">
            <option value="">-</option>
            <?php $__currentLoopData = $matrizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $matriz): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($matriz->id_matriz); ?>" <?php echo e(@verificaSelecionado($matriz->id_matriz, $dados->getIdMatriz())); ?>><?php echo e($matriz->id_matriz); ?> - <?php echo e($matriz->descricao); ?> <?php echo e($matriz->tipoProva()[0]->descricao); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <span id="helpBlock" class="help-block"> <?php echo e($errors->first('id_matriz')); ?></span>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-8 col-md-2">
        <button type="button" onclick="history.back()" class="btn btn-danger btn-block">Cancelar</button>
    </div>
    <div class="col-md-2">
        <button type="submit" class="btn btn-success btn-block">Salvar</button>
    </div>
</div>