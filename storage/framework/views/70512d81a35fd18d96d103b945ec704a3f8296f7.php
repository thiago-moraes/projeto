<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <h2>Relatório de Provas</h2>
            <hr>
        </div>
    </div>

    <div class="row">

        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="btn-group">
                <a href="/admin/prova/online" class="btn btn-default">Online</a>
            </div>
        </div>


        <form  method="get" action="" id="form-filtro" class="form-inline">
            <div class="col-md-12 col-xs-12 col-sm-12">

                <div class="form-group">
                    <label class="control-label">Tipo prova</label><br>
                    <select name="tipo" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        <?php $__currentLoopData = $tipo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($key); ?>" <?php echo e(@verificaSelecionado($key, $_GET['tipo'] ?? '')); ?>> <?php echo e($item); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Ano</label><br>
                    <select name="ano" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        <?php $__currentLoopData = $anos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($item->ano); ?>" <?php echo e(@verificaSelecionado($item->ano, $_GET['ano'] ?? date("Y"))); ?>> <?php echo e($item->ano); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Ano ME</label><br>
                    <select name="me" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        <?php $__currentLoopData = $me; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($key); ?>" <?php echo e(@verificaSelecionado($key, $_GET['me'] ?? '')); ?>> <?php echo e($item); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                
                <div class="form-group">
                    <label class="control-label">Trimestre</label><br>
                    <select name="trim" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        <?php $__currentLoopData = $trim; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($key); ?>" <?php echo e(@verificaSelecionado($key, $_GET['trim'] ?? '')); ?>> <?php echo e($item); ?>º Trimestre </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                
                <div class="form-group">
                    <label class="control-label">Cet</label><br>
                    <select name="cet" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        <?php $__currentLoopData = $cets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($item); ?>" <?php echo e(@verificaSelecionado($item, $_GET['cet'] ?? '')); ?>> <?php echo e($item); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="form-group">
                    <label class="control-label">Status</label><br>
                    <select name="s" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        <?php $__currentLoopData = $status; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($key); ?>" <?php echo e(@verificaSelecionado($key, $_GET['s'] ?? '')); ?>> <?php echo e($item); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="input-group">
                    <label class="control-label">Candidato</label><br>
                    <input name="c" class="form-control" value="<?php echo e(request()->input('c')); ?>" placeholder="Nome do candidato">
                    <span class="input-group-btn"><button class="btn btn-success" style="margin-top: 1.6em;"><span class="glyphicon glyphicon-search"></span></button></span>
                </div>
            </div>
        </form>

        <div class="col-md-12 col-sm-12 col-xs-12"><hr></div>
    <?php if(session('mensagem')): ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info"> <?php echo e(session('mensagem')); ?> </div>
    </div>
    <?php endif; ?>
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Candidato</th>
                    <th>Matriz</th>
                    <th>Status</th>
                    <th>Ano</th>
                    <th>Ano ME</th>
                    <th>CET</th>
                    <th>Trim</th>
                    <th>Tipo</th>
                    <th>Início</th>
                    <th>Fim</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

            <?php if(count($dados) > 0): ?>
                <?php $__currentLoopData = $dados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <tr>
                        <td><?php echo e($dado->getCandidato()->nome); ?></td>
                        <td><?php echo e($dado->getMatriz()->descricao); ?></td>
                        <td><?php echo e($status[$dado->getStatus()]); ?></td>
                        <td><?php echo e($dado->getMatriz()->ano); ?></td>
                        <td><?php echo e($me[$dado->getMatriz()->ind_me]); ?></td>
                        <td><?php echo e($dado->getCandidato()->cet); ?></td>
                        <td><?php echo e($dado->getMatriz()->trim); ?></td>
                        <td><?php echo e($tipo[$dado->getMatriz()->id_tipo_prova]); ?></td>
                        <td><?php echo e($dado->getInicio()->format('d/m/Y H:i:s')); ?> </td>
                        <td> <?php echo e($dado->getStatus() == 2 ? $dado->getFim()->format('d/m/Y H:i:s') : ''); ?></td>
                        <td>
                            <form action="/admin/delete/prova" method="post">
                                <?php echo e(csrf_field()); ?>

                                <input type="hidden" name="id_prova" value="<?php echo e($dado->getIdProva()); ?>">
                                <div  class="btn-group pull-right">
                                    <a href='/admin/edit/prova/<?php echo e($dado->getIdProva()); ?>' class="btn btn-primary" title="Editar prova"><span class="glyphicon glyphicon-edit"></span></a>
                                    <a href="/admin/add/time/<?php echo e($dado->getCandidato()->id); ?>" class="btn btn-success" title="Adicionar tempo a prova"><span class="glyphicon glyphicon-time"></span></a>
                                    <a href="/admin/cancela/prova/<?php echo e($dado->getIdProva()); ?>" class="btn btn-warning confirma" title="Finalizar a prova"><span class="glyphicon glyphicon-remove-circle"></span></a>
                                    <button class="btn btn-danger confirma" title="Excluir a prova "><span class="glyphicon glyphicon-trash"></span></button>
                                </div>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                <tr><td colspan="8">Nenhum registro encontrado</td></tr>
            <?php endif; ?>
            </tbody>
        <tfoot>
        <tr><th colspan="2">Total de registros encontrado: <?php echo e($total); ?></th><td colspan="15"><?php echo e($link); ?></td></tr>
        </tfoot>
        </table>
    </div>
    <script>
        jQuery(function($){
            $(".confirma").click(function(){
               return confirm("Deseja realmente " + $(this).attr("title"));
            });
        })
    </script>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>