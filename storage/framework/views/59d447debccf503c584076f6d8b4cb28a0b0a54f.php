 <style>
        
        .table {
            
            width: 100%;
        }
        
        .table tr {
            line-height: 2em;
        }
        
        .table tr:nth-child(odd) {
            
            border: solid 1px #e6e6e6;
            background: #e6e6e6;
            line-height: 2em;
        }
        
        .topo table{
            width: 100%;
        }

        
        h2 {
            margin: 2em;
            text-align: center;
        }
        
        .divisor {
            width: 100%;
        }
        
        .rodape{
            position:fixed;
            bottom: -70px;
            width: 100%;
            height: 50px;
            text-align: center;
            font-size: .8em;
        }
    </style>
    <div class="topo">
        <table>
            <tr>
                <td style="width: 12em"><img src="<?php echo e(public_path("/images/logo-sba.jpg")); ?>" width="160"></td>
                <td>
                    <table>
                        <tr><td>Nome:</td><td colspan="2"><?php echo e($candidato->nome); ?></td></tr>
                        <tr><td style="width: 3em">Data:</td><td colspan="2"> <?php echo e(formataData($prova->created_at)->format("d/m/Y")); ?></td></tr>
                    </table>
                </td>
            </tr>

        </table>
    </div>

    <div class="divisor">
        <h3 style="text-align: center">Relatório de respostas do candidato.</h3>
    </div>
    <table class="table" style="font-size: .8em">
        <?php $__currentLoopData = $questoes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr><td style="margin: .5em; font-weight: bold;"><?php echo e($key+1); ?>) <?php echo $value->descricao; ?></td></tr>
        <tr>
            <td>
                <?php $__currentLoopData = opcoesComResposta($value->id_prova_questao); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $dado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
                <?php if($dado->valor === 2): ?>  *  <?php endif; ?>
                <?php echo $dado->descricao; ?><br>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </td>
        </tr>
       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       <tr><td>Legenda</td></tr>
       <tr><td>Sua respostas está marcada com asterisco (*)</td></tr>
    </table>
    <div class="rodape">
        Documento gerado em <?= date("d/m/Y") ?> para <?= $candidato->nome ?>.
    </div>