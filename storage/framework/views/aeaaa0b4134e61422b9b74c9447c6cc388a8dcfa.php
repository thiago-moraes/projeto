<div class="form-group">

    <?php echo e(csrf_field()); ?>

    
    <input type="hidden" class="form-control" name="id_prova" value="<?php echo e($dados->getIdProva()); ?>">
    
    <div class="col-md-12">
        <label class="control-label">Candidato</label>
        <div class="form-control"><?php echo e($dados->getCandidato()->nome); ?></div>
    </div>
    
    <div class="col-md-12 <?php echo e($errors->first('id_matriz') ? 'has-error' : ''); ?>">
        <label class="control-label">Matriz</label>
        <select name="id_matriz" class="form-control">
            <option value=""> - </option>
            <?php $__currentLoopData = $matrizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $matriz): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($matriz->id_matriz); ?>" <?php echo e(verificaSelecionado($matriz->id_matriz, old('id_matriz', $dados->getIdMatriz()))); ?> ><?php echo e($matriz->descricao); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <span id="helpBlock" class="help-block"> <?php echo e($errors->first('id_matriz')); ?></span>
    </div>
    
    <div class="col-md-12 <?php echo e($errors->first('status') ? 'has-error' : ''); ?>">
        <label class="control-label">Status</label>
        
        <select name="status" class="form-control">
            <option value=""> - </option>
            <?php $__currentLoopData = $codigos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $codigo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($codigo->codigo); ?>" <?php echo e(verificaSelecionado($codigo->codigo, old('status', $dados->getStatus()))); ?> ><?php echo e($codigo->descricao); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <span id="helpBlock" class="help-block"> <?php echo e($errors->first('status')); ?></span>
    </div>
</div>
    

<div class="form-group">
    <div class="col-md-offset-10 col-md-2">
        <button type="submit" class="btn btn-success btn-block">Salvar</button>
    </div>
</div>