<?php $__env->startSection('content'); ?>

<div class="row">

    <br>

    <div class="col-md-12 text-center">
        <?php if(count($questoes)): ?>
        <h3>Existem questões não respondidas ou parcialmente respondidas em sua prova.</h3><br>
        <?php endif; ?>
    </div>
    <div class="col-md-offset-2 col-md-8 text-center" style="font-size: 1.3em">
        <?php $__currentLoopData = $questoes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $questao): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <a href="/prova/objetiva/<?php echo e($idProva); ?>/<?php echo e($questao->sequencia); ?>"><?php echo e($questao->sequencia); ?> - <?php echo $questao->descricao; ?></a> <br><br>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <h3 class="text-center">Deseja finalizar sua prova?</h3><br><br>
        <div class="row" id="botoes">
            <div class="col-md-offset-3 col-md-3">
                <form method="post" action="/prova/finaliza/confirma">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" value="<?php echo e($idProva); ?>" name="idProva">
                    <button class="btn btn-danger btn-block" type="submit">Sim</button>
                </form>
            </div>
            <div class="col-md-3">
                <a href="<?php echo URL::previous(); ?>" class="btn btn-success btn-block">Não</a>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>