<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="cache-control"   content="no-cache, no-store, max-age=0, must-revalidate" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="expires" content = "Mon, 22 jul 2006 11:12:01 GMT" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        <title><?php echo e(isset($matriz->descricao) ? $matriz->descricao : ''); ?> - <?php echo e(isset($matriz->ano) ? $matriz->ano : ''); ?></title>
        <!-- Styles -->
        <!-- <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>"> -->
        <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/prova.css')); ?>">
        <!-- Scripts -->
        <script>
         /*   history.pushState(null, null, location.href);
            window.onpopstate = function () {
               history.go(1);
           };*/

        </script>
        <script>
            window.Laravel = <?= json_encode(['csrfToken' => csrf_token(),]); ?>
        </script>
    </head>
    <script>
    window.history.forward();
    function noback(){
        window.history.forward();    
    }
    </script>
    <body onLoad="noback();" onpageshow="if(event.persisted) noback();" onunload="">
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                    <div class="container-fluid">

                        <div class="navbar-header" style="width: 100%">

                            <div class="col-xs-6 col-sm-4 col-md-4"><h4><?php echo e(isset($matriz->descricao) ? $matriz->descricao : ''); ?> - <?php echo e(isset($matriz->ano) ? $matriz->ano : ''); ?></h4></div>
                            <div class="col-xs-6 col-sm-4 col-md-4"><h4>Tempo restante: <span id="tempo-de-prova"></span></h4></div>

                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <!-- <div class="collapse navbar-collapse" id="app-navbar-collapse"> -->
                                    <!-- Right Side Of Navbar -->
                                    <ul class="nav navbar-nav navbar-right" style="margin-right:1em;">
                                        <!-- Authentication Links -->
                                        <?php if(auth()->guard()->guest()): ?>
                                        <li><a href="<?php echo e(url('/login')); ?>">Login</a></li>
                                        <?php else: ?>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                <?php echo e(Auth::user()->nome); ?> <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="<?php echo e(url('/logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                        Sair
                                                    </a>

                                                    <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST" style="display: none;">
                                                        <?php echo e(csrf_field()); ?>

                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                        <?php endif; ?>
                                    </ul>
                                <!-- </div> -->
                            </div>
                            <!-- Collapsed Hamburger -->
                            <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                                <span class="sr-only">Toggle Navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button> -->
                            <!-- Branding Image -->
                        </div>
                    </div>
            </nav>

            <div class="container">

                <!-- <div class="col-md-1" style="overflow-y: scroll; height: 650px;">
                    <h5>Atalho</h5>
                    <?php for($x=1;$x<$totalQuestoes; $x++): ?>
                    <a href="<?php echo e($x); ?>" class="btn btn-default btn-block"><?php echo e($x); ?></a>
                    <?php endfor; ?>
                </div>
                -->
                <div class="col-md-12 conteudo">

                    <?php echo $__env->yieldContent('content'); ?>

                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 total-respondidas">
                            Questões respondidas: <span class="total"><?= totalDeQuestoesRespondidas($questao->id_prova) ?></span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 total-nao-respondidas">
                            Questões não respondidas: <span class="total"><?= totalDeQuestoesNaoRespondidas($questao->id_prova) ?></span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 total-parcialmente-respondidas">
                            Questões parcialmente respondidas <span class="total"><?= totalDeQuestoesParcialmenteRespondidas($questao->id_prova) ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rodape">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    Seu IP: <?php echo e(\Request::ip()); ?>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 direita">
                    <?php $dataServidor = new \DateTime() ?>
                    Hora do servidor: <?= $dataServidor->format("d/m/Y H:i:s") ?>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 direita">
                    © Sociedade Brasileira de Anestesiologia 2018.
                </div>
            </div>
            
        </div>
       
        <?php

        $data = new \DateTime();

        $extension = verificaTimeExtension(Auth::user()->id);
        if(!is_null($extension) && $extension != '' ):
            $fimProva = new \DateTime( $extension );
        else:
            $fimProva = new \DateTime( $matriz->dt_fim );
        endif;
        $diffHora = $fimProva->diff($data);
        if( $diffHora->invert == 0 ){
            $fimProva = new \DateTime();
            $diffHora = $fimProva->diff($data);
        }
        ?>

        <!-- Scripts -->
        <script src="/js/app.js"></script>
        <script src="/js/contador.js"></script>
        <script>

            var hora2 = <?= json_encode($diffHora->format("%h")) ?>;
            tempoDeProva(<?= json_encode($fimProva->format('Y,m,d,H,i,s')) ?>, <?= json_encode($data->format('Y,m,d,H,i,s')) ?>);

        </script>
    </body>
</html>
