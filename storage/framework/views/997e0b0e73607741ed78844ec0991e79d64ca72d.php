<?php $__env->startSection('content'); ?>

<div class="row">
  <div class="col-md-12 col-xs-12 col-sm-12">
    <h2>Relatório de Matrizes</h2>
    <hr>
  </div>
</div>
<?php if(session('mensagem')): ?>
<div class="alert alert-info"> <?php echo e(session('mensagem')); ?> </div>
<?php endif; ?>
<div class="row">

  <form method="get" action="/admin/lista/matriz" id="form-filtro">
    <div class="col-xs-12 col-md-3 col-sm-3">
      <label class="control-label">Tipo</label>
      <select name="tipo" class="form-control filtros">
       <option value="0"> - </option>
       <?php $__currentLoopData = $tipo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $t): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
       <option value="<?php echo e($t->id_tipo_prova); ?>" <?php echo e(verificaSelecionado(request()->input('tipo'), $t->id_tipo_prova)); ?>> <?php echo e($t->descricao); ?></option>
       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
     </select>
   </div>

   <div class="col-xs-12 col-md-3 col-sm-3">
     <label class="control-label">Status</label>
     <select name="status" class="form-control filtros">
       <option value="0"> - </option>
       <?php $__currentLoopData = $status->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
       <option value="<?php echo e($s->codigo); ?>" <?php echo e(verificaSelecionado(request()->input('status'), $s->codigo)); ?>> <?php echo e($s->descricao); ?></option>
       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
     </select>
   </div>

   <div class="col-xs-12 col-md-3 col-sm-3">
    <label class="control-label">Ano</label>
    <select name="ano" class="form-control filtros">
      <option value="0"> - </option>
      <?php $__currentLoopData = $ano; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <option value="<?php echo e($a[0]->ano); ?>" <?php echo e(verificaSelecionado(request()->input('ano'), $a[0]->ano )); ?>> <?php echo e($a[0]->ano); ?></option>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
  </div>

  <div class="col-xs-12 col-md-3 col-sm-3">
   <label class="control-label">Descrição</label>
   <div class="input-group">
    <input name="descricao" class="form-control" value="<?php echo e(request()->input('descricao')); ?>">
    <span class="input-group-btn">
      <button class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
    </span>
  </div>
</div>
</form>

<div class="col-md-12">
  <table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>Descrição</th>
        <th>Tipo</th>
        <th>Status</th>
        <th>Ano</th>
        <th>Questão</th>
        <th>Trim</th>
        <th>Início</th>
        <th>Fim</th>
      </tr>
    </thead>
    <tbody>
      <?php $__currentLoopData = $dados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td><?php echo e($dado->getIdMatriz()); ?></td>
        <td><?php echo e($dado->getDescricao()); ?></td>
        <td><?php echo e($dado->getTipoProva()->descricao); ?></td>
        <td><?php echo e($dado->getStatus()->descricao); ?></td>
        <td><?php echo e($dado->getAno()); ?></td>
        <td><?php echo e($dado->getAnoQuestao()); ?></td>
        <td><?php echo e($dado->getTrim()); ?></td>
        <td><?php echo e($dado->getDtInicio()->format("d/m/Y H:i:s")); ?></td>
        <td><?php echo e($dado->getDtFim()->format("d/m/Y H:i:s")); ?></td>
        <td style="width: 22em;">
          <form action="/admin/delete/matriz" method="post">
            <?php echo e(csrf_field()); ?>

            <input type="hidden" name="id_matriz" value="<?php echo e($dado->getIdMatriz()); ?>">
            <div class="row">
              
            <div class="btn-group pull-right">
              <a href='/admin/edit/matriz/<?php echo e($dado->getIdMatriz()); ?>' class="btn btn-primary" title="Editar matriz"><span class="glyphicon glyphicon-edit"></span></a>
              <a href='/admin/matriz/<?php echo e($dado->getIdMatriz()); ?>/nota' target="_blank" class="btn btn-info" title="PDF Notas"><span class="glyphicon glyphicon-education"></span></a>
              <a href='/admin/candidato/email/provas/<?php echo e($dado->getIdMatriz()); ?>' class="btn btn-success confirma" title="Enviar a prova para os candidatos"><span class="glyphicon glyphicon-file"></span></a>
              <a href='../util/gerarsenhas/<?php echo e($dado->getIdMatriz()); ?>' class="btn btn-warning confirma" title="Gerar e encriptar senhas"><span class="glyphicon glyphicon-random"></span></a>
              <a href='../util/encriptarsenhas/<?php echo e($dado->getIdMatriz()); ?>' class="btn btn-danger confirma" title="Encriptar as senhas"><span class="glyphicon glyphicon-lock"></span></a>
            </div>
          </div>
          <div class="row">
            
            <div class="btn-group pull-right">
              <a href='/admin/criaprovapormatriz/<?php echo e($dado->getIdMatriz()); ?>' class="btn btn-primary confirma" title="Criar provas"><span class="glyphicon glyphicon-asterisk"></span></a>

              <a href='../util/enviarsenhas/<?php echo e($dado->getIdMatriz()); ?>' class="btn btn-info confirma" title="Senhas por e-mail"><span class="glyphicon glyphicon-envelope"></span></a>
              <a href="../util/enviarsenhas/sms/<?php echo e($dado->getIdMatriz()); ?>" class="btn btn-success confirma" title="Senhas por SMS"><span class="glyphicon glyphicon-phone"></span></a>
              <a href="/admin/provas/finaliza/<?php echo e($dado->getIdMatriz()); ?>" class="btn btn-warning confirma" title="Finalizar provas"><span class="glyphicon glyphicon-remove"></span></a>
              <button class="btn btn-danger confirma" title="Excluir matriz"><span class="glyphicon glyphicon-trash"></span></button>
            </div>
          </div>
          <div class="row">
            
            <div class="btn-group pull-right">
              <a href='/admin/pdf/provas/<?php echo e($dado->getIdMatriz()); ?>' class="btn btn-warning confirma" title="Gerar os PDF's da provas"><span class="glyphicon glyphicon-download-alt"></span></a>
            </div>
          </div>
          </form>

        </td>
      </tr>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
    <tfoot>
      <tr><td colspan="15"><?php echo e($links); ?></td></tr>
    </tfoot>
  </table>
</div>

</div>


<script>
 $(document).ready(function(){

   $('.filtros').on('change',function(){
     $("#form-filtro").submit();
   });
   $(".confirma").click(function(){
    return confirm("Deseja realmente " + $(this).attr("title"));
  });
 });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>