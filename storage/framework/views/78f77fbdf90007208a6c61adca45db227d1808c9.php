<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <h2>Relatório de Questões</h2>
            <hr>
        </div>
    </div>

    <div class="row">


        <div class="col-md-12 col-xs-12 col-sm-12">

            <form method="get" action="" id="form-filtro" class="form-inline">
                <div class="input-group">
                    <label class="control-label">Ano</label>
                    <select name="ano" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        <?php $__currentLoopData = $anos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ano): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($ano); ?>" <?php echo e(@verificaSelecionado($ano, $_GET['ano'] ?? date("Y"))); ?>> <?php echo e($ano); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="input-group">
                    <label for="" class="control-label">Ano ME</label>
                    <select name="ind" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                        <option value="">-</option>
                        <?php $__currentLoopData = $periodo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($key); ?>" <?php echo e(@verificaSelecionado($key, $_GET['ind'] ?? '')); ?>> <?php echo e($value); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
                <div class="input-group">
                    <label for="tipo" class="control-label">Tipo</label>
                    <select name="t" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                    <option value="">-</option>
                        <?php $__currentLoopData = $tipoProva; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($key); ?>" <?php echo e(@verificaSelecionado($key, $_GET['t'] ?? '')); ?>> <?php echo e($value); ?> </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            </form>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12"><hr></div>
    <?php if(session('mensagem')): ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info"> <?php echo e(session('mensagem')); ?> </div>
    </div>
    <?php endif; ?>
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Questão</th>
                    <th>Tipo</th>
                    <th>Ano</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

            <?php if(count($dados) > 0): ?>
                <?php $__currentLoopData = $dados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo $dado->descricao; ?></td>
                    <td><?php echo e(\App\Http\Controllers\MatrizController::TIPO_PROVA[$dado->id_tipo_prova]); ?></td>
                    <td><?php echo e($dado->ano); ?></td>
                    <td style="width: 10em;">
                        <span class="btn-group pull-right">
                            <a href='/admin/questoes/<?php echo e($dado->id_questao); ?>/edit' class="btn btn-warning" title="Editar prova"><span class="glyphicon glyphicon-edit"></span></a>
                            <a href="/admin/opcoes/<?php echo e($dado->id_questao); ?>" class="btn btn-success" title="Mostrar opções"><span class="glyphicon glyphicon-list"></span></a>
                        </span>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                <tr><td colspan="8">Nenhum registro encontrado</td></tr>
            <?php endif; ?>
            </tbody>
        <tfoot>
            <tr><td colspan="15"><?php echo e($dados->links()); ?></td></tr>
        </tfoot>
        </table>
    </div>
    <script>
        jQuery(function($){
            $(".confirma").click(function(){
               return confirm("Deseja realmente " + $(this).attr("title"));
            });
        })
    </script>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>