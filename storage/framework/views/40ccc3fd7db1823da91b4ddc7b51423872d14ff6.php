<?php $__env->startSection('content'); ?>
<div class="container">
    <br><br><br><br><br><br><br><br>
    <div class="panel">
        <div class="panel-body" >
            <form class="form-horizontal" role="form" method="POST" action="<?php echo e(url('/admin/login')); ?>" id="form-login">
                <?php echo e(csrf_field()); ?>


                <div class="form-group <?php echo e($errors->has('usuario') ? ' has-error' : ''); ?>">
                    <label for="email" class="col-md-4 control-label">Usuário</label>
                    <div class="col-md-6">
                        <input id="cpf" type="text" class="form-control" name="usuario" value="<?php echo e(old('usuario')); ?>" required autofocus>

                        <?php if($errors->has('usuario')): ?>
                            <span class="help-block">
                                <strong><?php echo e($errors->first('usuario')); ?></strong>
                            </span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                    <label for="password" class="col-md-4 control-label">Senha</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        <?php if($errors->has('password')): ?>
                            <span class="help-block">
                                <strong><?php echo e($errors->first('password')); ?></strong>
                            </span>
                        <?php endif; ?>
                    </div>
                    
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-6">
                         <?php if(session('mensagem')): ?>
                            <div class="alert alert-danger"><?php echo e(session('mensagem')); ?></div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">Logar</button>
                    </div>
                </div>
            </form>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>