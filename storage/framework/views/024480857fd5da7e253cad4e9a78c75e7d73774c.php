<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <title>Provas SBA - Dashboard</title>
    <style>
        
        .table {
            
            width: 100%;
        }
        
        .table tr {
            line-height: 2em;
        }
        
        .table tr:nth-child(odd) {
            
            border: solid 1px #e6e6e6;
            background: #e6e6e6;
        }
        
        .topo table{
            width: 100%;
        }
        
        h2 {
            margin: 2em;
            text-align: center;
        }
        
        .divisor {
            width: 100%;
        }
    </style>
</head>
    <body>
        <div class="container">
            
            <div class="topo">
                <table>
                    <tr>
                        <td width="50%"><img src="<?php echo e(public_path('/images/logo-sba.jpg')); ?>" width="180"></td>
                        <td width="50%">
                           <?php echo $__env->make('layouts.dados-topo', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </td>
                    </tr>
                </table>
                
                <span class="divisor"> &nbsp;</span>
            </div>

            <h2>RELATÓRIO DE NOTAS</h2>
            
              <?php echo $__env->yieldContent('content'); ?>
                
        </div>
    </body>
</html>
