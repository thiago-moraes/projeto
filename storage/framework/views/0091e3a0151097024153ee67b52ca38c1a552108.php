<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <h2>Editar opção</h2>
            <hr>
        </div>
    </div>

    <div class="row">
         <p><?php echo $questao->descricao; ?></p>
        <div class="col-md-12 col-sm-12 col-xs-12"><hr></div>
    <?php if(session('mensagem')): ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-info"> <?php echo e(session('mensagem')); ?> </div>
    </div>
    <?php endif; ?>
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

            <?php if(count($dados) > 0): ?>
                <?php $__currentLoopData = $dados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td width="1"><?php echo e(\App\Http\Controllers\OpcaoController::INDICE_ALFA[$key]); ?>)</td>
                        <td colspan="3">
                            <form class="form-horizontal row" action="/admin/opcoes/<?php echo e($dado->id_opcao); ?>" method="post">
                                <?php echo e(csrf_field()); ?>

                                <?php echo e(method_field('PUT')); ?>

                                <div class="col-md-9 col-sm-9 col-xs-12"><textarea name="descricao" id="" cols="30" rows="5" class="form-control"><?php echo e($dado->descricao); ?> </textarea></div>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                <select name="valor" onchange="document.getElementById('form-filtro').submit()" class="form-control">
                                    <?php $__currentLoopData = \App\Http\Controllers\OpcaoController::RESPOSTA; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($key); ?>" <?php echo e(@verificaSelecionado($key, $dado->valor ?? '')); ?>> <?php echo e($item); ?> </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12">
                                    <button class="btn btn-success btn-block pull-right">Salvar</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                <tr><td colspan="8">Nenhum registro encontrado</td></tr>
            <?php endif; ?>
            </tbody>
        <tfoot>
            <tr><td colspan="5" class="text-right"><a href="/admin/questoes" class="btn btn-info">Voltar</a> </td></tr>
        </tfoot>
        </table>
    </div>
    <script>
        jQuery(function($){
            $(".confirma").click(function(){
               return confirm("Deseja realmente " + $(this).attr("title"));
            });
        })
    </script>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>