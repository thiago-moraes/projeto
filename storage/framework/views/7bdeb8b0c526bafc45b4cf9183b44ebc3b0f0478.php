<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="cache-control"   content="no-cache, no-store, max-age=0, must-revalidate" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="expires" content = "Mon, 22 jul 2006 11:12:01 GMT" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e(isset($matriz->descricao) ? $matriz->descricao : 'Provas SBA'); ?> - <?php echo e(isset($matriz->ano) ? $matriz->ano : @date("Y")); ?></title>
    <!-- Styles -->
    <!-- <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>"> -->
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/prova.css')); ?>">
    <!-- Scripts -->
    <script>    
       /* window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>*/
    </script>
    <script>
            history.pushState(null, null, null);
            window.onpopstate = function () {
               history.go(1);
           };
        </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
               <div class="container">

                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                   <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button> -->
                    <!-- Branding Image -->

                    <h4><?php echo e(isset($matriz->descricao) ? $matriz->descricao : 'Provas SBA'); ?> - <?php echo e(isset($matriz->ano) ? $matriz->ano : @date("Y")); ?></h4>
                </div>

                <!-- <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Right Side Of Navbar
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links
                        <?php if(auth()->guard()->guest()): ?>
                            <li><a href="<?php echo e(url('/auth/login')); ?>">Login</a></li>
                            <li><a href="<?php echo e(url('/register')); ?>">Register</a></li>
                        <?php else: ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <?php echo e(Auth::user()->nome); ?> <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="<?php echo e(url('/logout')); ?>"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Sair
                                        </a>

                                        <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST" style="display: none;">
                                            <?php echo e(csrf_field()); ?>

                                        </form>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div> -->
            </div>
            </div>
        </nav>

        <div class="container">

            <div class="col-md-12 conteudo">

                <?php echo $__env->yieldContent('content'); ?>

            </div>
        </div>

    </div>

    <!-- Scripts -->
    <!-- <script src="/js/app.js"></script> -->

</body>
</html>
