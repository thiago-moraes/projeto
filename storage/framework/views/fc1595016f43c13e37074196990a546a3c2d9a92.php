<?php $__env->startSection('content'); ?>

    <table class="table table-striped" style="border-collapse: collapse;">
        <tr>
            <th>Nome</th>
            <th align="right">Nota</th>
            <th align="right">Aproveitamento</th>
        </tr>
        <?php $__currentLoopData = $dados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($dado->nome); ?></td>
            <td align="right"><?php echo e($dado->nota); ?></td>
            <td align="right"><?php echo e($dado->aproveitamento); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <tr><td colspan="2">Total de registros:</td><td align="right"><?php echo e(count( $dados )); ?></td></tr>
    </table>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.relnotapdf', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>