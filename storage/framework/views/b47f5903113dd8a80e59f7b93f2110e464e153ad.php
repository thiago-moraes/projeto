<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <h2>Editar candidato</h2>
        <hr>
    </div>
    <div class="col-md-12">
        <?php if(session('mensagem')): ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-info"> <?php echo e(session('mensagem')); ?> </div>
        </div>
        <?php endif; ?>
        <form class="form-horizontal" method="post" action ="/admin/candidato/<?php echo e($dados->getId()); ?>">
            <?php echo e(@method_field('PUT')); ?>

           <?php echo $__env->make('admin.conteudo-form-candidato', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </form>
    </div>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>