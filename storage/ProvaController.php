<?php

namespace App\Http\Controllers;

use App\Candidato;
use App\Matriz;
use App\Prova;
use App\TimeExtension;
use Illuminate\Http\Request;
use App\Http\Requests\ProvaRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ProvaQuestaoController;
use App\Http\Controllers\OpcaoProvaQuestaoController;
use Illuminate\Support\Facades\DB;

class ProvaController extends Controller
{
    private $candidato;
    private $matriz;
    private $idProva;
    private $idCandidato;
    private $idMatriz;
    private $status;
    private $inicio;
    private $fim;

    const STATUS = [0 => 'Não acessada', 1 => 'Não finalizada', 2 => 'Finalizada'];

    public function setCandidato($candidato){
        $this->candidato = $candidato;
    }

    public function getCandidato(){
        return $this->candidato;
    }

    public function setMatriz($matriz){
        $this->matriz = $matriz;

    }
    public function getMatriz(){
        return $this->matriz;

    }

    public function setIdProva($id){
        $this->idProva = $id;
    }

    public function getIdProva(){
        return $this->idProva;
    }

    public function setIdMatriz( $idMatriz ){
        $this->idMatriz = $idMatriz;
    }

    public function getIdMatriz(){
        return $this->idMatriz;
    }

    public function setIdCandidato( $idCandidato ){
        return $this->idCandidato = $idCandidato;
    }

    public function getIdCandidato(){
        return $this->idCandidato;
    }

    public function setStatus( $status ){
        $this->status = $status;
    }

    public function getStatus(){
        return $this->status;
    }

    public function getInicio(){
        return $this->inicio;
    }

    public function setInicio( $inicio ){
        /*$data = new \DateTime();

        if(strpos('/', $inicio))
            $this->inicio = $data->createFromFormat('d/m/Y H:i:s', $inicio);

        if(strpos('-', $inicio))
            $this->inicio = $data->createFromFormat('Y-m-d H:i:s', $inicio);
         */
        $this->inicio = $inicio;
    }

    public function getFim(){
        return $this->fim;
    }

    public function setFim( $fim ){
        /*$data = new \DateTime();

        if(strpos('/', $fim))
            $this->fim = $data->createFromFormat('d/m/Y H:i:s', $fim);

        if(strpos('-', $fim))
            $this->fim = $data->createFromFormat('Y-m-d H:i:s', $fim);
         */
        $this->fim = $fim;
    }

    public function index( $tipo, $idProva ){


        if(!preg_match('/[objetiva]|[escolha]/', $tipo))
            return view('errors.503');

        $prova = Prova::find( $idProva );
        if( $prova ){
            if( $prova->status == 2 ){
                return redirect('/prova/create');
                die();
            }
        }
        
        $provaQuestaoController = new ProvaQuestaoController();
        $respondidas = $provaQuestaoController->questoesRespondidasPorIdProva( $idProva );
        if( $respondidas->count() > 0 ){
            $questoes = $provaQuestaoController->questoesNaoRespondidasPorIdProva( $idProva );
            if( $questoes->count() > 0):
                return view('lista-questoes-objetiva',
                    [
                        'idProva' => $idProva,
                        'questoes' => array_map(function($obj){
                            $provaQuestao = new ProvaQuestaoController();
                            return (object) $provaQuestao->dadosProvaPorIdProvaQuestao( $obj['id_prova_questao'] );
                        }, $questoes->get()->toArray())
                    ]
                );
            else:
                return redirect("/prova/{$tipo}/{$idProva}/1");
            endif;
        }

        return redirect("/prova/{$tipo}/{$idProva}/1");
    }

    public function mostraQuestao( $tipo, $idProva, $sequencia ){
        
        $prova = Prova::find($idProva);
        if( $prova->status == 2){
            return $this->redirectFinalizacao( $prova->id_prova );
            die();
        }

        $provaQuestao = new ProvaQuestaoController();
        $questoes = $provaQuestao->questoesPorIdProva($prova->id_prova);
        
        $total = $questoes->count();

        $questao = (object) $provaQuestao->questaoPorProvaSequencia($prova->id_prova, $sequencia);

        $matriz = new MatrizController();
        $dadosMatriz = $matriz->retornaDadosMatriz(Auth::user()->id_matriz);
        
        return view($tipo,[
            'questao' =>$questao,
            'totalQuestoes' => $total,
            'matriz' => $dadosMatriz,
        ]);
    }

    public function create(){
        /* Verifica se o candidato está dentro da data/hora definido para a prova */
        $liberada = $this->verificaSeProvaEstaLiberada();

        if($liberada != ""){
            return view('prova-nao-liberada', ['data' => $liberada]);
        }

        /* Verifica se o candidato possui prova criada*/
        $dadosProva = $this->buscaProvaPorIdCandidato(Auth::user()->id, Auth::user()->id_matriz);        
        if( !is_object($dadosProva) ){

            $dadosProva = $this->criaProva(Auth::user()->id, Auth::user()->id_matriz);
            
        }else{

            if( $dadosProva->status == 2 ){
                return $this->redirectFinalizacao( $dadosProva->id_prova );
                die();
            }
			
			if( $dadosProva->status == 0 ){
				$dadosProva->status = 1;
				$dadosProva->save();
			}

        }

        return redirect('/prova/questoes/' . $dadosProva->id_prova);

    }

    public function criaProva( $idCandidato, $idMatriz )
    {
        return Prova::create([ 'id_candidato' => $idCandidato, 'id_matriz' => $idMatriz, 'status' => 0 ]);
    }

    public function finalizaTodasAsProva( $idMatriz )
    {
        $provas = Prova::where("id_matriz", $idMatriz)->where('status', 1)->get();

        foreach($provas as $prova):
            $this->cancelaProva($prova);
        endforeach;

        return redirect()->back()->with(["mensagem" => "Provas finalizadas com sucesso!"]);

    }

    public function cancelaProva( Prova $prova )
    {
        
        $nota = new NotaController();
        $nota->gravaNota($prova->id_prova);
        $prova->status = 2;
        $prova->responsible = 0;
        $prova->save();

        return redirect()->back()->with(["mensagem" => "Prova finalizada"]);
    }

    public function finalizaProva( Request $request )
    {

        $idProva = (int) $request->input( "idProva" );

        $prova = Prova::find($idProva);
        if( $prova->status == 2 ){
            return redirect("/prova/create");
            exit;
        }


        $provaQuestao = new ProvaQuestaoController();
        $questoes = $provaQuestao->questoesNaoRespondidasPorIdProva( $idProva );


        return view('questoes-nao-finalizadas-objetiva',
            [
                'idProva' => $idProva,
                'questoes' => array_map(function($obj){
                    $provaQuestao = new ProvaQuestaoController();
                    return (object) $provaQuestao->dadosProvaPorIdProvaQuestao( $obj['id_prova_questao'] );
                }, $questoes->get()->toArray())
            ]
        );

    }

    public function confirmaFinalizacaoDaProva(Request $request = null)
    {

        if( is_null($request->input('idProva')) ){
            $provaObj = Prova::where('id_candidato', Auth::user()->id)->where('id_matriz', Auth::user()->id_matriz)->first();
            $idProva = $provaObj->id_prova;
        }else{
            $idProva = $request->input('idProva');
            $provaObj = Prova::where('id_prova', $idProva)->first();
        }
        
        $provaObj->status = 2;
        $provaObj->save();

        return $this->redirectFinalizacao( $idProva );
        die();
    }

    private function redirectFinalizacao( $idProva )
    {

        $nota = new NotaController();
        $dados = $nota->gravaNota($idProva);

        $opcaoProvaQuestao = new OpcaoProvaQuestaoController();
        $dadosOpcoes = $opcaoProvaQuestao->resumo($idProva);
        
        return view('fim-prova', [
            'dadosNota' => $dados,
            'opcao' => $dadosOpcoes,
        ]);
    }

    public function buscaProvaPorIdCandidato($idCandidato, $idMatriz)
    {

        $prova = Prova::where('id_candidato', $idCandidato)
            ->where('id_matriz', $idMatriz)->first();

        if(is_object($prova)){
            if( $prova->status == 0){
                $prova->responsible = $idCandidato;
                $prova->status = 1;
                $prova->save();
            }

            return $prova;
        }

        return 0;
        
    }

    public function verificaProvaNaoFinalizada()
    {

        return Prova::where('id_candidato', '=', Auth::user()->id)
            ->where('status', '<=', 1)
            ->where('id_matriz', Auth::user()->id_matriz)->get();
    }

    private function verificaSeProvaEstaLiberada()
    {

        $timeZoneObj = new \DateTimeZone( 'America/Sao_Paulo' );

        $matrizObj = new MatrizController();
        $dadosMatriz = $matrizObj->retornaDadosMatriz(Auth::user()->id_matriz);

        $time = TimeExtension::where("candidato", Auth::user()->id)->orderBy("id", "DESC")->first();

        $data = new \DateTime('now', $timeZoneObj);

        $inicioDaProva = new \DateTime($dadosMatriz->dt_inicio, $timeZoneObj);
        $fimDaProva = new \DateTime($time->dt_fim ?? $dadosMatriz->dt_fim, $timeZoneObj);

        /*   Verifica se a prova está dentro do período de liberação */

        if( $inicioDaProva < $data && $fimDaProva > $data){
            return '';

        }

        /*  Retorna a data de liberação da prova */
        return $inicioDaProva;
    }

    public function formProva( $idProva )
    {

        $prova = Prova::find($idProva);
        $this->setDados( $prova );

        $codigos = \App\Codigos::where('grupo',1)->get();
        $matrizes = \App\Matriz::where('status', 1)->get();

        return view('admin.formProva',['dados' => $this, 'codigos' => $codigos, 'matrizes' => $matrizes]);
    }

    public function updateProva(ProvaRequest $request)
    {
        
        if( $request ){

            $idProva = $request->input('id_prova');
            $prova = Prova::find($idProva);

            $prova->id_matriz = $request->input('id_matriz');
            $prova->status = $request->input('status');

            $prova->save();

            return redirect('admin/lista/provas');

        }

        return redirect()
            ->back()
            ->withInput();
    }

    public function delete( Request $request )
    {

        $idProva = $request->input('id_prova');
        $prova = Prova::find( $idProva );
        $prova->delete();

        return redirect('admin/lista/provas')->with(['mensagem'=>'Prova removida com sucesso!']);
    }

    public function lista(Request $request)
    {

        $nome = $request->input('c') ?? '';

        $provas = Prova::join("candidatos", function($join){
            $join->on("candidatos.id", "=" , "prova.id_candidato")
                ->on("candidatos.id_matriz", "=", "prova.id_matriz");
        })
            ->join("matriz", "matriz.id_matriz", "=", "candidatos.id_matriz")
            ->where("candidatos.nome", "like", "%{$nome}%")
            ->when($request->input("ano"), function($query){
                $query->whereYear("prova.created_at", request()->input("ano"));
            })->when($request->input("me"), function($query){
                $query->where("matriz.ind_me", request()->input("me"));
            })->when($request->input("trim"), function($query){
                $query->where("matriz.trim", request()->input("trim"));
            })->when($request->input("tipo"), function($query){
                $query->where("matriz.id_tipo_prova", request()->input("tipo"));
            })->when($request->input("s"), function($query){
                $query->where("prova.status", request()->input("s"));
            })->when($request->input("cet"), function($query){
                $query->where("candidatos.cet", request()->input("cet"));
            })->when($request->input("ma"), function($query){
                $query->where("matriz.id_matriz", request()->input("ma"));
            })
            ->select("candidatos.nome","matriz.*", "prova.*")
            ->orderBy("nome")
            ->paginate(10);

        foreach($provas as $prova):
            $p = new ProvaController();
            $p->setDados( (object) $prova );
            $p->matriz = $prova->matriz();
            $dados[] = $p;
        endforeach;
        $cets = array_keys(Candidato::all(['cet'])->groupBy('cet')->toArray());
        sort($cets);
        
        return view('admin.listaProvas', [
            'dados' => $dados ?? [],
            'anos' => $this->anosProva(),
            'cets' => $cets,
            'matrizes' => array_collapse(Matriz::all(['id_matriz','descricao'])->groupBy('id_matriz')->toArray()),
            'me' => MatrizController::IND_ME,
            'tipo' => MatrizController::TIPO_PROVA,
            'trim' => MatrizController::TRIMESTRE,
            'status' => ProvaController::STATUS,
            'total' => $provas->total(),
            'link' => $provas->appends([
                'c' => $request->input("c"),
                's' => $request->input("s"),
                'ano' => $request->input("ano"),
                'trim' => $request->input("trim"),
                'me' => $request->input("me"),
                'tipo' => $request->input("tipo"),
                'ma' => $request->input("ma"),
                'cet' => $request->input("cet"),
            ])->links()
        ]);
    }

    private function anosProva()
    {
        $dados = DB::select("select YEAR(created_at) as ano from prova group by YEAR(created_at)");
        return $dados;
    }

    public function criaProvaPorMatriz( $idMatriz )
    {
        
        dispatch(new \App\Jobs\GeraProvasPorMatriz($idMatriz));
        return redirect()->back()->with('mensagem', "As provas estão sendo geradas.");

    }

    private function setDados( $prova ){
        
        $this->setIdProva($prova->id_prova);
        $this->setIdCandidato($prova->id_candidato);
        $this->setIdMatriz($prova->id_matriz);
        $this->setStatus($prova->status);
        $this->setInicio($prova->created_at);
        $this->setFim($prova->updated_at);
        $this->setCandidato($prova->dadosCandidato);
    }
    
}
