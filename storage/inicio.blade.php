<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Provas SBA</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style>
            ul li {
                padding: .8em 0px;
                font-size: 1.1em;
                font-weight: normal;
            }
            ul {
                list-style: none;
            }

        </style>
    </head>
    <body>
    <div class="flex-center position-ref full-height"><br><br>
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-4 col-md-4"><img src="{{ asset('images/logo-sba.jpg') }}" alt="SBA 70 anos" width="250"></div>
                </div><br>
                <div class="col-md-8 col-md-offset-2">

                    <h3 class="text-center">Sistema de provas online SBA.</h3>

                    <div class="row text-center">
                        <div class="col-xs-12 col-sm-12 col-md-12"><h4>Tempo para inicio da prova: <span id="tempo-de-prova"></span></h4></div>
                    </div>

                    <div class="links">
                        <ul>
                            <li>Para acesso a prova utilize seu número de CPF para usuário e a senha recebida.</li>
                            <li>Durante o tempo de prova você poderá navegar pelas questões respondendo ou não as opções.</li>
                            <li>O sistema permite questões parcialmente repondidas, ou seja, as opções podem ficar em branco.</li>
                            <li>Ao avançar ou retroceder na prova as respostas serão confirmadas.</li>
                            <li>Ao terminar de responder as questões clique no botão <button class="btn btn-danger">Finalizar Prova.</button> Antes de finalizar, revise sua prova. Não clique no botão finalizar se não for para terminar a prova.</li>
                            <li>Ao finalizar a prova o sistema solicitará confirmação.</li>
                            <li>Finalizando a prova o sistema irá calcular sua nota e aproveitamento.</li>
                            <li>A sessão será finalizada com 15 minutos de inatividade, para continuar do ponto que parou, basta recarregar a página e fazer novo login.</li>
							<li>As provas serão enviadas para o seu e-mail no dia seguinte a sua realização.</li>
                            <li><br><div class="col-md-4 col-md-offset-4"><a href="{{ url('/login') }}" class="btn btn-info btn-block">Login</a></div></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
    $data = new \DateTime();
    $fim = dataHoraProva();
 
    if($fim){
        
        $dataFim = new \DateTime($fim);
        $dataDiff = $dataFim->diff($data);

        if($dataDiff->invert == 0){
            
            print '<script>';
            print 'document.getElementById("tempo-de-prova").remove()';
            print '</script>';
        }
    }else{
        exit;
    }

?>
<script src="/js/contador.js"></script>

<script>
    <?php if($dataDiff->format("%D") > 1): ?>
        var hora2 = <?= json_encode($dataDiff->format("%D dias e %H")) ?>;
    <?php else: ?>
        var hora2 = <?= json_encode($dataDiff->format("%h")) ?>;
    <?php endif;?>
    tempoDeProva(<?= json_encode($dataFim->format('Y,m,d,H,i,s')) ?>, <?= json_encode($data->format('Y,m,d,H,i,s')) ?>);
    setInterval(function(){
        if(document.getElementById("tempo-de-prova") != null) {
            var texto = document.getElementById("tempo-de-prova").innerHTML;
            if (texto == '00:00:01') {
                location.href = 'http://<?= $_SERVER['HTTP_HOST'] ?>/login';
            }
        }
    },1000);
</script>

