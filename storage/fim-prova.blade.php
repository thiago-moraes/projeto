@extends('layouts.app')
<style>
    h4 {
        margin-bottom: 1.5em !important;
    }

    h2 {
        margin-bottom: 2em !important;
        text-decoration: underline;
    }
</style>
@section('content')

    <h2>Resultado</h2>
    <h4> Sua nota foi: {{ $dadosNota->nota }} </h4>
    <h4> Aproveitamento: {{ $dadosNota->aproveitamento }} % </h4>
    <h4> Quantidade de acertos: {{ @count($opcao['opcoesCorretas']) }} / {{ @count($opcao['opcoes']) }}</h4>


    <form id="logout-form" action="{{ url('/logout') }}" method="POST">

        {{ csrf_field() }}
        <button class="btn btn-success">Sair</button>
    </form>

@endsection
<script>
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
</script>