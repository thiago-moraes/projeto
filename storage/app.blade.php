<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="cache-control"   content="no-cache, no-store, max-age=0, must-revalidate" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="expires" content = "Mon, 22 jul 2006 11:12:01 GMT" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $matriz->descricao or 'Provas SBA' }} - {{ $matriz->ano or @date("Y") }}</title>
    <!-- Styles -->
    <!-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/prova.css') }}">
    <!-- Scripts -->
    <script>    
       /* window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>*/
    </script>
    <script>
            history.pushState(null, null, null);
            window.onpopstate = function () {
               history.go(1);
           };
        </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
               <div class="container">

                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                   <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button> -->
                    <!-- Branding Image -->

                    <h4>{{ $matriz->descricao or 'Provas SBA' }} - {{ $matriz->ano or @date("Y") }}</h4>
                </div>

                <!-- <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Right Side Of Navbar
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links
                        @guest()
                            <li><a href="{{ url('/auth/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->nome }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Sair
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div> -->
            </div>
            </div>
        </nav>

        <div class="container">

            <div class="col-md-12 conteudo">

                @yield('content')

            </div>
        </div>

    </div>

    <!-- Scripts -->
    <!-- <script src="/js/app.js"></script> -->

</body>
</html>
