<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::domain(['provasonlinesba.com.br','provassba.com.br'],function () {

Route::get('/', function () {
    return view('inicio');    
});


Route::get('/avaliacao', function () {
    return view('inicio');
});

Route::group(['prefix' => 'admin/login'], function(){
        
    Route::get('/', 'Auth\AdminLoginController@showLoginForm');
    Route::post('/', 'Auth\AdminLoginController@authenticate');
});


Route::group(['prefix' => 'admin',
'middleware' => 'auth:admin'
], 
function(){

    Route::get('/', 'Admin\AdminController@index');
    Route::get('/dashboard', 'Admin\AdminController@index');    
    Route::get('/prova/online', 'ProvaOnlineController@index');

    // Gera os PDFs das provas por matriz
    Route::get('/pdf/provas/{idMatriz}','MatrizController@relMailNotas');

    //Envia a prova de um candidato
    Route::get("/candidato/{idCandidato}/mailprova", "CandidatoController@relProvaMail")->where(['id'=>'[0-9]+']);

    //Envia as provas pros candidatos de uma matriz
    Route::get('/candidato/email/provas/{idMatriz}', 'UtilController@enviaProva');

    //Cria as provas a partir da matriz
    Route::get('/criaprovapormatriz/{id}', 'ProvaController@criaProvaPormatriz');
    
    //Gera um PDF com os candidatos e suas respectivas notas
    Route::get('/matriz/{id}/nota', 'MatrizController@relNotas')->where(['id'=>'[0-9]+']);
    
    //Finaliza todas a provas a partir da matriz
    Route::get('/provas/finaliza/{idMatriz}', 'ProvaController@finalizaTodasAsProva');

    //Finaliza uma prova
    Route::get("/cancela/prova/{prova}", "ProvaController@cancelaProva");

    //Adiciona tempo a prova de um candidato
    Route::get("/add/time/{idCandidato}", "CandidatoController@formAddExtension");
    Route::post("/add/time", "CandidatoController@addExtension");
    
    Route::group(['prefix' => 'util'], function(){

        Route::get('encriptarsenhas/{idMatriz}','UtilController@encrypt');
        Route::get('gerarsenhas/{idMatriz}','UtilController@gerarsenha');
        Route::get('encriptarsenhasadmin','UtilController@encryptAdmin');
        Route::get('enviarsenhas/sms/{idMatriz}','UtilController@enviaSenhaSMS');
        Route::get('enviarsenhas/{idMatriz}','UtilController@envia_senha');

        Route::get('import/questions', 'UtilController@formImportQuestions');
        Route::post('import/questions', 'UtilController@importQuestions');
    });


    Route::group(['prefix'=>'novo'], function(){

        Route::get('/matriz', 'MatrizController@formNovaMatriz');
        Route::post('/matriz', 'MatrizController@addMatriz');
        
    });

    Route::group(['prefix'=>'edit'], function(){
        
        Route::post('/matriz', 'MatrizController@updateMatriz');
        Route::get('/matriz/{idMatriz}', 'MatrizController@formMatriz')->where(['idMatriz' => '[0-9]+']);

        Route::get('/candidato/{idCandidato}', 'CandidatoController@formCandidato')->where(['idCandidato' => '[0-9]+']);
        Route::post('/candidato', 'CandidatoController@updateCandidato');
        
        Route::get('/prova/{idProva}', 'ProvaController@formProva')->where(['idprova'=>'[0-9]+']);
        Route::post('/prova/', 'ProvaController@updateProva');
    });



    Route::group(['prefix' => 'lista'], function(){

        Route::get('/matriz', 'MatrizController@lista');
        //Route::get('/candidatos', 'CandidatoController@lista');
        Route::get('/provas', 'ProvaController@lista');
    });
    
    Route::group(['prefix' => "delete"], function(){
        
        Route::post('/matriz', 'MatrizController@delete');
        Route::post('/prova', 'ProvaController@delete');
        Route::post('/candidato', 'CandidatoController@delete');
    });
    
    Route::group(['prefix' => "/send/senha"], function(){
        
        Route::get('/sms/{candidato}', 'CandidatoController@reminderSenhaSMS')->where(['id' => '[0-9]+']);
        Route::get('/{candidato}', 'CandidatoController@reminderSenha')->where(['id' => '[0-9]+']);
    });
    
    
    Route::resource("candidato", "CandidatoController");
    Route::resource("questoes", "QuestaoController");
    Route::resource("opcoes", "OpcaoController");
    Route::resource("sessoes", "SessionsController");
    
});

Route::get('/logout', function(){
    
    Illuminate\Support\Facades\Auth::logout();
    return redirect("/login");
});

Route::get('/login/remember', 'CandidatoController@formRemember');
Route::post('/login/remember', 'CandidatoController@remember');

Route::auth();

Route::group(['prefix' => 'prova', 
'middleware'=>'auth'
]
, function(){
    
    
    Route::get('/', 'ProvaController@create');
    Route::get('/create', 'ProvaController@create');
    Route::get("/questoes/{prova}", 'ProvaQuestaoController@criaQuestoesProva');
    Route::get('/{tipo}/{prova}/', 'ProvaController@index')
            ->where(['tipo' => '[a-z]+','prova' => '[0-9]+']);

    Route::get('/{tipo}/{prova}/{idProvaQuestao}', 'ProvaController@mostraQuestao')
            ->where(['tipo' => '[a-z]+','prova' => '[0-9]+', 'idProvaQuestao' => '[0-9]+']);

    Route::post('/confirma/resposta', 'ProvaController@index');
    Route::post('/grava/questao', 'ProvaQuestaoController@gravaQuestao');
    Route::post('/grava/questao/escolha', 'ProvaQuestaoController@gravaQuestaoEscolha');
    Route::any('/finaliza', 'ProvaController@finalizaProva');
    Route::any('/finaliza/confirma', 'ProvaController@confirmaFinalizacaoDaProva');
    
});

Route::get('/pdf/prova', 'CandidatoController@relNotas')->where(['id'=>'[0-9]+']);

//});

