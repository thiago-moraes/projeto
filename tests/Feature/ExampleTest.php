<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Contracts\Auth\Authenticatable;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /*public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }*/

   /* public function testFinalizaTodasAsProva()
    {
        $admin = \App\Admin::where("codigo", '=', 1);
        $response = $this->json("GET", '/admin/provas/finaliza/2');
        $response->assertStatus(200);
    }*/

    public function testFinalizaProva()
    {
        $candidatos = \App\Candidato::where('id_matriz', '=', 1)->get();
        foreach( $candidatos as $candidato ):
            $prova = \App\Prova::where("id_candidato", $candidato->id)->where("id_matriz", 1)->first();
            if( $prova )
                $response = $this->actingAs($candidato)->json("POST", '/prova/finaliza/confirma', ['idProva' => $prova->id_prova]);
                
        endforeach;
    }
}
